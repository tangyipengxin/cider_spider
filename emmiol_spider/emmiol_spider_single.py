# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
from entity.ProductEntity import Image, Product, Variant
from entity.EmmiolProdEntity import EmmiolProdEntity, EmmiolSKU, EmmiolImage, EmmiolDesc
from common import utils
import codecs
import json
import time
import datetime
from lxml import etree
from common import myRequest
import requests
import random





class EmmiolSpider(object):
    def __init__(self, debug):
        self.req = myRequest.MyRequest()
        self.date = datetime.date.today().strftime("%Y%m%d")
        self.debug = debug
        if self.debug == 'true':
            self.date = '20210501'
        self.prod_list = {}

    def init_conf(self):
        self.prod_spu_list = [
                'DAF0392TO',
'CAF0050DR',
'CAF0310DR',
'MBD0211DR',
'MBD0021DR',
'CBD0456DR',
'MBD0199DR',
'LBC0479DR',
'LBC0478DR',
'LBC0471DR',
'LBC0487DR',
'CBD0265DR',
'CBD0188DR',
'KBD0223DR',
'KBD0221DR',
'CBD0199DR',
'MBD0237DR',
'MBD0228DR',
'MBD0227DR',
'LBC0487DR',
'LBC0471DR',
'LBC0478DR',
'LBC0479DR',
'MBD0021DR',
'CAF0050DR',
                ]

    def url_spider(self):
        while True:
            data_json = []
            with open("../script/data_dump_1.json", "r") as f:
                data_lines = f.readlines()
                for line in data_lines:
                    data_unit = json.loads(line.strip())
                    item_id = data_unit.get('item_id')
                    spu_code = item_id[0:9]
                    if spu_code in self.prod_spu_list:
                        data_json.append(data_unit)

                for unit in data_json:

                    prod_unit = EmmiolProdEntity().from_json(unit)
                    if prod_unit.spu_code not in self.prod_list:
                        self.prod_list[prod_unit.spu_code] = prod_unit
                        self.extract_detail(prod_unit)
                            
            break


    def extract_detail(self, prod_unit):
        sku_list = prod_unit.sku_list
        detail_url = ""
        if prod_unit.init_detail_url == '':
            return False
        detail_url = prod_unit.init_detail_url
        print("start spider:%s" % (detail_url))
        while True:
            resp = self.req.sync_get(detail_url)
            if resp is not None:
                break
        print("finish spider:%s" % (detail_url))
        html_text = etree.HTML(resp.text)
        
        desc = EmmiolDesc()
        desc.detail = self.get_detail(html_text)
        desc.size = self.get_size_info(prod_unit.init_sku_id)
        desc.return_and_shipping = self.get_return_and_shipping(html_text)
        prod_unit.set_desc(desc)

        sku_list, next_detail_urls = self.init_sku_info(html_text, prod_unit)
        for sku in sku_list:
            prod_unit.add_sku(sku)
        self.get_next_images(next_detail_urls, prod_unit)
        prod_unit.init_sku_code()

        prod_unit.tags = self.get_tags(html_text)


    def get_next_images(self, next_detail_urls, prod_unit):
        for url in next_detail_urls:
            while True:
                resp = self.req.sync_get(url)
                if resp is not None:
                    break
            html_text = etree.HTML(resp.text)
            image_list = self.get_image_list(html_text, prod_unit)
            selected_color = self.get_select_color(html_text, prod_unit)
            prod_unit.set_color_images(selected_color, image_list)

    def get_select_color(self, html_text, prod_unit):
        color_data = html_text.xpath('//ul[@class="choose_list  clearfix"]')
        li_list = color_data[0].xpath('li')
        for li_unit in li_list:
            color = li_unit.get("title")

            li_class_attr = li_unit.get('class')
            if li_class_attr.find('selected') != -1:
                return color
        return ""


    def get_return_and_shipping(self, html_text):
        #url = "https://www.emmiol.com/co-common-ac-index.html"
        #headers = {"content-type": "application/x-www-form-urlencoded"}
        #data = [{"action": "freeShippingTips", "param":{"catId": "503"}}]
        #resp = requests.post(url, headers=headers, data=data)
        #print(resp.text)
        text_1 = "".join(html_text.xpath('//div[@class="ship-1 fl"]//text()'))
        text_2 = "".join(html_text.xpath('//div[@class="ship-2 fl"]//text()'))
        return "%s%s" % (text_1, text_2)

    def get_image_list(self, html_text, prod_unit):
        spu_code = prod_unit.get_spu_code()
        div_data = html_text.xpath('//div[@class="swiper-slide"]')
        src_list = []
        for div_unit in div_data:
            img_list = div_unit.xpath("img")
            for img_unit in img_list:
                src = img_unit.get('src')
                if src is None:
                    continue
                if src.find("img_original-%s" % spu_code) != -1:
                    src_list.append(src)
        return src_list

    def get_detail(self, html_text):
        description = html_text.xpath('//div[@class="de_con clearfix"]')
        desc_str = str(etree.tostring(description[0].getchildren()[0])).replace('<br/>', '\r\n').replace("<p>", "").replace("</p>", "")
        desc_line_list = desc_str.split("\r\n")
        detail_list = []
        for line_unit in desc_line_list:
            if len(line_unit.split(":")) == 2:
                detail_list.append({line_unit.split(":")[0].strip().replace("b'", ''): line_unit.split(":")[1].strip().replace("'", '')})
            else:
                detail_list.append({line_unit: ''})
        return detail_list

    def get_tags(self, html_text):
        tags = html_text.xpath('//div[@class="tag-part"]')
        if len(tags) == 0:
            return []
        tag_list = []
        for children in tags[0].getchildren():
            tag = children.text
            tag_list.append(tag)
        return tag_list

    def get_size_info(self, goodsId):
        data = {"goodsId": goodsId}
        headers = {"Content-Type": "application/x-www-form-urlencoded", "Content-Length": str(len(str(data))), "Host": "www.emmiol.com"}
        url = "https://www.emmiol.com/co-goods-ac-goodsSizeGuideDetails.json"
        while True:
            resp = requests.post(url, headers=headers, data=data)
            if resp is not None:
                break
        cm_size_list = json.loads(resp.text)
        size_data = cm_size_list.get('data')
        if len(size_data) != 0 and len(size_data.get('menu')) != 0:
            menu = size_data.get('menu')
            size_info = size_data.get("sizeList")
            return {"menu": menu, "size_list": size_info}

        url = "https://www.emmiol.com/co-goods-ac-goodsSizeGuide.json"
        while True:
            resp = requests.post(url, headers=headers, data=data)
            if resp is not None:
                break
        cm_size_list = json.loads(resp.text)
        size_data = cm_size_list.get('data')
        if len(size_data) != 0:
            size_info = size_data.get('US')
            menu = size_data.get("menu")
            return {"menu": menu, "size_list": [size_info]}

        return ""

    def get_pre_sale(self, html_text):
        pre_sale = html_text.xpath('//p[contains(@class, "pre-sale-wrap")]//text()')
        return "".join(pre_sale)
        

    def init_sku_info(self, html_text, emmiol_prod):
        sku_list = []
        select_sku = None 
        size_list = []
        color_list = []

        image_list = self.get_image_list(html_text, emmiol_prod)
        next_detail_urls = []

        color_data = html_text.xpath('//ul[@class="choose_list  clearfix"]')
        li_list = color_data[0].xpath('li')
        for li_unit in li_list:
            a_unit = li_unit.getchildren()[0]
            color = a_unit.get("title")
            color_detail = a_unit.get("data-url")
            sku = EmmiolSKU(emmiol_prod)
            sku.set_color(color)
            sku.price = emmiol_prod.get_price()
            sku.set_detail_url(color_detail)
            sku.add_images(image_list)

            li_class_attr = li_unit.get('class')
            if li_class_attr.find('selected') != -1:
                select_color = color
                select_sku = sku
            else:
                next_detail_urls.append(color_detail)

            sku_list.append(sku)
            color_list.append(color)

        size_data = html_text.xpath('//ul[@class="choose_list choose_list_inline"]')
        li_list = size_data[0].xpath("li")
        for li_unit in li_list:
            a_unit = li_unit.getchildren()[0]
            size = a_unit.get("title")
            size_detail = a_unit.get('data-url')

            li_class_attr = li_unit.get("class")
            print("select:%s" % (li_class_attr))
            if li_class_attr.find('selected') != -1:
                select_size = size
                select_sku.set_size(size)
                #select_sku.add_images(image_list)
            else:
                sku = EmmiolSKU(emmiol_prod)
                sku.set_size(size)
                sku.set_color(select_color)
                sku.price = emmiol_prod.get_price()
                sku.set_detail_url(size_detail)
                sku.add_images(image_list)
                sku_list.append(sku)

            size_list.append(size)
        print("color_list:%s, size_list:%s" % (color_list, size_list))

        for sku in sku_list:
            if sku.size == '':
                sku.set_size(select_size)

        
        for size in size_list:
            for color in color_list:
                if size == select_size or color == select_color:
                    continue
                else:
                    sku = EmmiolSKU(emmiol_prod)
                    sku.set_size(size)
                    sku.set_color(color)
                    sku_list.append(sku)
        print(len(sku_list))
        print("\r\n".join([json.dumps(sku.toDumpJSON()) for sku in sku_list]))
        return sku_list, next_detail_urls 

    def get_image_list(self, html_text, emmiol_prod):
        div_data = html_text.xpath('//div[@class="swiper-slide"]')
        spu_code = emmiol_prod.spu_code
        image_list = []
        for div_unit in div_data:
            img_list = div_unit.xpath("img")
            for img_unit in img_list:
                src = img_unit.get('src')
                if src is None:
                    continue
                if src.find("img_original-%s" % (spu_code)) != -1:
                    image = EmmiolImage(emmiol_prod)
                    image.src = src
                    image.set_image_id(src)
                    emmiol_prod.add_image(image)
                    image_list.append(image)

        return image_list 

    def run(self):
        self.init_conf()
        self.url_spider()
        local_file_path = "../data/emmiol_%s_newtmp.json" % (self.date)
        local_file = open(local_file_path, 'w')
        local_file.write(json.dumps([unit.toDumpJSON() for unit in self.prod_list.values()]))
        local_file.close()
        
if __name__ == "__main__":
    debug = sys.argv[1]
    EmmiolSpider(debug).run()


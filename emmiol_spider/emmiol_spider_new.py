# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
from entity.ProductEntity import Image, Product, Variant
from entity.EmmiolProdEntity import EmmiolProdEntity, EmmiolSKU, EmmiolImage, EmmiolDesc
from common import utils
import codecs
import json
import time
import datetime
from lxml import etree
from common import myRequest
import requests
import random
from common import emmiol_common as ec
from sqlalchemy import create_engine





class EmmiolSpider(object):
    def __init__(self, debug):
        self.req = myRequest.MyRequest()
        self.date = datetime.date.today().strftime("%Y%m%d")
        self.debug = debug
        self.prod_list = {}

    def init_db(self):
        self.engine = create_engine("mysql+pymysql://admin:JYFscvVevsANJYmrz9Ud@infwaves.c0rck9lj4qyp.us-east-2.rds.amazonaws.com:3306/erp_product_analysis", max_overflow=5)

    def get_item_list_data(self, text):
        text_split = text.split(";")
        data_json_str = ""
        for unit in text_split:
            if unit.find('googleGoodsItem') != -1:
                data_json_str = unit.split('=')[1]
            data_json = []
            try:
                data_json = json.loads(data_json_str)
            except:
                data_json = []
        return data_json

    def get_detail_url(self, html_text, sku_code):
        div_data = html_text.xpath('//div[@data-goods-id="%s"]' % sku_code)
        div_unit = div_data[0]
        detail_url = div_unit.get('data-href')
        return detail_url

    def get_first_image_url(self, html_text, sku_code):
        img_unit = html_text.xpath('//img[@data-sku="%s"]'%(sku_code))
        first_image = img_unit[0].get('data-original')
        return first_image

    def data_transform(self, item_unit):
        new_unit = {}
        spu_code = ec.extract_spu_code(item_unit.get('item_id'))
        new_unit['spu_code'] = spu_code
        new_unit['price'] = item_unit.get('price')
        new_unit['cat1'] = item_unit.get('item_list_name').split('>')[0].strip() if len(item_unit.get('item_list_name').split('>')) >= 1 else ''
        new_unit['cat2'] = item_unit.get('item_list_name').split('>')[1].strip() if len(item_unit.get('item_list_name').split('>')) >= 2 else ''
        new_unit['cat3'] = item_unit.get('item_list_name').split('>')[2].strip() if len(item_unit.get('item_list_name').split('>')) >= 3 else ''
        new_unit['cat4'] = item_unit.get('item_list_name').split('>')[3].strip() if len(item_unit.get('item_list_name').split('>')) >= 4 else ''
        new_unit['original_cat_id'] = item_unit.get('item_list_id')
        new_unit['detail_url'] = item_unit.get('detail_url')
        new_unit['color'] = item_unit.get('item_variant').split('/')[0]
        new_unit['size'] = item_unit.get('item_variant').split('/')[1]
        new_unit['first_image'] = item_unit.get('first_image')
        new_unit['item_name'] = item_unit.get('item_name')
        random.seed(spu_code)
        spu_id = int(random.random() * 1000000)
        new_unit['spu_id'] = spu_id
        return new_unit

    def run(self):
        self.init_db()
        url_part = "new-products" 
        index = 1
        while True:
            req_url = "https://www.emmiol.com/%s-page-%s.html" % (url_part, index)
            print("start spider: %s" % req_url)
            while True:
                resp = self.req.sync_get(req_url)
                if resp is not None:
                    break
            print("finish spider:%s" % req_url)
            data_json = self.get_item_list_data(resp.text)
            if len(data_json) == 0:
                break
            html_text = etree.HTML(resp.text) 
            prod_list = []
            for unit in data_json:
                new_unit = {}
                item_id = unit.get('item_id')
                detail_url = self.get_detail_url(html_text, item_id)
                first_image = self.get_first_image_url(html_text, item_id)
                unit["detail_url"] = detail_url
                unit["first_image"] = first_image
                new_unit = self.data_transform(unit)
                print(new_unit.get('cat1').lower())
                if new_unit.get('cat1').lower() == 'women':
                    prod_list.append(new_unit)
            print(prod_list)
            if len(prod_list) != 0:
                self.update_data(prod_list)
            else:
                print("new page:%s, empty" % (index))
            index = index + 1

    def update_data(self, data_json):
        insert_sql = "insert ignore erp_product_analysis.spider_prod (source_type, date, spu_code, detail_url, detail_info, status)values"
        value_str_list = []
        for unit in data_json:
            source_type = 'emmiol'
            date = self.date
            spu_code = unit.get('spu_code')
            detail_url = unit.get('detail_url')
            detail_info = json.dumps(unit).replace("'", "\\'")
            value_str = "('%s',%s,'%s', '%s', '%s', 0)" % (source_type, date, spu_code, detail_url, detail_info)
            value_str_list.append(value_str)
        value = ",".join(value_str_list)
        insert_sql = "%s %s;" % (insert_sql, value)
        print(insert_sql)
        cur = self.engine.execute(insert_sql)


if __name__ == "__main__":
    debug = sys.argv[1]
    EmmiolSpider(debug).run()


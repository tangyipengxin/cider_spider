# encoding=utf-8

import sys
sys.path.insert(0, './')
sys.path.insert(0, '../')

from common import emmiol_common as ec
import json
import requests
import time









if __name__ == '__main__':
    start_time = time.time()
    with open("../data/emmiol_all_prod_20210519.json", 'r') as f:
        data = f.read()
        data_json = json.loads(data)
    data_json = data_json
    size_data = {}
    for unit in data_json:
        detail_url = unit.get('detail_url')
        spu_code = unit.get('spu_code')
        sku_id = ec.extract_sku_id(detail_url)
        size_info = ec.get_size_info(sku_id)
        unit = {"spu_code": spu_code, "size_info": size_info, "detail_url": detail_url} 
        size_data[spu_code] = unit
        print(unit)

    end_time = time.time()
    print("start_time:%s, end_time:%s, total_time:%s, avg_time:%s" % (start_time, end_time, end_time- start_time, (end_time - start_time) / len(data_json)))
    with open("../data/emmiol/emmiol_size_total_20210523.json", "w") as f:
        f.write(json.dumps(list(size_data.values())))


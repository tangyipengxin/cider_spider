# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
from entity.ProductEntity import Image, Product, Variant
from entity.EmmiolProdEntity import EmmiolProdEntity, EmmiolSKU, EmmiolImage, EmmiolDesc
from entity.ShelfProdEntity import ProdSPU
from common import utils
import codecs
import json
import time
import datetime
from lxml import etree
from common import myRequest
import requests
import random

from common import emmiol_common as ec
from sqlalchemy import create_engine
from sqlalchemy import text

from elasticsearch import Elasticsearch
from elasticsearch import helpers


class EmmiolSpider(object):
    def __init__(self, spu_code):
        self.req = myRequest.MyRequest()
        self.date = datetime.date.today().strftime("%Y%m%d")
        self.prod_list = {}
        self.spu_code = spu_code


    def init_db(self):
        self.engine = create_engine("mysql+pymysql://admin:JYFscvVevsANJYmrz9Ud@infwaves.c0rck9lj4qyp.us-east-2.rds.amazonaws.com:3306/erp_product_analysis", max_overflow=5)
        self.db_conn = self.engine.connect()
        region_name = 'us-east-1'
        aws_access_key_id = "AKIAWN4RN6OCPEDDK7PX"
        aws_secret_access_key = "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"
        self.s3 = utils.init_s3(region_name, aws_access_key_id, aws_secret_access_key)
    
    def extract_detail(self, list_item_info):
        spu_code = list_item_info.get('spu_code')
        detail_url = list_item_info.get('detail_url')
        print("spider: %s start, spu_code:%s" % (detail_url, spu_code))
        while True:
            resp = requests.get(detail_url)
            if resp is not None:
                break

        detail_info_list = []
        html_text = etree.HTML(resp.text)
        color_info = ec.get_color_info(html_text)
        for color_info_unit in color_info:
            record = {}
            color = color_info_unit.get('color')
            color_detail_url = color_info_unit.get('detail_url')
            if color_detail_url != detail_url:
                while True:
                    resp = requests.get(color_detail_url)
                    if resp is not None:
                        break 
                other_html_text = etree.HTML(resp.text)
                record['color'] = color
                record['color_detail_url'] = color_detail_url
                record['html_text'] = other_html_text
            else:
                record['color'] = color
                record['color_detail_url'] = color_detail_url
                record['html_text'] = html_text
            detail_info_list.append(record)

        if len(detail_info_list) == 0:
            print("spu:%s not color" % (spu_code))
            return list_item_info.get('date'), None

        emmiol_prod = self.gene_emmiol_prod(list_item_info, detail_info_list)
        return list_item_info.get('date'), emmiol_prod

    def gene_emmiol_prod(self, list_item_info, detail_info_dict):
        prod = EmmiolProdEntity()
        spu_code = list_item_info.get('spu_code')
        prod.spu_code = list_item_info.get('spu_code')
        prod.init_spu_id(prod.spu_code)
        prod.title = list_item_info.get('item_name')

        prod.spu_name = list_item_info.get('item_name')
        prod.cat1 = list_item_info.get('cat1').strip()
        prod.cat2 = list_item_info.get('cat2').strip()
        prod.cat3 = list_item_info.get('cat3').strip()
        prod.cat4 = list_item_info.get('cat4').strip()
        prod.original_cat_id = list_item_info.get('original_cat_id')
        result = ec.category_mapping(prod.original_cat_id)
        prod.standard_cat1_id = result.get('cat1_id')
        prod.standard_cat2_id = result.get('cat2_id')
        prod.standard_cat3_id = result.get('cat3_id')
        prod.standard_cat4_id = result.get('cat4_id')
        prod.standard_cat1 = result.get('cat1_name')
        prod.standard_cat2 = result.get('cat2_name')
        prod.standard_cat3 = result.get('cat3_name')
        prod.standard_cat4 = result.get('cat4_name')
        prod.detail_url = list_item_info.get('detail_url')

        single_html_text = detail_info_dict[0].get('html_text')
        prod.description = EmmiolDesc()
        sku_id = ec.extract_sku_id(list_item_info.get('detail_url'))
        prod.description.size = ec.get_size_table(sku_id)
        prod.description.return_and_shipping = ec.get_return_and_shipping(single_html_text)
        prod.description.detail = ec.get_detail(single_html_text)
        prod.description.preSale = ec.parse_pre_sale(single_html_text)

        prod.tags = ec.parse_tags(single_html_text)

        prod.created_at = utils.toTimestamp(str(list_item_info.get('date')))

        size_info = ec.get_size_info(single_html_text)
        color_info = ec.get_color_info(single_html_text)

        first_image = list_item_info.get('first_image')
        match_str_list = []
        if first_image != "":
            image_match_str = first_image.split("?")[0].split("/")[-1].split("-")[0]
            match_str_list.append(image_match_str)
        match_str_list.append(spu_code)


        index = 1
        for color_info_unit in color_info:
            color = color_info_unit.get('color')
            detail_info_unit = {}
            for unit in detail_info_dict:
                if unit.get('color').lower() == color.lower():
                    detail_info_unit = unit
            html_text = detail_info_unit.get('html_text')
            image_list = ec.get_image_list(html_text, match_str_list)
            image_obj_list = []
            for image_src in image_list:
                image = EmmiolImage(prod)
                image.src = image_src
                image.position = index
                image.set_image_id(image_src)
                prod.images.append(image)
                image_obj_list.append(image)

            size_info = ec.get_size_info(html_text)
            for size_unit in size_info:
                size = size_unit.get('size')
                sku = EmmiolSKU(prod)
                sku.color = color
                sku.size = size
                sku.init_sku_name()
                sku.detail_url = list_item_info.get('detail_url')
                sku.price = list_item_info.get('price') 
                sku.images = image_obj_list
                print("sku len:%s" % len(image_obj_list))
                prod.sku_list.append(sku)

        prod.default_image = EmmiolImage(prod)
        prod.default_image.src = list_item_info.get('first_image')

        prod.init_sku_code()

        for image in prod.images:
            image.init_aws_url(self.s3)
        prod.default_image.init_aws_url(self.s3)

        return prod



    def get_spider_list(self):
        if self.spu_code == 'all':
            sql = "select source_type, date, spu_code, detail_url, detail_info, status from erp_product_analysis.spider_prod where status=0 and source_type='emmiol'"
        else:
            sql = "select source_type, date, spu_code, detail_url, detail_info, status from erp_product_analysis.spider_prod where source_type='emmiol' and spu_code='%s'" % (self.spu_code)
        cursor = self.engine.execute(sql)
        res = cursor.fetchall()
        field_list = ['source_type', 'date', 'spu_code', 'detail_url', 'detail_info', 'status']
        records = []
        for row in res:
            index = 0
            res_dict = {}
            for field in field_list:
                res_dict[field] = row[index]
                index = index + 1
            record = json.loads(res_dict.get('detail_info'))
            record['date'] = res_dict.get('date')
            records.append(record)

        return records

    def update_sql_data(self, data_json):
        insert_sql = 'replace into erp_product_analysis.spider_prod_detail (date, spu_code, prod_detail, status)values'
        value_str_list = []
        for unit in data_json:
            date = unit.get('date')
            prod_unit = unit.get('prod')

            spu_code = prod_unit.get('spu_code')
            status = 0
            detail_str = json.dumps(prod_unit)

            detail_str = detail_str.replace("'", "\\'")
            detail_str = detail_str.replace('\\"', '')
            detail_str = detail_str.replace('\\n', '')

            detail_str = detail_str.replace("%", '%%')
            value_str = "(%s, '%s', '%s', %s)" % (self.date, spu_code, detail_str, status)
            value_str_list.append(value_str)
        value_str_res = ",".join(value_str_list)
        insert_sql = "%s %s"% (insert_sql, value_str_res)
        print(insert_sql)
        self.engine.execute(insert_sql)


    def update_es_data(self, prod_list):
        dest_prod_list = [] 
        for prod in prod_list:
            prod_spu = ProdSPU().fromJSON(prod.toShelfProdJson())
            dest_prod_list.append(prod_spu)
        index = 0
        interval = 10
        while index * interval < len(dest_prod_list):
            sub_prod_list = dest_prod_list[index * interval: index * interval + interval]
            action = []
            es_doc_ids = []
            for prod_unit in sub_prod_list:
                es_doc_id, es_json = prod_unit.toESJSON()
                status = self.es.exists(index='idx_competitor_product', id=es_doc_id)
                if status == True:
                    action.append({"_index": "idx_competitor_product","_id": es_doc_id, "doc": es_json, "_op_type": "update"})
                else:
                    action.append({"_index": "idx_competitor_product","_id": es_doc_id, "_source": es_json, "_op_type": "create"})
            print(action)
            helpers.bulk(self.es, action)
            index = index + 1

    def update_store_status(self, data_json):
        update_result = {}
        for unit in data_json:
            status = unit.get('status')
            spu_code = unit.get('spu_code')
            if status not in update_result:
                update_result[status] = []
            update_result[status].append(spu_code)

        for status, spu_code_list in update_result.items():
            update_sql = "update erp_product_analysis.spider_prod set status=%s where spu_code in (%s)" % (status, ",".join(["'%s'" % unit for unit in spu_code_list]))
            print(update_sql)
        cursor = self.db_conn.execute(update_sql)



    def close_db(self):
        if self.conn is not None:
            self.conn.close()

    def run(self):
        self.init_db()
        list_item_list = self.get_spider_list()
        spider_res = []
        update_res = []
        interval = 10
        for unit in list_item_list:
            date, prod = self.extract_detail(unit)
            if prod is not None:
                record = {"date": date, "prod": prod.toDumpJSON(), "status": 1, "spu_code": unit.get("spu_code")}
                spider_res.append(record)
                update_res.append(record)
                if len(update_res) >= interval:
                    self.update_sql_data(update_res)
                    self.update_store_status(update_res)
                    update_res = []
            else:
                record = {"date": date, "prod": None, "status": 9, "spu_code": unit.get('spu_code')}
                self.update_store_status([record])
                pass

        if len(update_res) != 0:
            self.update_sql_data(update_res)
            self.update_store_status(update_res)


if __name__ == "__main__":
    spu_code = sys.argv[1]
    EmmiolSpider(spu_code).run()

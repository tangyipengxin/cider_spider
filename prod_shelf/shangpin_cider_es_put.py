# encoding=utf-8
import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')


from elasticsearch import Elasticsearch
from elasticsearch import helpers
import sys
import json
from entity.EmmiolProdEntity import EmmiolProdEntity, EmmiolSKU, EmmiolImage, EmmiolDesc
from entity.ShelfProdEntity import ProdSPU
from entity.ProductEntity import Product
import requests
from common import utils
from common import emmiol_common as ec


class ProdPutES(object):
    def __init__(self, date, site):
        self.es = None
        self.s3 = None
        self.date = date
        self.site = site
        self.debug = debug
        self.prod_list = []
        self.cat_id_mapping = {
                'crop tops':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
                'tops':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
'knitwear':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2216,'cat4_id':2217},
't-shirts':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':1738},
'blouses':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':1733},
'sweatshirts':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1773,'cat4_id':None},
'skirts':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1732},
'shorts':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1912},
'trousers':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':None},
'jackets':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2037,'cat4_id':None},
'blazers':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2036,'cat4_id':1739},
'dresses':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
'denim':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':None},
'outerwear':{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2037,'cat4_id':1735},
'bottoms':{'cat1_id':2030, 'cat2_id':2059,'cat3_id':2571,'cat4_id':None},
'':{'cat1_id':2030, 'cat2_id':None,'cat3_id':None,'cat4_id':None},
                }
    

    def init_info(self):
        if self.debug == 'true':
            self.es = Elasticsearch('3.138.153.109:9200')
        else:
            self.es = Elasticsearch('3.138.153.109:19200')
        region_name = 'us-east-1'
        aws_access_key_id = "AKIAWN4RN6OCPEDDK7PX"
        aws_secret_access_key = "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"
        self.s3 = utils.init_s3(region_name, aws_access_key_id, aws_secret_access_key)

    def init_data(self):
        with open("../data/%s_%s.json" % (self.site, self.date), 'r') as f:
            data_json_str = f.read()

        data_json = json.loads(data_json_str)
        data_json = data_json
        for unit in data_json:
            try:
                prod = Product().fromLoadJSON(unit)
            except:
                continue
            prod.site = self.site
            cat1 = prod.cat1
            cat2 = prod.cat2
            if cat1 == 'accessories':
                continue
            if self.site == 'cider':
                if cat2.strip() != '' and cat2 is not None:
                    shein_cat_inf = self.cat_id_mapping.get(cat2.lower().strip())
                elif cat1.strip() == '' or cat1.lower() in ('matching-set'):
                    shein_cat_inf = {'cat1_id':2030, 'cat2_id':None,'cat3_id':None,'cat4_id':None}
                else:
                    shein_cat_inf = self.cat_id_mapping.get(cat1.lower().strip())
                cat1_id = shein_cat_inf.get('cat1_id')
                cat2_id = shein_cat_inf.get('cat2_id')
                cat3_id = shein_cat_inf.get('cat3_id')
                cat4_id = shein_cat_inf.get('cat4_id')
                prod.standard_cat1_id = cat1_id
                prod.standard_cat2_id = cat2_id
                prod.standard_cat3_id = cat3_id
                prod.standard_cat4_id = cat4_id
            self.init_aws_url(self.s3, prod)
            prod_spu = ProdSPU().fromJSON(prod.toShelfProdJson())
            for sku in prod_spu.skuList:
                sku.sellPrice = round(sku.sellPrice * 1.22, 2)
                sku.msrp = sku.sellPrice 
            self.prod_list.append(prod_spu)

    def init_aws_url(self, s3, prod):
        for image in prod.images:
            image.init_aws_url(s3)
       
    def es_write(self, prod_list):
        action = []
        es_doc_ids = []
        actions = []
        for prod_unit in prod_list:
            try:
                es_doc_id, es_json = prod_unit.toESJSON()
            except:
                continue
            status = self.es.exists(index='idx_competitor_product', id=es_doc_id)
            print(es_json)
            if status == True:
                action.append({"_index": "idx_competitor_product","_id": es_doc_id, "doc": es_json, "_op_type": "update"})
            else:
                action.append({"_index": "idx_competitor_product","_id": es_doc_id, "_source": es_json, "_op_type": "create"})
        
            
        print(action)
        helpers.bulk(self.es, action)

    def run(self):
        self.init_info()
        self.init_data()
        index = 0 
        interval = 10
        self.prod_list = self.prod_list
        while index * interval < len(self.prod_list):
            print(index)
            self.es_write(self.prod_list[index * interval: index * interval + interval])
            index = index + 1


if __name__ == '__main__':
    site = sys.argv[1]
    date = sys.argv[2]
    debug = sys.argv[3]
    ProdPutES(date, site, debug).run()
    

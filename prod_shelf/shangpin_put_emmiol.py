# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
from entity.ProductEntity import Image, Product, Variant
from entity.EmmiolProdEntity import EmmiolProdEntity, EmmiolSKU, EmmiolImage, EmmiolDesc
from entity.ShelfProdEntity import ProdSPU
from common import utils
import codecs
import json
import time
import datetime
from lxml import etree
from common import myRequest
import requests
import random

from common import emmiol_common as ec
from sqlalchemy import create_engine
from sqlalchemy import text

from elasticsearch import Elasticsearch
from elasticsearch import helpers


class EmmiolSpider(object):
    def __init__(self, spu_code, debug):
        self.debug = debug
        self.spu_code = spu_code
        self.prod_list = {}


    def init_db(self):
        self.engine = create_engine("mysql+pymysql://admin:JYFscvVevsANJYmrz9Ud@infwaves.c0rck9lj4qyp.us-east-2.rds.amazonaws.com:3306/erp_product_analysis", max_overflow=5)
        self.db_conn = self.engine.connect()
        if self.debug == 'true':
            self.es = Elasticsearch('3.138.153.109:9200')
        else:
            self.es = Elasticsearch('3.138.153.109:19200')

    def get_spider_list(self):
        if self.spu_code == 'all':
            sql = "select source_type, date, spu_code, prod_detail, status from erp_product_analysis.spider_prod_detail where status=0 and source_type='emmiol'"
        else:
            sql = "select source_type, date, spu_code, prod_detail, status from erp_product_analysis.spider_prod_detail where status=0 and source_type='emmiol' and spu_code='%s'" % (self.spu_code)
        cursor = self.engine.execute(sql)
        res = cursor.fetchall()
        field_list = ['source_type', 'date', 'spu_code', 'prod_detail', 'status']
        records = []
        for row in res:
            index = 0
            res_dict = {}
            for field in field_list:
                res_dict[field] = row[index]
                index = index + 1
            res_dict['prod'] = EmmiolProdEntity().fromJSON(json.loads(res_dict.get('prod_detail')))
            records.append(res_dict)
        return records


    def update_es_data(self, record_list):
        action = []
        es_doc_ids = []
        for record in record_list:
            prod_unit = record.get('prod')
            es_doc_id, es_json = prod_unit.toESJSON()
            status = self.es.exists(index='idx_competitor_product', id=es_doc_id)
            if status == True:
                action.append({"_index": "idx_competitor_product","_id": es_doc_id, "doc": es_json, "_op_type": "update"})
            else:
                action.append({"_index": "idx_competitor_product","_id": es_doc_id, "_source": es_json, "_op_type": "create"})
        print(action)
        helpers.bulk(self.es, action)

    def update_store_status(self, data_json):
        update_result = {}
        for unit in data_json:
            status = unit.get('status')
            spu_code = unit.get('spu_code')
            if status not in update_result:
                update_result[status] = []
            update_result[status].append(spu_code)

        for status, spu_code_list in update_result.items():
            update_sql = "update erp_product_analysis.spider_prod_detail set status=%s where spu_code in (%s)" % (status, ",".join(["'%s'" % unit for unit in spu_code_list]))
            print(update_sql)
        cursor = self.db_conn.execute(update_sql)



    def close_db(self):
        if self.conn is not None:
            self.conn.close()

    def run(self):
        self.init_db()
        prod_json_list = self.get_spider_list()
        spider_res = []
        update_res = []
        interval = 10
        if self.debug == 'true':
            prod_json_list = prod_json_list[0:2]
        print(len(prod_json_list))
        for unit in prod_json_list:
            prod = unit.get('prod')
            prod_spu = ProdSPU().fromJSON(prod.toShelfProdJson())
            record = {"date": unit.get('date'), "prod": prod_spu, "status": 1, "spu_code": unit.get("spu_code")}
            spider_res.append(record)
            update_res.append(record)
            if len(update_res) >= interval:
                self.update_es_data(update_res)
                #self.update_store_status(update_res)
                update_res = []

        if len(update_res) != 0:
            print(2222)
            self.update_es_data(update_res)
            #self.update_store_status(update_res)


if __name__ == "__main__":
    spu_code = sys.argv[1]
    debug = sys.argv[2]
    EmmiolSpider(spu_code, debug).run()

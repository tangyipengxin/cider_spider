# encoding=utf-8
import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
import requests
import time
import json
from entity.ProductEntity import Product
from entity.ShelfProdEntity import ProdSPU, ProdSKU
from entity.EmmiolProdEntity import EmmiolProdEntity, EmmiolImage
import os
from datetime import datetime, timedelta
from lxml import etree
import shein_category
from sqlalchemy import create_engine

from common import emmiol_common as ec
from elasticsearch import Elasticsearch
from elasticsearch import helpers


class ProdShelf(object):
    def __init__(self, spu_code, debug):
        self.spu_code = spu_code
        self.debug = debug
        self.prod_list = []
        self.spu_mapping = {}

    def init_conf(self): 
        self.menu_conf = {
            1930: ['SIZE', 'US', 'BUST', 'HIPS'],
            2182: ["SIZE", "US", "WAIST", "HIPS"],
            2183: ["SIZE", "US", "WAIST", "BUST", "HIPS"],
            2181: ["SIZE", 'US', "WAIST", "BUST", "CUP SIZE"],
            2192: ["SIZE", "US", "WAIST", "BUST", "CUP SIZE"],
            2191: ["SIZE", "US", "WAIST", "BUST", "CUP SIZE"],
            3024: ["SIZE", "US", "WAIST", "BUST" ],
            1882: ["SIZE", "US", "WAIST", "BUST", "HIPS", "CUP SIZE"],
            1767: ["SIZE", "US", "WAIST", "HIPS"],
            2037: ["SIZE", "US", "WAIST", "BUST"],
            2040: ["SIZE", "US", "WAIST", "BUST", "HIPS"],
            1930: ["SIZE", "US", "WAIST", "BUST", "HIPS"],
            1727: ["SIZE", "US", "WAIST", "BUST", "HIPS"],
            1860: ["SIZE", "US", "WAIST", "BUST", "HIPS"],
            2169: ["SIZE", "US", "WAIST", "BUST", "HIPS"],
            2036: ["SIZE", "US", "WAIST", "BUST", "HIPS"],
            2216: ["SIZE", "US", "WAIST", "BUST"],
            1773: ["SIZE", "US", "WAIST", "BUST"],
            1766: ["SIZE", "US", "WAIST", "BUST"],
            2195: ["SIZE", "US", "WAIST", "BUST", "HIPS", "CUP SIZE"],
            2202: ["SIZE", "US", "WAIST", "BUST", "HIPS", "CUP SIZE"],
            2333: ["SIZE", "US", "WAIST", "BUST", "CUP SIZE"],
            2332: ["SIZE", "US", "WAIST", "BUST", "CUP SIZE"]
        }
        with open('../data_conf/spu_number_mapping.conf', 'r') as f:
            spu_number_mappings = f.readlines()
            for spu_unit in spu_number_mappings:
                self.spu_mapping[spu_unit.strip().split(' ')[1].strip()] = spu_unit.strip().split(' ')[0].strip()

    def init_db(self):
        self.engine = create_engine("mysql+pymysql://admin:JYFscvVevsANJYmrz9Ud@infwaves.c0rck9lj4qyp.us-east-2.rds.amazonaws.com:3306/erp_product_analysis", max_overflow=5)
        self.db_conn = self.engine.connect()
        if self.debug == 'true':
            self.es = Elasticsearch('3.138.153.109:9200')
        else:
            self.es = Elasticsearch('3.138.153.109:19200')

    def init_prod_list(self):
        sql = "select date, spu_code, prod_detail from erp_product_analysis.spider_prod_detail where status = 0 and source_type='emmiol'"
        cursor = self.db_conn.execute(sql)
        res = cursor.fetchall()
        records = []
        for row in res:
            record = {}
            record['date'] = row[0]
            record['spu_code'] = row[1]
            record['prod_detail'] = row[2]
            records.append(record)
        return records

    def update_image_position(self, prod):
        images = prod.images
        for image in images:
            image.variantIds = prod.get_image_variantids(image.image_id)
            index = 1
            for image in prod.images:
                image.position = index
                index = index + 1
    def update_size_table(self, prod):
        if len(prod.sku_list) <= 1:
            return 
        else:
            detail_url = prod.sku_list[0].detail_url
            origin_sku_id = detail_url.split('product')[1].replace('.html', '')
            size_table = ec.get_size_table(origin_sku_id)
            menu = size_table.get('menu')
            size_list = size_table.get('size_list')
            default_menu = ['SIZE', 'US', 'BUST', 'WAIST', 'HIPS', 'CUP SIZE']
            match = True
            if len(menu) != 6:
                match = False
            else:
                for index in range(len(default_menu)):
                    if default_menu[index] != menu[index]:
                        match = False
            new_menu = []
            if match == True:
                dest_menu = menu_conf.get(standard_cat3_id) 
                if dest_menu is None:
                    print("not menu:%s" % standard_cat3_id)
                index_list = []
                for index in range(len(default_menu)): 
                    menu_unit = menu[index]
                    if menu_unit in dest_menu:
                        index_list.append(index)
                print(index_list)

                for index in index_list:
                    new_menu.append(menu[index])        
                new_size_info = {}
                for key, value in size_list[0].items():
                    size_index = 0
                    new_size_list = []
                    for size_unit in value:
                        new_size_unit = []
                        for index in index_list:
                            new_size_unit.append(value[size_index][index])
                        size_index = size_index + 1
                        new_size_list.append(new_size_unit)
                    new_size_info[key] = new_size_list
                size_table = {"menu": new_menu, "size_list": [new_size_info]}
                prod.description.size = size_table
            else:
                pass



    def init_data(self, records):
        update_prod_list = []
        for record in records:
            date = record.get('date')
            spu_code = record.get('spu_code')
            prod_detail = json.loads(record.get('prod_detail'))
            prod = EmmiolProdEntity().fromJSON(prod_detail)
            self.update_image_position(prod)

            prod_spu = ProdSPU().fromJSON(prod.toShelfProdJson())

            print(prod_spu.productType)
                    
            prod_spu.number = prod_spu.number if self.spu_mapping.get(spu_code) is None else self.spu_mapping.get(spu_code)
            update_prod_list.append(prod_spu)
            if len(update_prod_list)  > 10:
                update_prod_list = []

    def run(self):
        self.init_db()
        self.init_conf()
        records = self.init_prod_list()

        self.init_data(records)

        

if __name__ == '__main__':
    spu_code = sys.argv[1]
    debug = sys.argv[2]
    ProdShelf(spu_code, debug).run() 

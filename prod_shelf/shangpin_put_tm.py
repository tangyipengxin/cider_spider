# encoding=utf-8
import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
import requests
import time
import json
from entity.ProductEntity import Product
from entity.ShelfProdEntity import ProdSPU, ProdSKU
import os
from datetime import datetime, timedelta


class ProdShelf(object):
    def __init__(self):
        self.cur_date = datetime.today().strftime("%Y%m%d")
        self.last_date = (datetime.today() + timedelta(-1)).strftime("%Y%m%d")
        self.cur_date = '20210421'
        self.last_date = '20210401'
        print(self.cur_date)
        print(self.last_date)
        self.debug = False

    def init_black_list(self):
        infringe_path = '../conf/cider_infringe_blacklist.conf'
        infringe_file = open(infringe_path, 'r')
        lines = infringe_file.readlines()
        infringe_file.close()
        self.prod_id_black_list = [unit.strip() for unit in lines]

    def init_white_list(self):
        white_path = '../conf/cider_whitelist.conf'
        white_file = open(white_path, 'r')
        lines = white_file.readlines()
        white_file.close()
        self.prod_id_white_list = [unit.strip() for unit in lines]

    def send_add_req(self, record_list):
        headers = {
            # "VERSION": "1",
            "Content-Type": "application/json",
        }
        #url = 'http://127.0.0.1:9999/product/product/up'
        url = 'http://3.142.144.47:9999/product/product/up'
        for record in record_list:
            spu = record.get('spu')
            data = json.dumps(spu.toJson())
            status_code, text, cost_time = self.send_req(headers, url, data)
            #status_code, text, cost_time = (200, '', 10)
            result = {}
            print(("status:%s, text:%s, cost_time:%s, spu:%s" % (status_code, text, cost_time, data)).encode(encoding='utf-8'))
            record['status_code'] = status_code
            record['text'] = text
            record['cost_time'] = cost_time
            if cost_time < 1:
                time.sleep(2)
        return record_list


    def send_error_msg(self, results):
        text_list = []
        spu_count = 0
        for result in results:
            status_code = result['status_code']
            if status_code in  [200, 408]:
                continue
            spu = result['spu']
            spu_count = spu_count + 1
            text = "status_code:%s, handle:%s" % (status_code, spu.handle)
            text_list.append(text)

        headers = {
            "Content-Type": "application/json"
        }
        url = 'https://open.feishu.cn/open-apis/bot/v2/hook/0a638cee-06cb-4a50-938e-7f64fbe99e30'
        spu_info_str = "[NEW ERROR MESSAGE]\r\nspu count:%s,  date:%s \r\n\r\n%s" % (spu_count, self.cur_date, "\r\n".join(text_list))
        data = json.dumps({"msg_type": "text", "content": {"text": spu_info_str}})
        #print(data)
        status, text, cost_time = self.send_req(headers, url, data)
        #print("status:%s, text:%s, cost_time:%s" % (status, text, cost_time))

    def send_msg(self, msg_str):
        headers = {
            "Content-Type": "application/json"
        }
        url = 'https://open.feishu.cn/open-apis/bot/v2/hook/0a638cee-06cb-4a50-938e-7f64fbe99e30'
        #url = 'https://open.feishu.cn/open-apis/bot/v2/hook/2fb2fab9-0a06-4581-bc81-f24e38ec7786'
        data = json.dumps({"msg_type": "text", "content": {"text": msg_str}})
        status, text, cost_time = self.send_req(headers, url, data)
        #print("status:%s, text:%s, cost_time:%s" % (status, text, cost_time))

    def send_req(self, headers, url, data, repeat = 1):
        for index in range(repeat):
            start = time.time()
            resp = requests.post(url, headers=headers, data=data)
            end = time.time()
            status = resp.status_code
            text = resp.text
            cost_time = end - start
            if status == 200:
                break
        return status, text, cost_time

    def send_update_req(self, spu_list):
        headers = {
            # "VERSION": "1",
            "Content-Type": "application/json",
        }
        url = 'http://127.0.0.1:9999/product/product/update'
        for spu in spu_list:
            data = spu.toJson()
            resp = requests.post(url, headers=headers, data=json.dumps(data))
            #end = time.time()
            #print(resp.status_code)
            #print(resp.text.encode(encoding='utf-8'))
            #print("cost:%s" % (end -start))
            #if end - start < 1:
            #    time.sleep(2)


    def loadProdData(self, data_file_path):
        prodSPUList = {}
        if os.path.exists(data_file_path) == False:
            return prodSPUList

        data_file = open(data_file_path, 'r')
        data_json_str = data_file.read()
        data_file.close()

        data_json = json.loads(data_json_str)
        for product_json in data_json:
            product = Product().fromLoadJSON(product_json)
            if product.product_type.lower() != 'accessories' and product.isValid() == True:
                prodSPU = ProdSPU().fromSpiderData(product) 
                if product.handle in self.prod_id_black_list:
                    continue
                prodSPU.status = 'draft'
                prodSPUList[prodSPU.number] = prodSPU
        return prodSPUList

    def extractSPUList(self, cur_spus, last_spus):
        new_prod_list = []
        update_prod_list = []
        review_prod_list = []
        for product_id, spu in cur_spus.items():
            if product_id not in last_spus:
                new_prod_list.append({"spu": spu})
            else:
                last_spu = last_spus.get(product_id)
                status, reason, reason_type = spu.compareUpdateDiff(last_spu)
                if status == False:
                    update_prod_list.append({"spu": spu, 'reason': reason, 'reason_type': reason_type})

        return new_prod_list, update_prod_list, []

    def get_new_msg(self, results):
        text_list = []
        spu_count = 0
        image_count = 0
        for result in results:
            status_code = result['status_code']
            if status_code not in  [200, 408]:
                continue
            spu = result['spu']
            spu_count = spu_count + 1
            images = spu.images
            if len(images) == 0:
                text = "%s\t no image" % (spu.handle)
                text_list.append(text)
            else:
                for image in images:
                    image_count = image_count + 1
                    text = "new_spu\t\t%s\t%s" % (spu.handle, image.src)
                    text_list.append(text)
	return spu_count, image_count, "\r\n".join(text_list)

    def get_update_msg(self, results):
        text_list = []
        spu_count = 0
        image_count = 0
        for result in results:
            status_code = result['status_code']
            if status_code not in  [200, 408]:
                continue

            reason_type = result['reason_type']
            if reason_type.find('image') == -1:
                continue

            spu = result['spu']
            spu_count = spu_count + 1
            images = spu.images
            if len(images) == 0:
                text = "%s\t no image" % (spu.handle)
                text_list.append(text)
            else:
                for image in images:
                    image_count = image_count + 1
                    text = "image_update\t\t%s\t%s" % (spu.handle, image.src)
                    text_list.append(text)
        return spu_count, image_count, '\r\n'.join(text_list)

    def run(self):
        self.init_black_list()
        self.init_white_list()
        cur_data_file_path = '../data/cider_%s.json' % (self.cur_date)
        last_data_file_path = '../data/cider_%s.json' % (self.last_date)
        cur_spus = self.loadProdData(cur_data_file_path)
        last_spus = self.loadProdData(last_data_file_path)
        print(len(cur_spus.values()))
        print(len(last_spus.values()))
        new_prods, update_prods, review_prods = self.extractSPUList(cur_spus, last_spus)
        print(len(new_prods))
        print(len(update_prods))
        print(len(review_prods))
        for unit in update_prods:
            spu = unit.get('spu')
            reason = unit.get('reason')
            print("title:%s, reason:%s" % (spu.chineseTitle, reason))

        
        new_results = self.send_add_req(new_prods)
        self.send_error_msg(new_results)

        
        update_results = self.send_add_req(update_prods)
        self.send_error_msg(update_results)

        new_spu_count, new_image_count, new_msg = self.get_new_msg(new_results)
        update_spu_count, update_image_count, update_msg = self.get_update_msg(update_results)
        all_msg = "DATE=%s\r\n[NEW INFO] spu_count:%s, image_count:%s\r\n[UPDATE INFO] spu_count:%s, image_count:%s \r\n\r\n%s\r\n%s" % (self.cur_date, new_spu_count, new_image_count, update_spu_count, update_image_count, new_msg, update_msg)
        self.send_msg(all_msg)


if __name__ == '__main__':
    ProdShelf().run() 

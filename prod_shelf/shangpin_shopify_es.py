# encoding=utf-8
import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
import traceback


from elasticsearch import Elasticsearch
from elasticsearch import helpers
import sys
import json
from entity.EmmiolProdEntity import EmmiolProdEntity, EmmiolSKU, EmmiolImage, EmmiolDesc
from entity.ShelfProdEntity import ProdSPU
from entity.ProductEntity import Product
import requests
from common import utils
from common import emmiol_common as ec


class ProdPutES(object):
    def __init__(self, date, site, debug):
        self.es = None
        self.s3 = None
        self.date = date
        self.last_date = utils.date_add(self.date, -1)
        self.site = site
        self.debug = debug
        self.prod_list = []

    def init_info(self):
        if self.debug == 'true':
            self.es = Elasticsearch('3.138.153.109:9200')
        else:
            self.es = Elasticsearch('3.138.153.109:19200')
        region_name = 'us-east-1'
        aws_access_key_id = "AKIAWN4RN6OCPEDDK7PX"
        aws_secret_access_key = "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"
        self.s3 = utils.init_s3(region_name, aws_access_key_id, aws_secret_access_key)

    def init_data(self):
        with open("../data/%s_%s.json" % (self.site, self.date), 'r') as f:
            data_json_str = f.read()
            cur_data_json = json.loads(data_json_str)

        with open('../data/%s_%s.json' % (self.site, self.last_date), 'r') as f:
            data_json_str = f.read()
            last_data_json = json.loads(data_json_str)

        last_prod_dict = {}
        for unit in last_data_json:
            product_id = unit.get('product_id')
            last_prod_dict[product_id] = unit

        data_json = []
        for unit in cur_data_json:
            product_id = unit.get('product_id')
            if product_id not in last_prod_dict:
                data_json.append(unit)

        if self.debug == 'true':
            data_json = data_json[0:100]
        update_prod_list = []
        for unit in data_json:
            prod = Product().fromLoadJSON(unit)
            prod.site = self.site
            self.init_aws_url(self.s3, prod)
            self.alter_option_order(prod)
            self.get_size_table(prod)
            prod.init_default_cat_info()
            prod_spu = ProdSPU().fromJSON(prod.toShelfProdJson())
            if self.site == 'ellejamilla':
                if prod_spu.standard_cat2 != '' and prod_spu.standard_cat2 is not None:
                    self.prod_list.append(prod_spu)
                    update_prod_list.append(prod_spu)
            else:
                self.prod_list.append(prod_spu)
                update_prod_list.append(prod_spu)
            if len(update_prod_list) >= 10:
                self.es_write(update_prod_list)
                # self.es_product_write(update_prod_list)
                update_prod_list = []
        if len(update_prod_list) != 0:
            self.es_write(update_prod_list)
            # self.es_product_write(update_prod_list)
            update_prod_list = []


    def alter_option_order(self, prod_spu):
        if prod_spu.site == 'ellejamilla':
            sku_list = prod_spu.variants
            for sku in sku_list:
                option1 = sku.option1
                option2 = sku.option2
                sku.option1 = option2
                sku.option2 = option1
            options = prod_spu.options
            if len(options) != 2:
                pass    
            else:
                new_options = []
                option1 = options[0]
                option1['position'] = 2
                option2 = options[1]
                option2['position'] = 1
                new_options.append(option2)
                new_options.append(option1)
        return

    def get_size_table(self, prod_spu):
        if prod_spu.site == 'ellejamilla':
            cm_size, inch_size = prod_spu.description.get_size_table_ellejamilla()
            prod_spu.description.cm_size = cm_size
            prod_spu.description.inch_size = inch_size
        return


    def init_aws_url(self, s3, prod):
        for image in prod.images:
            image.init_aws_url(s3)
       
    def es_write(self, prod_list):
        action = []
        es_doc_ids = []
        actions = []
        print("prod_list:%s" % (len(prod_list)))
        for prod_unit in prod_list:
            try:
                es_doc_id, es_json = prod_unit.toESJSON()
            except Exception as e:
                print(traceback.format_exc(limit=5))
                continue
            status = self.es.exists(index='idx_competitor_product', id=es_doc_id)
            print(es_json)
            if status == True:
                action.append({"_index": "idx_competitor_product","_id": es_doc_id, "doc": es_json, "_op_type": "update"})
            else:
                action.append({"_index": "idx_competitor_product","_id": es_doc_id, "_source": es_json, "_op_type": "create"})
        print("es_write:%s" % action)
        helpers.bulk(self.es, action)

    def es_product_write(self, prod_list):
        action = []
        for prod_unit in prod_list:
            try:
                es_doc_id, es_json = prod_unit.toESJSON()
            except:
                continue
            status = self.es.exists(index='idx_product', id=es_doc_id)
            if status == True:
                action.append({"_index": "idx_product","_id": es_doc_id, "doc": es_json, "_op_type": "update"})
        print("prodcut_write:%s" % action)
        helpers.bulk(self.es, action)

    def run(self):
        self.init_info()
        self.init_data()


if __name__ == '__main__':
    site = sys.argv[1]
    date = sys.argv[2]
    debug = sys.argv[3]
    ProdPutES(date, site, debug).run()
    

# encoding=utf-8
import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
from lxml import etree


from elasticsearch import Elasticsearch
from elasticsearch import helpers
import sys
import json
from entity.EmmiolProdEntity import EmmiolProdEntity, EmmiolSKU, EmmiolImage, EmmiolDesc
from entity.ShelfProdEntity import ProdSPU
import requests
from common import utils
from common import emmiol_common as ec


class ProdPutES(object):
    def __init__(self, index):
        self.es = None
        self.s3 = None
        self.index = index
        self.prod_list = []
        self.cat_id_mapping = {
                221:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
576:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':2990},
225:{'cat1_id':2030, 'cat2_id':1888,'cat3_id':2228,'cat4_id':2231},
403:{'cat1_id':2030, 'cat2_id':2059,'cat3_id':2571,'cat4_id':2580},
109:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
192:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
579:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
247:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':2193},
19:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
27:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
7:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
34:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2037,'cat4_id':1735},
580:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
26:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':1733},
25:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1882,'cat4_id':None},
210:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1912},
539:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':None},
582:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
43:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2040,'cat4_id':1780},
504:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
561:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
433:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
458:{'cat1_id':2030, 'cat2_id':1894,'cat3_id':2181,'cat4_id':2184},
218:{'cat1_id':2030, 'cat2_id':1969,'cat3_id':2047,'cat4_id':None},
584:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
546:{'cat1_id':2030, 'cat2_id':2038,'cat3_id':2195,'cat4_id':None},
226:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':1866},
248:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':1766},
503:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
37:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
507:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
106:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1860,'cat4_id':None},
583:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
108:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1860,'cat4_id':1860},
578:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
38:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1871},
308:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':1779},
39:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1732},
473:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2216,'cat4_id':2217},
510:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
456:{'cat1_id':2030, 'cat2_id':1894,'cat3_id':2181,'cat4_id':None},
24:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
30:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
508:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
548:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':2291},
28:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2216,'cat4_id':1734},
491:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2216,'cat4_id':1734},
524:{'cat1_id':2030, 'cat2_id':2038,'cat3_id':2202,'cat4_id':2341},
492:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2216,'cat4_id':2219},
519:{'cat1_id':2030, 'cat2_id':2038,'cat3_id':2202,'cat4_id':2338},
        227:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':None},
        448:{'cat1_id':2030, 'cat2_id':2202,'cat3_id':None,'cat4_id':None},
        461:{'cat1_id':2030, 'cat2_id':1894,'cat3_id':2183,'cat4_id':None},
        509:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
        450:{'cat1_id':2030, 'cat2_id':2038,'cat3_id':2195,'cat4_id':None},
        469:{'cat1_id':2030, 'cat2_id':2038,'cat3_id':2195,'cat4_id':2203},
        460:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
        522:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1860,'cat4_id':None},
        544:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
        228:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
        506:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
        527:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1860,'cat4_id':1860},
        526:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
        577:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
        230:{'cat1_id':2030, 'cat2_id':None,'cat3_id':None,'cat4_id':None},
        547:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':None},
        454:{'cat1_id':2030, 'cat2_id':1894,'cat3_id':None,'cat4_id':None},
        426:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':None,'cat4_id':None},
        459:{'cat1_id':2030, 'cat2_id':1894,'cat3_id':None,'cat4_id':None},
        540:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
        585:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
        8:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
                }
    

    def init_info(self):
        self.es = Elasticsearch('3.138.153.109:9200')
        region_name = 'us-east-1'
        aws_access_key_id = "AKIAWN4RN6OCPEDDK7PX"
        aws_secret_access_key = "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"
        self.s3 = utils.init_s3(region_name, aws_access_key_id, aws_secret_access_key)

    def init_data(self):
        data_json_res = {}
        detail_file_list = ['../data/emmiol_20210522_newtmp.json', '../data/emmiol_20210519_part2.json', '../data/emmiol_20210519_part3.json', '../data/emmiol_20210520_part1.json', '../data/emmiol_20210520_part1_part2.json', '../data/emmiol_20210520_part1_part3.json']
        if self.index != 'all':
            index = int(self.index)
            detail_file_list = [detail_file_list[index]]
        for detail_unit in detail_file_list:
            data_file = open(detail_unit, "r")
            data_json_str = data_file.read()
            data_file.close()
            data_json = json.loads(data_json_str)
            data_json = data_json
            for unit in data_json:
                spu_code = unit.get('spu_code')
                data_json_res[spu_code] = unit

        #with open('../data/emmiol_20210519_new.json', 'r') as f:
        #    data_json_str = f.read()
        #    new_data_json = json.loads(data_json_str)
        #    for unit in data_json:
        #        spu_code = unit.get('spu_code')
        #        data_json_res.pop(spu_code)
        print("total:l%s"% (len(data_json_res)))

        size_res = {}
        with open("../data/emmiol/emmiol_size_total_20210523.json", 'r') as f:
            size_data = f.read()
            size_json = json.loads(size_data)
            for unit in size_json:
                spu_code = unit.get('spu_code')
                size_info = unit.get('size_info')
            size_res[spu_code] = size_info

        data_json = list(data_json_res.values())
        data_json = data_json
        for unit in data_json:
            spu_code = unit.get('spu_code')
            prod = EmmiolProdEntity().fromJSON(unit)
            original_cat_id = prod.original_cat_id 
            shein_cat_inf = self.cat_id_mapping.get(int(original_cat_id.strip()))
            cat1 = prod.cat1.strip()
            if cat1.lower() != 'women':
                continue 
            print(original_cat_id)
            print(prod.cat1)
            print(prod.cat2)
            print(prod.cat3)
            print(prod.cat4)
            print(shein_cat_inf)
            print( self.cat_id_mapping.get(226))
            cat1_id = shein_cat_inf.get('cat1_id')
            cat2_id = shein_cat_inf.get('cat2_id')
            cat3_id = shein_cat_inf.get('cat3_id')
            cat4_id = shein_cat_inf.get('cat4_id')
            prod.standard_cat1_id = cat1_id
            prod.standard_cat2_id = cat2_id
            prod.standard_cat3_id = cat3_id
            prod.standard_cat4_id = cat4_id
            if prod.description.size == "" or prod.description.size is None or len(prod.description.size.get('menu')) == 0:
                spu_code = prod.spu_code
                for sku in prod.sku_list:
                    if sku.detail_url != '':
                        origin_sku_id = sku.detail_url.split('product')[1].replace('.html', '')
                size_info = size_res.get(spu_code)
                if size_info is None:
                    print("spu_code:%s get size info" % (spu_code))
                    size_info = ec.get_size_table(origin_sku_id)
                else:
                    prod.description.size = size_info
            detail_url = prod.get_detail_url()

            while True:
                resp = requests.get(detail_url)
                print('spider:%s' % detail_url)
                if resp is not None:
                    break
            html_text = etree.HTML(resp.text)
            color_info = ec.get_color_info(html_text)
            print("corlor:%s " % color_info)
            for unit in color_info:
                cur_detail_url = unit.get('detail_url')
                if detail_url != cur_detail_url:
                    while True:
                        resp = requests.get(cur_detail_url)
                        print("spider:%s" % (cur_detail_url))
                        if resp is not None:
                            break
                    cur_html_text = etree.HTML(resp.text)
                    unit['html_text'] = cur_html_text
                    unit['image_list'] = ec.get_image_list(cur_html_text, [prod.spu_code])
                else:
                    unit['html_text'] = html_text
                    unit['image_list'] = ec.get_image_list(html_text, [prod.spu_code])
                    print(unit['image_list'])
            color_dict = {}
            for unit in color_info:
                color_dict[unit.get('color').lower()] = unit

            index = 1 
            prod.images = []
            for sku in prod.sku_list:
                color = sku.color 
                color_info = color_dict.get(color.lower())
                if color_info is None:
                    print("clor_dict:%s, color:%s" % (color_dict, color))
                    continue
                image_src_list = color_info.get('image_list')
                image_obj_list = []
                sku.images = []
                for image_src in image_src_list:
                    image_obj = EmmiolImage(prod)
                    image_obj.position = index
                    image_obj.src = image_src
                    image_obj.set_image_id(image_src)
                    sku.images.append(image_obj)
                    index = index + 1
                print("sku image len:%s" % (len(sku.images)))
            print("color_dict:%s" % color_dict)
            for unit in color_dict.values():
                image_src_list = unit.get('image_list')
                for image_src in image_src_list:
                    image_obj = EmmiolImage(prod)
                    image_obj.position = index
                    image_obj.src = image_src
                    prod.images.append(image_obj)

            print("prod image len:%s" % (len(prod.images)))
            self.init_aws_url(self.s3, prod)
            prod_spu = ProdSPU().fromJSON(prod.toShelfProdJson())
            self.prod_list.append(prod_spu)


    def init_aws_url(self, s3, prod):
        for image in prod.images:
            image.init_aws_url(s3)
        prod.default_image.init_aws_url(s3)
       
    def es_write(self, prod_list):
        action = []
        for prod_unit in prod_list:
            es_doc_id, es_json = prod_unit.toESJSON()
            es_json['launch_time'] = '2021-05-01 00:00:00'
            action.append({"_index": "idx_competitor_product","_id": es_doc_id, "_source": es_json, "_op_type": "update"})
        print(action)
        helpers.bulk(self.es, action)

    def run(self):
        self.init_info()
        self.init_data()
        index = 0 
        interval = 10
        self.prod_list = self.prod_list
        while index * interval < len(self.prod_list):
            self.es_write(self.prod_list[index * interval: index * interval + interval])
            index = index + 1


if __name__ == '__main__':
    date = sys.argv[1]
    ProdPutES(date).run()
    

# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
from entity.ProductEntity import Product
import json
import pandas as pd


class ShopifyAnalysis(object):
    def __init__(self, shopify_site):
        self.shopify_site = shopify_site
        self.prod_list = []

    def loadProdData(self, data_file_path):
        data_file = open(data_file_path, 'r')
        data_json_str = data_file.read()
        data_file.close()

        data_json = json.loads(data_json_str)
        for product_json in data_json:
            product = Product().fromLoadJSON(product_json)
            if product.cat1 != '':
                self.prod_list.append(product)
            else:
                print(product.title)

    def prodMapFrame(self, prod_list):
        rows = []
        for prod in prod_list:
            row = []
            row.append(prod.product_id)
            row.append(prod.cat1)
            row.append(prod.get_price())
            row.append(prod.get_price_level(30))
            rows.append(row)
        header = ['product_id', 'cat1', 'price', 'price_level']
        df = pd.DataFrame(rows, columns=header)
        cat_price_grouped = pd.DataFrame(df.groupby(['cat1', 'price_level']).agg({'product_id':'count'}))
        cat_group = pd.DataFrame(df.groupby(['cat1']).agg({'product_id':'count'}))
        price_group = pd.DataFrame(df.groupby(['price_level']).agg({'product_id':'count'}))
        all_group = pd.DataFrame(df.agg({'product_id': 'count'}))
        total_mechant = all_group.at['product_id', 0]
        cat_percent_group = pd.DataFrame(cat_price_grouped.div(cat_group, level='cat1'))
        all_percent_group = pd.DataFrame(price_group.div(total_mechant))
        price_group.columns = ['商品数']

        all_result_group = all_percent_group.join(price_group, how='outer', on=['price_level'])
        all_result_group['商品总数'] = total_mechant
        all_result_group['cat1'] = '总计'
        all_result_group = all_result_group.join(all_result_group.index.to_frame(), how='outer', on=['price_level'])
        all_result_group = all_result_group.reset_index(drop=True)



        merge_1 = pd.DataFrame(pd.merge(cat_percent_group, cat_price_grouped, how='outer', on=['cat1', 'price_level']))
        merge_2 = merge_1.join(cat_group, how='outer', on=['cat1'])
        merge_index = merge_2.index.to_frame()
        merge_3 = merge_2.join(merge_index, how='outer', on=['cat1', 'price_level'])
        merge_3 = merge_3.reset_index(drop=True)
        all_result_group.columns = ['占比', '商品数', '分类商品总数量', '分类名', '价格区间']
        merge_3.columns = ['占比', '商品数', '分类商品总数量', '分类名', '价格区间']
        all_result_group = all_result_group.append(merge_3)
        all_result_group['占比'] = all_result_group['占比'].apply(lambda x: format(x, '.2%'))
        all_result_group = all_result_group[['分类名', '价格区间', '占比', '商品数', '分类商品总数量']]
        print(all_result_group.to_csv(sep='\t', index=None))


    def run(self):
        self.loadProdData("../data/%s_%s.json" % (self.shopify_site, '20210510'))
        #for prod_unit in self.prod_list:
        #    if prod_unit.cat1 == '':
        #        print("%s\t%s" % (prod_unit.title, prod_unit.labels))
        self.prodMapFrame(self.prod_list)

        best_seller_prods = []
        for unit in self.prod_list:
            if unit.best_seller_index != -1 and unit.best_seller_index < 80:
                best_seller_prods.append(unit)
        print("len:%s" % (len(best_seller_prods)))
        self.prodMapFrame(best_seller_prods)





if __name__ == '__main__':
    ShopifyAnalysis ('forloveandlemons').run()

# encoding=utf-8


import requests
import json
from lxml import etree
from common import shein_category

# 抽取spu code信息
def extract_spu_code(sku_code):
    if sku_code.find('-') == -1:
        spu_code = sku_code[0:9]
    else:
        spu_code = sku_code.split('-')[0]
    return spu_code


# 抽取sku_id信息
def extract_sku_id(url):
    return url.split('product')[1].replace('.html', '')




# 获取尺码表信息
def get_size_table(goodsId):
    data = {"goodsId": goodsId}
    print(data)
    headers = {"Content-Type": "application/x-www-form-urlencoded", "Content-Length": str(len(str(data))), "Host": "www.emmiol.com"}
    url = "https://www.emmiol.com/co-goods-ac-goodsSizeGuideDetails.json"
    while True:
        resp = requests.post(url, headers=headers, data=data)
        if resp is not None:
            break
    cm_size_list = json.loads(resp.text)
    size_data = cm_size_list.get('data')
    print(size_data)
    if len(size_data) != 0 and len(size_data.get('menu')) != 0:
        menu = size_data.get('menu')
        size_info = size_data.get("sizeList")
        print("1233455 menu:%s" % menu)
        return {"menu": menu, "size_list": size_info}

    url = "https://www.emmiol.com/co-goods-ac-goodsSizeGuide.json"
    while True:
        resp = requests.post(url, headers=headers, data=data)
        if resp is not None:
            break
    cm_size_list = json.loads(resp.text)
    size_data = cm_size_list.get('data')
    if len(size_data) != 0:
        size_info = size_data.get('US')
        menu = size_data.get("menu")
        return {"menu": menu, "size_list": [size_info]}

    return {"menu": [], "size_list": []}




# 获取预售信息
def parse_pre_sale(html_text):
    pre_sale = html_text.xpath('//p[contains(@class, "pre-sale-wrap")]//text()')
    return "".join(pre_sale)


def parse_tags(html_text):
    tags = html_text.xpath('//div[@class="tag-part"]')
    if len(tags) == 0:
        return []
    tag_list = []
    for children in tags[0].getchildren():
        tag = children.text
        tag_list.append(tag)
    return tag_list


def get_detail(html_text):
    description = html_text.xpath('//div[@class="de_con clearfix"]')
    desc_str = str(etree.tostring(description[0].getchildren()[0])).replace('<br/>', '\r\n').replace("<p>", "").replace("</p>", "")
    desc_line_list = desc_str.split("\r\n")
    detail_list = []
    for line_unit in desc_line_list:
        if len(line_unit.split(":")) == 2:
            detail_list.append({line_unit.split(":")[0].strip().replace("b'", ''): line_unit.split(":")[1].strip().replace("'", '')})
        else:
            detail_list.append({line_unit: ''})
    return detail_list


def get_image_list(html_text, match_str_list):
    div_data = html_text.xpath('//div[@class="swiper-slide"]')
    src_list = []
    print(len(div_data))
    for div_unit in div_data:
        img_list = div_unit.xpath("img")
        for img_unit in img_list:
            src = img_unit.get('src')
            print(src)
            if src is None:
                continue
            #for match_str in match_str_list:
            if src.find("img_original") != -1 and src.endswith('jpg') == True:
                if src not in src_list:
                    src_list.append(src)
            else:
                for match_str in match_str_list:
                    if src.find(match_str) != -1:
                        if src not in src_list:
                            src_list.append(src)
                        break
    return src_list

def get_return_and_shipping(html_text):
    text_1 = "".join(html_text.xpath('//div[@class="ship-1 fl"]//text()'))
    text_2 = "".join(html_text.xpath('//div[@class="ship-2 fl"]//text()'))
    return "%s%s" % (text_1, text_2)



def get_select_color(html_text):
    color_data = html_text.xpath('//ul[@class="choose_list  clearfix"]')
    li_list = color_data[0].xpath('li')
    for li_unit in li_list:
        color = li_unit.get("title")

        li_class_attr = li_unit.get('class')
        if li_class_attr.find('selected') != -1:
            return color
    return ""


def get_select_size(html_text):
    size_data = html_text.xpath('//ul[@class="choose_list choose_list_inline"]')
    li_list = size_data[0].xpath("li")
    for li_unit in li_list:
        size = a_unit.get("title")
        li_class_attr = li_unit.get("class")
        if li_class_attr.find('selected') != -1:
            return size
    return ""


def get_color_info(html_text):
    color_data = html_text.xpath('//ul[@class="choose_list  clearfix"]')
    if len(color_data) == 0:
        return []
    li_list = color_data[0].xpath('li')
    color_dict = [] 
    for li_unit in li_list:
        a_unit = li_unit.getchildren()[0]
        color = a_unit.get("title")
        color_detail = a_unit.get("data-url")
        color_info = {"color": color, "detail_url": color_detail}
        color_dict.append(color_info)
    return color_dict

def get_size_info(html_text):
    size_data = html_text.xpath('//ul[@class="choose_list choose_list_inline"]')
    if len(size_data) == 0:
        return []
    li_list = size_data[0].xpath("li")
    size_dict = []
    for li_unit in li_list:
        a_unit = li_unit.getchildren()[0]
        size = a_unit.get("title")
        size_detail = a_unit.get('data-url')
        size_info = {"size": size, "detail_url": size_detail}
        size_dict.append(size_info)

    return size_dict



def category_mapping(original_cat_id):
    cat_mapping = {
                            221:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
576:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':2990},
225:{'cat1_id':2030, 'cat2_id':1888,'cat3_id':2228,'cat4_id':2231},
403:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':None},
109:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
192:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
579:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
247:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':2193},
19:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
27:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
7:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
34:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2037,'cat4_id':1735},
580:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
26:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':1733},
25:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1882,'cat4_id':None},
210:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1912},
539:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':None},
582:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
43:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2040,'cat4_id':1780},
504:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
561:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
433:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
458:{'cat1_id':2030, 'cat2_id':1894,'cat3_id':2181,'cat4_id':2184},
218:{'cat1_id':2030, 'cat2_id':1969,'cat3_id':2047,'cat4_id':None},
584:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
546:{'cat1_id':2030, 'cat2_id':2038,'cat3_id':2195,'cat4_id':None},
226:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':1866},
248:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':1766},
503:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
37:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
507:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
106:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1860,'cat4_id':None},
583:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
108:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1860,'cat4_id':1860},
578:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
38:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1871},
308:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':1779},
39:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1732},
473:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2216,'cat4_id':2217},
510:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
456:{'cat1_id':2030, 'cat2_id':1894,'cat3_id':2181,'cat4_id':None},
24:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
30:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1766,'cat4_id':None},
508:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
548:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':2291},
28:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2216,'cat4_id':1734},
491:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2216,'cat4_id':1734},
524:{'cat1_id':2030, 'cat2_id':2038,'cat3_id':2202,'cat4_id':2341},
492:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':2216,'cat4_id':2219},
519:{'cat1_id':2030, 'cat2_id':2038,'cat3_id':2202,'cat4_id':2338},
        227:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':None},
        448:{'cat1_id':2030, 'cat2_id':2202,'cat3_id':None,'cat4_id':None},
        461:{'cat1_id':2030, 'cat2_id':1894,'cat3_id':2183,'cat4_id':None},
        509:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
        450:{'cat1_id':2030, 'cat2_id':2038,'cat3_id':2195,'cat4_id':None},
        469:{'cat1_id':2030, 'cat2_id':2038,'cat3_id':2195,'cat4_id':2203},
        460:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
        522:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1860,'cat4_id':None},
        544:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
        228:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
        506:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1930,'cat4_id':1934},
        527:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1860,'cat4_id':1860},
        526:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
        577:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
        230:{'cat1_id':2030, 'cat2_id':None,'cat3_id':None,'cat4_id':None},
        547:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':2191,'cat4_id':None},
        454:{'cat1_id':2030, 'cat2_id':1894,'cat3_id':None,'cat4_id':None},
        426:{'cat1_id':2030, 'cat2_id':2039,'cat3_id':None,'cat4_id':None},
        459:{'cat1_id':2030, 'cat2_id':1894,'cat3_id':None,'cat4_id':None},
        540:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
        585:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1767,'cat4_id':1740},
        8:{'cat1_id':2030, 'cat2_id':2035,'cat3_id':1727,'cat4_id':None},
    }

    if original_cat_id is None:
        return {}

    dest_cat_info = cat_mapping.get(int(original_cat_id))
    result = {}
    if dest_cat_info is not None:
        cat1_id = dest_cat_info.get('cat1_id')
        cat2_id = dest_cat_info.get('cat2_id')
        cat3_id = dest_cat_info.get('cat3_id')
        cat4_id = dest_cat_info.get('cat4_id')
        result['cat1_id'] = cat1_id
        result['cat2_id'] = cat2_id
        result['cat3_id'] = cat3_id
        result['cat4_id'] = cat4_id

        cat1_name = None if cat1_id is None else shein_category.all_category_data.get(cat1_id).get("level1_cat_name")
        cat2_name = None if cat2_id is None else shein_category.all_category_data.get(cat2_id).get("level2_cat_name")
        cat3_name = None if cat3_id is None else shein_category.all_category_data.get(cat3_id).get("level3_cat_name")
        cat4_name = None if cat4_id is None else shein_category.all_category_data.get(cat4_id).get("level4_cat_name")
        result['cat1_name'] = cat1_name
        result['cat2_name'] = cat2_name
        result['cat3_name'] = cat3_name
        result['cat4_name'] = cat4_name
    else:
        print("cat_id:%s no standard cat info" % (original_cat_id))

    return result



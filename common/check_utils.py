




def empty_check(value):

    if value is None:
        return True

    if type(value) == type("a"):
        if value == '':
            return True

        return False

    elif type(value) == type(0):
        if value == 0:
            return True

        return False

    elif type(value) == type([]):
        if len(value) == 0:
            return True
        else:
            return False
    elif type(value) == type({}):
        if len(value) == 0:
            return True
        else:
            return False
    else:
        pass
    return False

def field_type_check(value, field_type):
    if field_type == 'string':
        if type(value) == type("abc"):
            return True
        else:
            return False
    
    if field_type == 'int':
        if type(value) == type(0):
            return True
        else:
            return False

    if field_type == 'double':
        if type(value) == type(1.0):
            return True
        else:
            return False

    return True

    

# encoding=utf-8

from lxml import etree


def parse_size_table(body_html):
    body = body_html.xpath("//tbody")
    if len(body) == 0:
        return [{}]
    tr = body[0].xpath("./tr")
    line_index = 0
    head_list = []
    value_lines = []
    for tr_unit in tr:
        td_list = tr_unit.xpath("./td")
        value_line = {} 
        unit_index = 0
        for td in td_list:
            if line_index == 0:
                if td.text is not None and td.text.strip() != '':
                    head_list.append(td.text)
            else:
                if td.text is not None and td.text.strip() != '':
                    head_value = head_list[unit_index]
                    unit_value = td.text
                    try:
                        value_line[head_value] = int(unit_value)
                    except:
                        value_line[head_value] = unit_value
                    unit_index = unit_index + 1
        if line_index != 0:
            value_lines.append(value_line)
        line_index = line_index + 1
    print(value_lines)
    return value_lines 


def parse_detail(body_html):
    span = body_html.xpath("//p")
    detail_dict = {}
    if len(span) == 0:
        return [detail_dict]
    for unit in span[0:1]:
        span_info = unit.xpath("./span")
        for span_unit in span_info:
            print(span_unit.xpath(".//text()"))
            text_unit = " ".join(span_unit.xpath(".//text()"))
            if text_unit.find(':') == -1:
                continue
            key = text_unit.split(":")[0]
            value = text_unit.split(":")[1]
            detail_dict[key] = value
    print(detail_dict)
    return [detail_dict]


def parse_detail_unicornbae(body_html):
    p_list = body_html.xpath("//p")
    detail_info = {}
    for p_unit in p_list[0:1]:
        sub_p_unit_list = p_unit.xpath('.//node()')
        new_record = []
        for sub_unit in sub_p_unit_list:
            if type(sub_unit) == type(etree._Element()):
                if sub_unit.tag == 'br':
                    sub_detail_unit = "".join(new_record)
                    if sub_detail_unit.find(':') == -1:
                        sub_detail_key = sub_detail_unit
                        sub_detail_value = ''
                    else:
                        sub_detail_key = sub_detail_unit.split(":")[0]
                        sub_detail_value = sub_detail_unit.split(":")[1]
                    detail_info[sub_detail_key] = sub_detail_value
                    new_record = []
                elif sub_unit.tag == 'strong':
                    pass
            elif type(sub_unit) == type(etree._ElementUnicodeResult()):
                if sub_unit is not None:
                    new_record.append(sub_unit)
            else:
                pass
        if len(new_record) != 0:
            sub_detail_unit = "".join(new_record)
            if sub_detail_unit.find(':') == -1:
                sub_detail_key = sub_detail_unit
                sub_detail_value = ''
            else:
                sub_detail_key = sub_detail_unit.split(":")[0]
                sub_detail_value = sub_detail_unit.split(":")[1]
            detail_info[sub_detail_key] = sub_detail_value
    return [detail_info]

def parse_size_table_unicornbae(body_html):
    thead_list = body_html.xpath("//thead")
    tbody_list = body_html.xpath("//tbody")
    if len(tbody_list) == 0:
        return [{}]
    
    is_head_in_body = False
    if len(thead_list) != 0:
        thead = thead_list[0] 
        head_th_list = thead.xpath("./tr/th")
        head_text_list = []
        for head_th_unit in head_th_list:
            th_text = "".join(head_th_unit.xpath(".//text()")).strip()
            head_text_list.append(th_text)
    else:
        tr_list = tbody_list[0].xpath("./tr")
        if len(tr_list) >= 1:
            tr_head = tr_list[0]
        else:
            return [{}]
        td_list = tr_head.xpath("./td")
        head_text_list = []
        for td_unit in td_list:
            td_text = "".join(td_unit.xpath(".//text()")).strip()
            head_text_list.append(td_text)
        is_head_in_body = True

    tbody = tbody_list[0]
    body_tr_list = tbody.xpath("./tr")
    size_table = []
    if is_head_in_body == True:
        if len(body_tr_list) <= 1:
            return [{}]
        else:
            body_tr_list = body_tr_list[1:]
    for tr_unit in body_tr_list:
        td_list = tr_unit.xpath('./td')
        index = 0
        size_table_unit = {} 
        if len(td_list) != len(head_text_list):
            print("td len:%s, head len:%s" % (len(td_list), len(head_text_list)))
            size_table.append(size_table_unit)
            continue

        for td_unit in td_list:
            td_text = "".join(td_unit.xpath(".//text()")).strip()
            head = head_text_list[index]
            size_table_unit[head] = td_text
            index = index + 1
        size_table.append(size_table_unit)
    return size_table

def parse_size_image(body_html):
    if body_html is None:
        return ""

    img_list = body_html.xpath("//img")
    if len(img_list) == 0:
        return ""

    size_image = img_list[0].get('src')
    return size_image

        
if __name__ == '__main__':
    html_text = '''<p><strong>Season:</strong> Spring/Autumn<br><strong>Material:</strong> Polyester, Spandex<br><strong>Silhouette:</strong> Straight<br><strong>Neckline:</strong> Halter<br><strong>Sleeve Length(cm):</strong> Sleeveless<br><strong>Sleeve Style:</strong> Spaghetti Strap<br><strong>Gender:</strong> WOMEN<br><strong>Decoration:</strong> Button<br><strong>Style:</strong> VSCO Girl<br><strong>Waistline:</strong> Empire<br><strong>Pattern Type:</strong> Print<br><strong>Dresses Length:</strong> Above Knee, Mini Dress</p>\n<table>\n<tbody>\n<tr>\n<td width=\"100\">\n<p><strong>Size (cm)</strong></p>\n</td>\n<td width=\"103\">\n<p><strong>Waist</strong></p>\n</td>\n<td width=\"103\">\n<p><strong>Hips</strong></p>\n</td>\n<td width=\"107\">\n<p><strong>Dress</strong></p>\n</td>\n<td width=\"107\">\n<p><strong>Bust</strong></p>\n</td>\n</tr>\n<tr>\n<td width=\"100\">\n<p><strong>S</strong></p>\n</td>\n<td width=\"103\">\n<p>66</p>\n</td>\n<td width=\"103\">\n<p>92</p>\n</td>\n<td width=\"107\">\n<p>77</p>\n</td>\n<td width=\"107\">\n<p>84</p>\n</td>\n</tr>\n<tr>\n<td width=\"100\">\n<p><strong>M</strong></p>\n</td>\n<td width=\"103\">\n<p>70</p>\n</td>\n<td width=\"103\">\n<p>96</p>\n</td>\n<td width=\"107\">\n<p>79</p>\n</td>\n<td width=\"107\">\n<p>88</p>\n</td>\n</tr>\n<tr>\n<td width=\"100\">\n<p><strong>L</strong></p>\n</td>\n<td width=\"103\">\n<p>74</p>\n</td>\n<td width=\"103\">\n<p>100</p>\n</td>\n<td width=\"107\">\n<p>81</p>\n</td>\n<td width=\"107\">\n<p>92</p>\n</td>\n</tr>\n</tbody>\n</table>'''
    html_text = etree.HTML(html_text)
    #print(parse_size_table_unicornbae(html_text))
    print(parse_detail_unicornbae(html_text))
    print(parse_size_image(html_text))

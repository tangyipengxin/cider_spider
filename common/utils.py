# encoding=utf-8
import boto3
from datetime import datetime, timedelta
import datetime as dt
import time
import requests
from decimal import Decimal
import sys
import aiohttp


def BoolTrans(bool_str):
    bool_str = bool_str.lower()
    if bool_str == 'true':
        return True
    if bool_str == 'false':
        return False

    if bool_str == '':
        return False


def cm_float_format(value):
    value = Decimal(value).quantize(Decimal("0.1"), rounding="ROUND_HALF_UP")
    start_value = str(value).split('.')[0]
    end_value = str(value).split('.')[1]
    if int(end_value) == 5:
        return float(value)
    elif int(end_value) < 5:
        return int(start_value)
    else:
        return int(start_value) + 1 


def date_add(date, delta):
    cur_date = datetime.strptime(date, '%Y%m%d')
    res_date = cur_date + dt.timedelta(days=delta)

    return res_date.strftime("%Y%m%d")

def inch_float_format(value):
    value = float(value)
    value = round(value, 1)
    end_value = str(value).split('.')[1]
    if end_value == '0':
        return int(str(value).split('.')[0])
    else:
        return value

def inch_float_int_format(value):
    value = round(float(value))
    return value
    

def TimeTrans(time_str):
    datetime_str = time_str[0:19]
    tz_utc_str = time_str[19:25].replace(':','')
    new_time_str = datetime_str + tz_utc_str
    dt = datetime.strptime(new_time_str, "%Y-%m-%dT%H:%M:%S%z")
    timestamp = dt.timestamp()
    return timestamp

def toTimestamp(time_str):
    dt = datetime.strptime(time_str, "%Y%m%d")
    timestamp = dt.timestamp()
    return timestamp

def DateFormat(timestamp):
    return time.strftime('%Y%m%d', time.localtime(timestamp))

def DatetimeFormat(timestamp):
    print("timestamp:%s" % (timestamp))
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(timestamp))

def InchToCM(inch):
    return round(inch * 2.54)

def CMToInch(cm):
    return round(cm / 2.54)

def DataCompare(data1, data2):
    if data1 is None and data2 is None:
        return True
    
    if type(data1) != type(data2):
        return False

    if isinstance(data1, (list, set)) == True:
        if len(data1) != len(data2):
            return False

        for index in range(len(data1)):
            status = DataCompare(data1[index], data2[index])
            if status == False:
                return False
        return True

    elif isinstance(data1, dict) == False:
        if data1 == data2:
            return True
        else:
            return False
    elif isinstance(data1, dict) == True:
        keys1 = data1.keys()
        keys2 = data2.keys()
        if len(keys1) != len(keys2):
            return False

        for key1 in keys1:
            if key1 not in keys2:
                return False
            value1 = data1.get(key1)
            value2 = data2.get(key1)
            status = DataCompare(value1, value2)
            if status == False:
                return False
        return True

def image_download(image_url):
    r = requests.get(image_url)
    image_name = image_url.split("?")[0].split('/')[-1]
    image_path = "../image/%s" % image_name
    with open(image_path, 'wb') as f:
        f.write(r.content)

    return image_name

def upload_aws(S3, image_name):
    image_path = "../image/%s" % (image_name)
    bucket = "image-resize-442149761924-us-east-1"
    key = "images/%s" % image_name
    with open(image_path, 'rb') as f:
        photo_stream = f.read()
        try:
            S3.Bucket(bucket).put_object(Key=key, Body=photo_stream)
        except Exception as e:
            print("upload {} error:{}".format(image_name, e))
            return False, ""
    image_url = "https://%s.s3.amazonaws.com/%s" % (bucket, key) 
    return True, image_url 

        
def init_s3(region_name, aws_access_key_id, aws_secret_access_key):
    s3 = boto3.resource('s3', region_name=region_name, aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
    return s3

def async_image_cut(image_name):
    url = "https://img.shopoases.com/images/%s?d=1300x1600" % (image_name)
    session = aiohttp.ClientSession()
    session.get(url)

def gene_id_fromstring(text):
    hash = 23;
    for char in text:
        hash = hash * 31 + ord(char)
    return hash

def send_alarm(msg):
    headers = {
        "Content-Type": "application/json"
    }
    url = "https://open.feishu.cn/open-apis/bot/v2/hook/0a638cee-06cb-4a50-938e-7f64fbe99e30"
    data = json.dumps({"msg_type": "text", "content": {"text": msg}})
    resp = requests.post(url, headers=headers, data=data)
    return resp



if __name__ == '__main__':
    async_image_cut('0bcb131d579809aea2eaf740755f0d9f.jpg')
    #time_str = '2021-04-01T23:25:33-07:00'
    #stamp = TimeTrans(time_str)
    #print(stamp)


    #image_url = "https://cdnimg.emmiol.com/E/202012/img_original-CAL0272BO-141501867.jpg"
    #image_name = image_download(image_url)
    #print(image_name)

    #region_name = 'us-east-1' 
    #aws_access_key_id = "AKIAWN4RN6OCPEDDK7PX"
    #aws_secret_access_key = "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"
    #s3 = boto3.resource('s3', region_name=region_name, aws_access_key_id=aws_access_key_id,
    #                    aws_secret_access_key=aws_secret_access_key)
    
    #upload_aws(s3,  image_name)
    #value = sys.argv[1]

    #print(inch_float_format(value))


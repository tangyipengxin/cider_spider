# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
from common import utils, shopify_common as sc
from lxml import etree
import json
import time

class Product(object):
    def __init__(self):
        self.product_id = 0
        self.title = ''
        self.handle = ''
        self.body_html = ''
        self.published_at = ''
        self.created_at = ''
        self.updated_at = ''
        self.vendor = ''
        self.product_type = ''
        self.tags = []
        self.images = []
        self.options = []
        self.variants = []

        self.cat1 = ""
        self.cat2 = ""
        self.cat3 = ""
        self.cat4 = ""
        self.standard_cat1 = ''
        self.standard_cat2 = ''
        self.standard_cat3 = ''
        self.standard_cat4 = ''
        self.standard_cat1_id = 0
        self.standard_cat2_id = 0
        self.standard_cat3_id = 0
        self.standard_cat4_id = 0
        self.labels = set()
        self.best_seller_index = -1
        self.site = ''
        self.init_conf()

    def init_conf(self):
        self.site_platform_mapping = {
                "cider": 2,
                "genzlabs": 3,
                "ellejamilla": 4,
                "unicornbae": 5,
                "ivrose": 6
                }


    def toShelfProdJson(self):
        prod_json = {}
        prod_json['platform_id'] = self.site_platform_mapping.get(self.site)
        print(prod_json['platform_id'])
        prod_json["number"] = self.product_id
        prod_json["shop_id"] = 1
        prod_json["vendor"] = ""
        prod_json["handle"] = self.handle
        prod_json["tags"] = self.tags
        prod_json["chineseTitle"] = self.title
        prod_json["englishTitle"] = self.title
        prod_json["defaultImageUrl"] = self.get_default_image_url()
        prod_json["html"] = ''
        prod_json["imageIndex"] = ""
        prod_json["source_type"] = ''
        prod_json["totalInventoryQuantity"] = ''
        prod_json["options"] = self.get_options()
        prod_json["description"] = self.description.toShelfProdJson()
        prod_json["variants"] = [unit.toShelfJson() for unit in self.variants]
        prod_json["spu"] = self.product_id
        prod_json["createdByName"] = "tangyipeng"
        prod_json["updatedByName"] = "tangyipeng"
        prod_json["createdAt"] = self.created_at * 1000
        prod_json["updatedAt"] = self.created_at
        prod_json["createdBy"] = 0
        prod_json["updatedBy"] = 0
        prod_json["productType"] = self.get_product_type()
        prod_json["images"] = [unit.toShelfProdJson() for unit in self.images]
        prod_json["labels"] = list(self.labels)
        prod_json['detail_url'] = "https://%s.com/products/%s" % (self.site, self.handle)
        prod_json['cat1'] = self.cat1
        prod_json['cat2'] = self.cat2
        prod_json['cat3'] = self.cat3
        prod_json['cat4'] = self.cat4

        prod_json['standard_cat1'] = self.standard_cat1
        prod_json['standard_cat2'] = self.standard_cat2
        prod_json['standard_cat3'] = self.standard_cat3
        prod_json['standard_cat4'] = self.standard_cat4
        prod_json['standard_cat1_id'] = self.standard_cat1_id
        prod_json['standard_cat2_id'] = self.standard_cat2_id
        prod_json['standard_cat3_id'] = self.standard_cat3_id
        prod_json['standard_cat4_id'] = self.standard_cat4_id

        return prod_json

    def add_label(self, label):
        self.labels.add(label)

    def contain_label(self, label):
        if label in self.labels:
            return True
        else:
            return False

    def add_option(self, name, position, values=[]):
        option = {"name": name, "position": position, "values": values}
        self.options.append(option)

    def get_options(self):
        return self.options

    def update_option_position(self, name, position):
        for option in self.options:
            if option.get("name") == name:
                option['position'] = position
                return True
        return False

    def update_option_values(self, name, values):
        if option in self.options:
            if option.get("name") == name:
                option['position'] = position
                return True

        return False

    def get_body_html(self):
        return self.body_html

    def update_standard_cat_label(self, standard_cat1, standard_cat2, standard_cat3, standard_cat4, standard_cat1_id, standard_cat2_id, standard_cat3_id, standard_cat4_id):
        if standard_cat1 is None or standard_cat1 == '':
            return False

        new_noempty_count = 1 if standard_cat1 is not None and standard_cat1 != '' else 0 + 1 if standard_cat2 is not None and standard_cat2 != '' else 0 + 1 if standard_cat3 is not None and standard_cat3 != '' else 0 + 1 if standard_cat4 is not None and standard_cat4 != '' else 0
        cur_noempty_count = 1 if self.standard_cat1 is not None and self.standard_cat1 != '' else 0 + 1 if self.standard_cat2 is not None and self.standard_cat2 != '' else 0 + 1 if self.standard_cat3 is not None and self.standard_cat3 != '' else 0 + 1 if self.standard_cat4 is not None and self.standard_cat4 != '' else 0
        if new_noempty_count < cur_noempty_count:
            return False


        self.standard_cat1 = standard_cat1
        self.standard_cat2 = standard_cat2
        self.standard_cat3 = standard_cat3
        self.standard_cat4 = standard_cat4
        self.standard_cat1_id = standard_cat1_id
        self.standard_cat2_id = standard_cat2_id
        self.standard_cat3_id = standard_cat3_id
        self.standard_cat4_id = standard_cat4_id
        return True

    def update_cat_label(self, cat1, cat2, cat3, cat4):
        cat1 = '' if cat1 is None else cat1.lower()
        cat2 = '' if cat2 is None else cat2.lower()
        cat3 = '' if cat3 is None else cat3.lower()
        cat4 = '' if cat4 is None else cat4.lower()

        if self.cat1 == '' and cat1 != '':
            self.cat1 = cat1
            self.cat2 = cat2
            self.cat3 = cat3
            self.cat4 = cat4
            return True


        if self.cat1 != '' and cat1 == '':
            return False

        if self.cat1 == '' and cat1 == '':
            return False

        if self.cat1 != '' and cat1 != '' and self.cat1 != cat1:
            print('warning: product has two cat info, old_cat:%s, new_cat:%s, product_id:%s, title:%s' % (self.cat1, cat1, self.product_id, self.title))
            self.cat1 = cat1
            self.cat2 = cat2
            self.cat3 = cat3
            self.cat4 = cat4
            return False

        old_empty_count = 1 if self.cat2 == '' else 0 + 1 if self.cat3 == '' else 0 + 1 if self.cat4 == '' else 0
        new_empty_count = 1 if cat2 == '' else 0 + 1 if cat3 == '' else 0 + 1 if cat4 == '' else 0
        if old_empty_count >= new_empty_count:
            self.cat1 = cat1
            self.cat2 = cat2
            self.cat3 = cat3
            self.cat4 = cat4
            return True
        return False

    def get_price(self):
        if self.variants is None or len(self.variants) == 0:
            return -1
        return float(self.variants[0].price)

    def get_price_level(self, interval = 10):
        if self.get_price() == -1:
            return -1
        return "%s~%s" % (interval * (int(self.get_price()) // interval), interval * ((int(self.get_price()) // interval) + 1))

    def get_default_image(self):
        for image in self.images:
            position = image.get_position()
            if position == 1:
                return image
        return None

    def get_default_image_url(self):
        image = self.get_default_image()
        if image is None:
            return ''
        return image.get_src()

    def get_index_image(self, index):
        for image in self.images:
            position = image.get_position()
            if position == index:
                return image
        return None

    def get_product_id(self):
        return self.product_id

    def get_tags(self):
        return self.tags

    def get_title(self):
        return self.title

    def get_product_type(self):
        if self.cat4 is not None and self.cat4 != '':
            return self.cat4

        if self.cat3 is not None and self.cat3 != '':
            return self.cat3

        if self.cat2 is not None and self.cat2 != '':
            return self.cat2

        if self.cat1 is not None and self.cat1 != '':
            return self.cat1

        if self.product_type is not None and self.product_type != '':
            if self.product_type.lower() == 'outwear':
                return 'outerwear'
            if self.product_type.lower() == 'skirt':
                return 'skirts'
            if self.product_type.lower() in ('top', 't', 'sweater', 'sw'):
                return 'tops'
            if self.product_type.lower() in ('bottom', 'pants'):
                return 'bottoms'
            if self.product_type.lower() in ('dress'):
                return 'dresses'
            if self.product_type.lower() in ('dn'):
                return 'denim'
            return self.product_type
        return ''

    def get_cat1(self):
        return self.cat1

    def update_cat_info(self, labels):
        if self.cat1 is not None and self.cat1 != '':
            return 

        if 'matching-set' in labels:
            self.cat1 = 'matching-set'

    def fromSpiderJSON(self, data_json):
        if data_json is None:
            print("warning: data_json is None, parse end")
            return self
        self.product_id = data_json.get('id')
        self.title = data_json.get('title')
        self.handle = data_json.get('handle')
        self.body_html = data_json.get('body_html')
        self.description = Description(self).fromHTMLData(self.body_html)
        self.published_at = utils.TimeTrans(data_json.get('published_at'))
        self.created_at = utils.TimeTrans(data_json.get('created_at'))
        self.updated_at = utils.TimeTrans(data_json.get('updated_at'))
        self.vendor = data_json.get('vendor')
        self.product_type = data_json.get('product_type')
        self.tags = data_json.get('tags')
        images_json = data_json.get('images')
        for image_json_unit in images_json:
            if image_json_unit is None:
                print("title:%s" % self.title)
            image = Image(self).fromSpiderJSON(image_json_unit)
            self.images.append(image)

        variants_json = data_json.get('variants')
        for variant_json_unit in variants_json:
            variant = Variant(self).fromSpiderJSON(variant_json_unit)
            self.variants.append(variant)

        options_json = data_json.get('options')
        for option_json_unit in options_json:
            self.add_option(option_json_unit.get('name'), option_json_unit.get('position'), option_json_unit.get('values'))
        return self

    def init_default_cat_info(self):
        if self.cat1 == '' and (self.standard_cat1 == '' or self.standard_cat1 is None):
            self.standard_cat1 = 'women'
            self.standard_cat1_id = 2030

    def fromLoadJSON(self, data_json):
        if data_json is None:
            print("warning: data_json is None, parse end")
            return self
        self.product_id = data_json.get('product_id')
        self.title = data_json.get('title')
        self.handle = data_json.get('handle')
        self.body_html = data_json.get('body_html')
        self.description = Description(self).fromLoadJSON(data_json.get('description'))
        self.published_at = data_json.get('published_at')
        self.created_at = data_json.get('created_at')
        self.updated_at = data_json.get('updated_at')
        self.vendor = data_json.get('vendor')
        self.product_type = data_json.get('product_type')
        self.tags = data_json.get('tags')
        images_json = data_json.get('images')
        for image_json_unit in images_json:
            if image_json_unit is None:
                print("title:%s" % self.title)
            image = Image(self).fromLoadJSON(image_json_unit)
            self.images.append(image)

        variants_json = data_json.get('variants')
        for variant_json_unit in variants_json:
            variant = Variant(self).fromLoadJSON(variant_json_unit)
            self.variants.append(variant)

        self.options = data_json.get('options')
        self.labels = set(data_json.get('labels'))

        self.cat1 = data_json.get('cat1')
        self.cat2 = data_json.get('cat2')
        self.cat3 = data_json.get('cat3')
        self.cat4 = data_json.get('cat4')
        self.standard_cat1 = data_json.get('standard_cat1')
        self.standard_cat2 = data_json.get('standard_cat2')
        self.standard_cat3 = data_json.get('standard_cat3')
        self.standard_cat4 = data_json.get('standard_cat4')
        self.standard_cat1_id = data_json.get('standard_cat1_id')
        self.standard_cat2_id = data_json.get('standard_cat2_id')
        self.standard_cat3_id = data_json.get('standard_cat3_id')
        self.standard_cat4_id = data_json.get('standard_cat4_id')
        self.update_cat_info(self.labels)
        self.best_seller_index = -1 if data_json.get('best_seller_index') is None else data_json.get('best_seller_index')
        return self

    def toDumpJSON(self):
        data_json = {}
        data_json['product_id'] = self.product_id
        data_json['title'] = self.title
        data_json['handle'] = self.handle
        data_json['body_html'] = self.body_html
        data_json['published_at'] = self.published_at
        data_json['created_at'] = self.created_at
        data_json['updated_at'] = self.updated_at
        data_json['vendor'] = self.vendor
        data_json['product_type'] = self.product_type
        data_json['tags'] = self.tags
        #data_json['images'] = [image.toDumpJSON() for image in self.images]
        data_json['images'] = [image.toDumpJSON() for image in self.images]
        data_json['variants'] = [variant.toDumpJSON() for variant in self.variants]
        data_json['options'] = self.options 
        data_json['cat1'] = self.cat1
        data_json['cat2'] = self.cat2
        data_json['cat3'] = self.cat3
        data_json['cat4'] = self.cat4
        data_json['standard_cat1'] = self.standard_cat1
        data_json['standard_cat2'] = self.standard_cat2
        data_json['standard_cat3'] = self.standard_cat3
        data_json['standard_cat4'] = self.standard_cat4
        data_json['standard_cat1_id'] = self.standard_cat1_id
        data_json['standard_cat2_id'] = self.standard_cat2_id
        data_json['standard_cat3_id'] = self.standard_cat3_id
        data_json['standard_cat4_id'] = self.standard_cat4_id
        data_json['labels'] = list(self.labels)
        data_json['description'] = self.description.toDumpJSON()
        data_json['best_seller_index'] = self.best_seller_index
        return data_json

    def isValid(self):
        if len(self.variants) == 0:
            return False

        if len(self.images) == 0:
            return False

        if self.description.isValid() == False:
            return False

        return True

    def set_best_seller_index(self, index):
        self.best_seller_index = index

    def toCSV(self):
        fields = []
        fields.append(str(self.product_id))
        fields.append(self.title)
        fields.append(utils.DateFormat(self.published_at))
        fields.append(utils.DateFormat(self.created_at))
        fields.append(utils.DateFormat(self.updated_at))
        fields.append(self.cat1)
        fields.append(self.cat2)
        fields.append(self.cat3)
        fields.append(self.cat4)
        fields.append("#".join(self.labels))
        fields.append("#".join(['NA' if unit.price is None else unit.price for unit in self.variants]))
        fields.append("#".join(['NA' if unit.compare_at_price is None else unit.compare_at_price for unit in self.variants]))
        fields.append('NA' if len(self.variants) == 0 else str(self.variants[0].price))
        fields.append('NA' if len(self.variants) == 0 else str(self.variants[0].compare_at_price))
        csv_str = ",".join(['NA' if field is None else field for field in fields])
        return csv_str

        

class Variant(object):
    def __init__(self, product):
        self.product = product
        self.id = ''
        self.title = ''
        self.option1 = ''
        self.option2 = ''
        self.option3 = ''
        self.sku = ''
        self.requires_shipping = True
        self.taxable = True
        self.available = True
        self.price = 0
        self.grams = 0
        self.compare_at_price = 0
        self.postion = 0
        self.product_id = 0
        self.created_at = ''
        self.updated_at = ''
        self.featured_image = None

    def toShelfJson(self):
        prod_json = {}
        prod_json['sku'] = self.sku
        prod_json['variantId'] = self.id
        prod_json['status'] = 1
        prod_json['statusStr'] = "In Stock"
        prod_json['requiresShipping'] = "true"
        prod_json['barCode'] = ""
        prod_json['defaultImageUrl'] = ""
        prod_json['imageIndex'] = 0
        prod_json['inventoryPolicy'] = "continue"
        prod_json['inventoryManagement'] = "shopify"
        prod_json['name'] = self.title
        prod_json['sellPrice'] = self.price
        prod_json['msrp'] = self.price
        prod_json['purchasePrice'] = 0
        prod_json['inventoryQuantity'] = 1000
        prod_json['html'] = ''
        prod_json['weightUnit'] = "g"
        prod_json['weight'] = "10"
        prod_json['grams'] = ""
        prod_json['option1'] = self.option1
        prod_json['option2'] = self.option2
        prod_json['option3'] = ""
        prod_json['src'] = self.get_src()
        
        return prod_json
    
    def get_src(self):
        images = self.product.images
        src = ""
        for image in images:
            variants_ids = image.variants_ids
            for variant_id in variants_ids:
                if variant_id == self.id:
                    src = image.get_src()
        return src

    def get_sku(self):
        return self.sku

    def fromSpiderJSON(self, data_json):
        if data_json is None:
            print("warning: variant data_json is None, parse end ")
            return self
        self.id = data_json.get('id')
        self.title = data_json.get('title')
        self.option1 = data_json.get('option1')
        self.option2 = data_json.get('option2')
        self.option3 = data_json.get('option3')
        self.sku = data_json.get('sku')
        self.requires_shipping = data_json.get('requires_shipping')
        self.taxable = data_json.get('taxable')
        self.available = data_json.get('available')
        self.price = float(data_json.get('price'))
        self.grams = data_json.get('grams')
        self.compare_at_price = float(data_json.get('compare_at_price')) if data_json.get('compare_at_price') != '' and data_json.get('compare_at_price') is not None else None
        self.position = data_json.get('position')
        self.product_id = data_json.get('product_id')
        self.created_at = utils.TimeTrans(data_json.get('created_at'))
        self.updated_at = utils.TimeTrans(data_json.get('updated_at'))
        feature_image_json = data_json.get('featured_image')
        if feature_image_json is None:
            pass
            #print("featuren image json is none, title:%s" % self.product.title)
        self.feature_image = Image(self.product).fromSpiderJSON(feature_image_json)
        return self

    def get_option_info(self, index):
        options = self.product.get_options()
        cur_option = None
        for option in options:
            if option.get('position') == index:
                cur_option = option
        name = '' if cur_option is None else cur_option.get('name')
        value = ''
        if index == 1:
            value = self.option1
        elif index == 2:
            value = self.option2
        elif index == 3:
            value = self.option3
        else:
            value = ''
        return name, value

    def get_option_count(self):
        return 0 if self.product.get_options() is None else len(self.product.get_options())

    def get_price(self):
        if self.compare_at_price is not None and self.compare_at_price != 0:
            return self.compare_at_price
        return self.price

    def get_sale_price(self):
        return self.price

    def get_default_image(self):
        return self.feature_image

    def get_default_image_url(self):
        if self.get_default_image() is None:
            return ''
        default_image = self.get_default_image()
        return default_image.get_src()

    def get_default_image_index(self):
        if self.get_default_image() is None:
            return ''
        default_image = self.get_default_image()
        return default_image.get_position()

    def get_title(self):
        return self.title

    def fromLoadJSON(self, data_json):
        if data_json is None:
            print("warning: variant data_json is None, parse end ")
            return self
        self.id = data_json.get('id')
        self.title = data_json.get('title')
        self.option1 = data_json.get('option1')
        self.option2 = data_json.get('option2')
        self.option3 = data_json.get('option3')
        self.sku = data_json.get('sku')
        self.requires_shipping = data_json.get('requires_shipping')
        self.taxable = data_json.get('taxable')
        self.available = data_json.get('available')
        self.price = data_json.get('price')
        self.grams = data_json.get('grams')
        self.compare_at_price = data_json.get('compare_at_price')
        self.position = data_json.get('position')
        self.product_id = data_json.get('product_id')
        self.created_at = data_json.get('created_at')
        self.updated_at = data_json.get('updated_at')
        feature_image_json = data_json.get('featured_image')
        if feature_image_json is None:
            #print("featuren image json is none, title:%s" % self.product.title)
            pass
        self.feature_image = Image(self.product).fromLoadJSON(feature_image_json)
        return self

    def toDumpJSON(self):
        data_json = {}
        data_json['id'] = self.id
        data_json['title'] = self.title
        data_json['option1'] = self.option1
        data_json['option2'] = self.option2
        data_json['option3'] = self.option3
        data_json['sku'] = self.sku
        data_json['requires_shipping'] = self.requires_shipping
        data_json['taxable'] = self.taxable
        data_json['available'] = self.available
        data_json['price'] = self.price
        data_json['grams'] = self.grams
        data_json['compare_at_price'] = self.compare_at_price
        data_json['position'] = self.position
        data_json['product_id'] = self.product_id
        data_json['created_at'] = self.created_at
        data_json['updated_at'] = self.updated_at
        data_json['featured_image'] = None if self.featured_image is None else self.featured_image.toDumpJSON()
        return data_json



class Image(object):
    def __init__(self, product):
        self.product = product
        self.image_id = 0
        self.created_at = ''
        self.position = 0
        self.updated_at = ''
        self.product_id = 0
        self.width = 0
        self.height = 0
        self.src = ''
        self.variants_ids = []
        self.alt = ''
        self.aws_url = ''

    def toShelfProdJson(self):
        prod_json = {}
        prod_json['code'] = ""
        prod_json['image_id'] = self.image_id
        prod_json['src'] = self.get_src()
        prod_json['variantIds'] = self.variants_ids
        prod_json['width'] = self.width
        prod_json['height'] = self.height
        prod_json['position'] = self.position
        return prod_json

    def init_aws_url(self, S3):
        if self.aws_url == "" or self.aws_url is None:
            image_name = utils.image_download(self.src)
            status, image_url = utils.upload_aws(S3, image_name)
            if status == True:
                self.aws_url = image_url

    def fromSpiderJSON(self, data_json):
        if data_json is None:
            return self
        self.image_id = data_json.get('id')
        self.created_at = utils.TimeTrans(data_json.get('created_at'))
        self.position = data_json.get('position')
        self.updated_at = utils.TimeTrans(data_json.get('updated_at'))
        self.product_id = data_json.get('product_id')
        self.width = data_json.get('width')
        self.height = data_json.get('height')
        self.src = data_json.get('src')
        self.variants_ids = data_json.get('variant_ids')
        self.alt = data_json.get('alt')
        return self

    def fromLoadJSON(self, data_json):
        if data_json is None:
            return self

        self.image_id = data_json.get('image_id')
        self.created_at = data_json.get('created_at')
        self.position = data_json.get('position')
        self.updated_at = data_json.get('updated_at')
        self.product_id = data_json.get('product_id')
        self.width = data_json.get('width')
        self.height = data_json.get('height')
        self.src = data_json.get('src')
        self.variants_ids = data_json.get('variants_ids')
        self.alt = data_json.get('alt')
        return self

    def get_position(self):
        return self.position

    def get_src(self):
        #if self.aws_url is None or self.aws_url == '':
        #    self.init_aws_url()
        if self.aws_url is not None and self.aws_url != '':
            return self.aws_url
        return self.src

    def toDumpJSON(self):
        data_json = {}
        data_json['image_id'] = self.image_id
        data_json['created_at'] = self.created_at
        data_json['position'] = self.position
        data_json['updated_at'] = self.updated_at
        data_json['product_id'] = self.product_id
        data_json['width'] = self.width
        data_json['height'] = self.height
        data_json['src'] = self.src
        data_json['variants_ids'] = self.variants_ids
        data_json['alt'] = self.alt
        return data_json

class Description(object):
    def __init__(self, prod):
        self.prod = prod
        self.cm_size = []
        self.inch_size = []
        self.shipping_and_returns = []
        self.description = ""
        self.detail = {}
        self.preSale = {}
        self.size_img = '' 

    def fromHTMLData(self, body_html):
        if body_html is None or body_html == '':
            return self
        desc_html = etree.HTML(body_html)
        if self.prod.site == 'cider':
            div_data = desc_html.xpath('//div[@id="description_data"]')
            if len(div_data) == 0:
                return self

            desc_json = json.loads(div_data[0].text)
            self.cm_size = desc_json.get('size')
            self.inch_size = self.get_inch_size()
            self.shipping_and_returns = desc_json.get('shipping_and_returns')
            self.description = desc_json.get('description')
            self.detail = desc_json.get('detail')
            self.preSale = desc_json.get('preSale')
            self.has_desc = True
        elif self.prod.site in ('genzlabs'):
            self.cm_size = sc.parse_size_table(desc_html)
            self.inch_size = self.get_inch_size()
            self.shipping_and_returns = ""
            self.description = ""
            self.preSale = ""
            self.detail = sc.parse_detail(desc_html)
            self.has_desc = True
        elif self.prod.site in ('unicornbae'):
            print("hanle:%s" % self.prod.handle)
            self.cm_size = sc.parse_size_table_unicornbae(desc_html)
            self.detail = sc.parse_detail_unicornbae(desc_html)
            self.inch_size = self.get_inch_size()
            self.shipping_and_returns = ""
            self.description = ""
            self.preSalse = ""
            self.has_desc = True
        elif self.prod.site in ('ellejamilla'):
            self.size_img = sc.parse_size_image(desc_html)
            cm_size, inch_size = self.get_size_table_ellejamilla()
            self.cm_size = cm_size
            self.detail = [{}]
            self.inch_size = inch_size
            self.shipping_and_returns = ""
            self.description = ""
            self.preSalse = ""
            self.has_desc = True
        else:
            pass
        return self

    def get_cm_size(self):
        return self.cm_size

    def get_inch_size(self):
        self.inch_size = []
        if self.cm_size is None or len(self.cm_size) == 0:
            return self.inch_size

        try:
            for unit in self.cm_size:
                inch_unit = {}
                for key, value in unit.items():
                    if key.lower().find('size') != -1:
                        inch_unit[key] = value
                    else:
                        if value is None or value  == '':
                            inch_unit[key] = value
                        elif str(value).find('-') != -1:
                            key1 = value.split('-')[0]
                            key2 = value.split('-')[1]
                            key1_inch = int(utils.CMToInch(float(key1)))
                            key2_inch = int(utils.CMToInch(float(key2)))
                            inch_unit[key] = "%s-%s" % (str(key1_inch), str(key2_inch))
                        elif str(value).find(' ') != -1:
                            key1 = value.split(' ')[0]
                            key2 = value.split(' ')[1]
                            key1_inch = int(utils.CMToInch(float(key1)))
                            key2_inch = int(utils.CMToInch(float(key2)))
                            inch_unit[key] = "%s %s" % (str(key1_inch), str(key2_inch))
                        else:
                            inch_unit[key] = int(utils.CMToInch(float(value)))
                self.inch_size.append(inch_unit)
        except Exception as e:
            print(e)
            return [{}]
        return self.inch_size

    def get_size_table_ellejamilla(self):
        cm_size_first = []
        cm_size_first.append({"SIZE": "XS", "BUST": "76-79", "WAIST": "58.5-61"})
        cm_size_first.append({"SIZE": "S", "BUST": "80.5-83", "WAIST": "62.5-65"})
        cm_size_first.append({"SIZE": "M", "BUST": "88-93", "WAIST": "70-75"})
        cm_size_first.append({"SIZE": "L", "BUST": "98-103", "WAIST": "80-85"})
        cm_size_first.append({"SIZE": "XL", "BUST": "109-116", "WAIST": "91-98"})
        cm_size_first.append({"SIZE": "2XL", "BUST": "123-130", "WAIST": "105-112"})
        cm_size_first.append({"SIZE": "3XL", "BUST": "137", "WAIST": "119"})
        cm_size_first.append({"SIZE": "4XL", "BUST": "144-151", "WAIST": "126-133"})

        inch_size_first = []
        inch_size_first.append({"SIZE": "XS", "BUST": "30-31", "WAIST": "23-24"})
        inch_size_first.append({"SIZE": "S", "BUST": "32-33", "WAIST": "25-26"})
        inch_size_first.append({"SIZE": "M", "BUST": "35-37", "WAIST": "28-30"})
        inch_size_first.append({"SIZE": "L", "BUST": "39-41", "WAIST": "31-33"})
        inch_size_first.append({"SIZE": "XL", "BUST": "43-46", "WAIST": "36-39"})
        inch_size_first.append({"SIZE": "2XL", "BUST": "48-51", "WAIST": "41-44"})
        inch_size_first.append({"SIZE": "3XL", "BUST": "54", "WAIST": "47"})
        inch_size_first.append({"SIZE": "4XL", "BUST": "57-59", "WAIST": "50-52"})


        cm_size_second = []
        cm_size_second.append({"SIZE": "XS", "WAIST": "55.5-58", "HIPS": "80-82.5"})
        cm_size_second.append({"SIZE": "S", "WAIST": "60.5-63", "HIPS": "85-87.5"})
        cm_size_second.append({"SIZE": "M", "WAIST": "68-73", "HIPS": "87.5-92.5"})
        cm_size_second.append({"SIZE": "L", "WAIST": "78-83", "HIPS": "102.5-107.5"})
        cm_size_second.append({"SIZE": "XL", "WAIST": "91-98", "HIPS": "116.5-123.5"})
        cm_size_second.append({"SIZE": "2XL", "WAIST": "105-112", "HIPS": "130.5-137.5"})
        cm_size_second.append({"SIZE": "3XL", "WAIST": "119", "HIPS": "144.5"})
        cm_size_second.append({"SIZE": "4XL", "WAIST": "126-133", "HIPS": "151.5-158.5"})

        inch_size_second = []
        inch_size_second.append({"SIZE": "XS", "WAIST": "22-23", "HIPS": "31-32"})
        inch_size_second.append({"SIZE": "S", "WAIST": "24-25", "HIPS": "33-34"})
        inch_size_second.append({"SIZE": "M", "WAIST": "27-29", "HIPS": "36-38"})
        inch_size_second.append({"SIZE": "L", "WAIST": "31-33", "HIPS": "40-42"})
        inch_size_second.append({"SIZE": "XL", "WAIST": "36-39", "HIPS": "46-49"})
        inch_size_second.append({"SIZE": "2XL", "WAIST": "41-44", "HIPS": "51-54"})
        inch_size_second.append({"SIZE": "3XL", "WAIST": "47", "HIPS": "57"})
        inch_size_second.append({"SIZE": "4XL", "WAIST": "50-52", "HIPS": "60-62"})

        cm_size_three = []
        cm_size_three.append({"SIZE": "XS","BUST":"76-79", "WAIST": "58.5-61", "HIPS": "81-84"})
        cm_size_three.append({"SIZE": "S","BUST":"80.5-83", "WAIST": "62.5-65", "HIPS": "86-88.5"})
        cm_size_three.append({"SIZE": "M","BUST":"88-93", "WAIST": "70-75", "HIPS": "93.5-98.5"})
        cm_size_three.append({"SIZE": "L","BUST":"98-103", "WAIST": "80-85", "HIPS": "103.5-108.5"})
        cm_size_three.append({"SIZE": "XL","BUST":"109-116", "WAIST": "91-98", "HIPS": "116.5-123.5"})
        cm_size_three.append({"SIZE": "2XL","BUST":"123-130", "WAIST": "105-112", "HIPS": "130-137"})
        cm_size_three.append({"SIZE": "3XL","BUST":"137", "WAIST": "119", "HIPS": "151.5"})
        cm_size_three.append({"SIZE": "4XL","BUST":"144-151", "WAIST": "126-133", "HIPS": "151.5-158.5"})

        inch_size_three = []
        inch_size_three.append({"SIZE": "XS", "BUST": "30-31", "WAIST": "23-24", "HIPS": "32-33"})
        inch_size_three.append({"SIZE": "S", "BUST": "32-33", "WAIST": "25-26", "HIPS": "34-35"})
        inch_size_three.append({"SIZE": "M", "BUST": "35-37", "WAIST": "28-30", "HIPS": "37-39"})
        inch_size_three.append({"SIZE": "L", "BUST": "39-41", "WAIST": "31-33", "HIPS": "41-43"})
        inch_size_three.append({"SIZE": "XL", "BUST": "43-46", "WAIST": "36-39", "HIPS": "46-49"})
        inch_size_three.append({"SIZE": "2XL", "BUST": "48-51", "WAIST": "41-44", "HIPS": "51-54"})
        inch_size_three.append({"SIZE": "3XL", "BUST": "54", "WAIST": "47", "HIPS": "57"})
        inch_size_three.append({"SIZE": "4XL", "BUST": "57-59", "WAIST": "50-52", "HIPS": "60-62"})

        cm_size_forth = []
        cm_size_forth.append({"SIZE": "XS", "BUST": "76-79", "WAIST": "58.5-61", "HIPS": "81-84"})
        cm_size_forth.append({"SIZE": "S", "BUST": "80.5-83", "WAIST": "62.5-65", "HIPS": "86-88.5"})
        cm_size_forth.append({"SIZE": "M", "BUST": "88-93", "WAIST": "70-75", "HIPS": "93.5-98.5"})
        cm_size_forth.append({"SIZE": "L", "BUST": "98-103", "WAIST": "80-85", "HIPS": "103.5-108.5"})
        cm_size_forth.append({"SIZE": "XL", "BUST": "109-116", "WAIST": "91-98", "HIPS": "116.5-123.5"})
        cm_size_forth.append({"SIZE": "2XL", "BUST": "123-130", "WAIST": "105-112", "HIPS": "130-137"})
        cm_size_forth.append({"SIZE": "3XL", "BUST": "137", "WAIST": "119", "HIPS": "151.5"})
        cm_size_forth.append({"SIZE": "4XL", "BUST": "144-151", "WAIST": "126-133", "HIPS": "151.5-158.5"})

        inch_size_forth = []
        inch_size_forth.append({"SIZE": "XS", "BUST": "30-31", "WAIST": "23-24", "HIPS": "32-33"})
        inch_size_forth.append({"SIZE": "S", "BUST": "32-33", "WAIST": "25-26", "HIPS": "34-35"})
        inch_size_forth.append({"SIZE": "M", "BUST": "35-37", "WAIST": "28-30", "HIPS": "37-39"})
        inch_size_forth.append({"SIZE": "L", "BUST": "39-41", "WAIST": "31-33", "HIPS": "41-43"})
        inch_size_forth.append({"SIZE": "XL", "BUST": "43-46", "WAIST": "36-39", "HIPS": "46-49"})
        inch_size_forth.append({"SIZE": "2XL", "BUST": "48-51", "WAIST": "41-44", "HIPS": "51-54"})
        inch_size_forth.append({"SIZE": "3XL", "BUST": "54", "WAIST": "47", "HIPS": "57"})
        inch_size_forth.append({"SIZE": "4XL", "BUST": "57-59", "WAIST": "50-52", "HIPS": "60-62"})



        if self.prod.cat2.lower() in ('tops'):
            return cm_size_forth, inch_size_forth

        if self.prod.cat2.lower() in ('dresses', 'jumpsuits & rompers'):
            return cm_size_forth, inch_size_forth

        if self.prod.cat2.lower() in ('denim', 'bottoms') :
            return cm_size_second, inch_size_second

        if self.prod.cat2.lower() in ('swimwear', 'lingerie & sleepwear', 'loungewear'):
            return cm_size_three, inch_size_three

        return cm_size_forth, inch_size_forth
        
    def toDumpJSON(self):
        desc_json = {}
        desc_json['size'] = {"cm": self.cm_size, "inch": self.inch_size}
        desc_json['shipping_and_returns'] = self.shipping_and_returns
        desc_json['description'] = self.description
        desc_json['detail'] = self.detail
        desc_json['preSale'] = self.preSale
        desc_json['size_img'] = self.size_img
        return desc_json

    def fromLoadJSON(self, data_json):
        self.preSale = data_json.get('preSale')
        self.detail = data_json.get('detail')
        self.description = data_json.get('description')
        self.shipping_and_returns = data_json.get('shipping_and_returns')
        self.size = data_json.get('size')
        self.cm_size = self.size.get('cm')
        self.inch_size = self.size.get('inch')
        self.size_img = data_json.get('size_img')
        return self
    

    def toShelfProdJson(self):
        prod_json = {}
        new_cm_size = []
        for cm_unit in self.cm_size:
            new_cm_unit = {}
            for key, value in cm_unit.items():
                if key.lower().find('size') != -1:
                    key = 'SIZE'
                new_cm_unit[key] = value
            new_cm_size.append(new_cm_unit)
        prod_json['cm_size'] = new_cm_size
        new_inch_size = []
        for inch_unit in self.inch_size:
            new_inch_unit = {}
            for key, value in inch_unit.items():
                if key.lower().find('size') != -1:
                    key = 'SIZE'
                new_inch_unit[key] = value
            new_inch_size.append(new_inch_unit)
        prod_json['inch_size'] = new_inch_size
        prod_json['shippingAndReturns'] = self.shipping_and_returns
        prod_json['description'] = self.description
        cur_detail = {}
        prod_json['detail'] = self.detail
        prod_json['preSale'] = self.preSale
        return prod_json


if __name__ == '__main__':
    data_file = open('../data/cider_20210410.json', 'r')
    key_field = sys.argv[1]
    data = data_file.read()
    data_file.close()

    data_json = json.loads(data)
    products = []
    detail_key = set()
    detail_value = set()
    title_list = []
    for unit in data_json:
        product = Product().fromLoadJSON(unit)
        products.append(product)
        if product.description.detail is None or len(product.description.detail) == 0:
            continue
        for key, value in product.description.detail[0].items():
            if key == 'sleeve_type':
                title_list.append(product.title)
            if key == key_field:
                detail_value.add(value)
            detail_key.add(key)
    #print(title_list)
    #print(detail_key)
    #print(detail_value)

        #print(json.dumps(product.toDumpJSON()).encode(encoding='utf-8'))







class ShelfProdCheck(object):
    def __init__(self, prod):
        self.prod = prod
        
    def init_conf(self):
        with open("../conf/data_check.conf", 'r') as f:
            check_conf = f.read()
            self.check_conf_json = json.loads()
        
    def check(self):
        es_json = self.prod.toESJson()
        check_status = True
        check_result = []
        for check_item in self.check_conf_json:
            field = check_item.get('field')
            value = es_json.get(field)
            if check_item.get('empty_check') is not None and check_item.get('empty_check').get('switch') == True:
                result = cu.empty_check(value)
                if result == True:
                    check_status = False
                    check_result.append({"field": field, "reason": "emtpy check"})
            
            elif check_item.get('field_type_check') is not None and check_item.get('field_type_check').get('switch') == True:
                field_type = check_item.get('field_type_check').get('type_name')
                result = cu.field_type_check(value, field_type)
                if result == False:
                    check_status = False
                    check_result.append({"field": field, "reason": "field_type error"})

            elif check_item.get("value_range_check") is not None and check_item.get('value_range_check').get('switch') == True:
                value_range_check = check_item.get('value_range_check')
                range_start = value_range_check.get('range_start')
                range_end = value_range_check.get('range_end')
                result = True
                if value <= range_end and value >= range_start:
                    result = True
                else:
                    check_status = False
                    check_result.append({"field": field, "reason": "value range error"})
            elif check_item.get('value_enum_check') is not None and check_item.get('value_enum_check').get('switch') == True:
                value_enum_check = check_item.get('value_enum_check')
                value_list = value_enum_check.get('value_list')
                if value not in value_list:
                    check_status = False
                    check_result.append({"field": field, "reason": "value enum check error"})
        images = es_json.get('images')
        self.image_index_check(images)

    def category_check(self):
        pass


    def image_index_check(self, images):
        index = 1
        for image in images:
            if image.get('position') != index:
                return False

        return True

    def variant_image_check(self, variants):
        for variant in variants:

    def variant_size_order_check(self, variants):
        size_order = ['xxs', 'xs', 's', 'm', 'l', 'xl', '2xl', '3xl', '4xl']
        size_list = []
        for variant in variants:
            size = variant.get('option2')
            size_list.append(size)
        last_index = -1
        for size_unit in size_list:
            try:
                index = size_order.index(size_unit.lower())
                if index >= last_index:
                    last_index = index
                else:
                    return False
            except Exception as e:
                return False
        return True


    def size_table_check(self, size_table):
        if "cm" not in size_table:
            return False

        head_list = ['sleeve', 'shoulder', 'waist', 'hips', 'length', 'bust', 'top length', 'bottom length', 'cup size', 'size']
        cm_size = size_table.get('cm')
        has_size_unit = False
        for size_unit in cm_size:
            for key, value in size_unit.items():
                if key.lower() not in head_list:
                    return False
                if key.lower() == 'size':
                    has_size_unit = True
        if has_size_unit == False:
            return False


                

        



        
                    



            




# encoding=utf-8

import time
import json
import random
from common import utils
import hashlib


class ProdSPU(object):
    def __init__(self):
        self.categoryId = 0 # �
        self.number = "" # spu_id
        self.shopId = 1
        self.vendor = ""
        self.tags = []
        self.chineseTitle = ""
        self.englishTitle = ""
        self.defaultImageUrl = ""
        self.imageIndex = 1
        self.sourceType = 1
        self.html = ""
        self.productType = ""
        self.totalInventoryQuantity = 0
        self.skuList = []
        self.fulfillmentService = "manual"
        self.created_at = 0
        self.updated_at = 0
        self.createdByName = "tangyipeng"
        self.updatedByName = "tangyipeng"
        self.createdAt = int(time.time() * 1000)
        self.updatedAt = int(time.time() * 1000)
        self.createdBy = 0
        self.updatedBy = 0
        self.status = 'active'
        self.spu = ""
        self.detail_url = ""
        self.init_prod_conf()
        self.platform_id = 1
        self.cat1 = ''
        self.cat2 = ''
        self.cat3 = ''
        self.cat4 = ''
        self.standard_cat1_id = 0
        self.standard_cat2_id = 0
        self.standard_cat3_id = 0
        self.standard_cat4_id = 0
        self.stardard_cat1 = ''
        self.stardard_cat2 = ''
        self.stardard_cat3 = ''
        self.stardard_cat4 = ''

    def init_prod_conf(self):
        infringe_path = '../conf/cider_infringe_blacklist.conf'
        infringe_file = open(infringe_path, 'r')
        lines = infringe_file.readlines()
        infringe_file.close()
        self.infringe_black_list_handle = [unit.strip() for unit in lines]

    def add_sku(self, sku_unit):
        self.skuList.append(sku_unit)

    def get_output_tags(self):
        output_tags = []
        output_tags = output_tags + list(self.labels)
        if self.standard_cat4 != '' and self.standard_cat4 is not None:
            output_tags.append(self.standard_cat4)

        if self.standard_cat3 != '' and self.standard_cat3 is not None:
            output_tags.append(self.standard_cat3)

        if self.standard_cat2 != '' and self.standard_cat2 is not None:
            output_tags.append(self.standard_cat2)

        if self.standard_cat1 != '' and self.standard_cat1 is not None:
            output_tags.append(self.standard_cat1)

        output_tags = list(set(output_tags))

        # erase blank tag
        
        output_tags = filter(lambda x: (x != '' and x is not None), output_tags)
        return output_tags

    def toJson(self):
        spu_json = {}
        # spu_json['id'] = random.randint(0,100000000)
        spu_json['categoryId'] = '0'
        #spu_json['number'] = str(self.number)
        spu_json['number'] = self.number 
        spu_json['spu'] = self.spu
        spu_json['handle'] = self.handle
        spu_json['shopId'] = self.shopId
        spu_json['vendor'] = 'waves'
        #spu_json['categoryId'] = 6629907071139
        spu_json['tags'] = ",".join(self.get_output_tags())
        spu_json['chineseTitle'] = self.chineseTitle
        spu_json['englishTitle'] = self.englishTitle
        spu_json['defaultImageUrl'] = self.defaultImageUrl
        spu_json['imageIndex'] = self.imageIndex
        spu_json['sourceType'] = self.sourceType
        spu_json['totalInventoryQuantity'] = self.totalInventoryQuantity
        sku_json = []
        for sku in self.skuList:
             sku_json.append(sku.toJson())
        #sku_json.append(self.skuList[0].toJson())
        spu_json['skuList'] = sku_json
        spu_json['html'] = self.html
        #spu_json['html'] = ''
        spu_json['productType'] = self.productType
        spu_json['fulfillmentService'] = self.fulfillmentService
        spu_json['createdByName'] = self.createdByName
        spu_json['updatedByName'] = self.updatedByName
        spu_json['createdAt'] = self.createdAt
        spu_json['createdBy'] = self.createdBy
        spu_json['updatedBy'] = self.updatedBy
        spu_json['updatedAt'] = self.updatedAt
        new_options = []
        for option in self.options:
            new_option = {}
            new_option['name'] = option.get('name')
            new_option['value'] = ",".join(option.get('values'))
            new_options.append(new_option)
        spu_json['productSkuPropertyList'] = new_options
        spu_json['images'] = [image.toJSON() for image in self.images]
        spu_json['status'] = self.status
        spu_json['htmlBody'] = self.description.toJSON()

        return spu_json

    def get_image(self, image_id):
        for image in self.images:
            if image.image_id == image_id:
                return image
        return None

    def fromJSON(self, data_json):
        self.number = data_json.get('number')
        self.shop_id = data_json.get('shop_id')
        self.vendor = data_json.get('vendor')
        self.handle = data_json.get('handle')
        self.tags = data_json.get('tags')
        self.chineseTitle = data_json.get('chineseTitle')
        self.englishTitle = data_json.get('englishTitle')
        self.defaultImageUrl = data_json.get('defaultImageUrl')
        self.html = data_json.get('html')
        self.imageIndex = data_json.get('imageIndex')
        self.source_type = data_json.get('source_type')
        self.totalInventoryQuantity = data_json.get('totalInventoryQuantity')
        self.options = data_json.get('options')
        self.spu = data_json.get('spu')
        self.createdByName = data_json.get('createdByName')
        self.updatedByName = data_json.get('updatedByName')
        self.createdAt = data_json.get('createdAt')
        self.updatedAt = data_json.get('updatedAt')
        self.createdBy = data_json.get('createdBy')
        self.updatedBy = data_json.get('updatedBy')
        self.productType = data_json.get('productType')
        self.labels= data_json.get('labels')
        self.images = [ImageSPU(self).fromJSON(unit) for unit in data_json.get('images')]
        self.skuList = [ProdSKU(self).fromJSON(unit) for unit in data_json.get('variants')]
        self.description = DescSPU(self).fromJSON(data_json.get('description'))
        self.detail_url = data_json.get("detail_url")
        self.cat1 = data_json.get('cat1')
        self.cat2 = data_json.get('cat2')
        self.cat3 = data_json.get('cat3')
        self.cat4 = data_json.get('cat4')
        self.standard_cat1_id = data_json.get('standard_cat1_id')
        self.standard_cat2_id = data_json.get('standard_cat2_id')
        self.standard_cat3_id = data_json.get('standard_cat3_id')
        self.standard_cat4_id = data_json.get('standard_cat4_id')
        self.standard_cat1 = data_json.get('standard_cat1')
        self.standard_cat2 = data_json.get('standard_cat2')
        self.standard_cat3 = data_json.get('standard_cat3')
        self.standard_cat4 = data_json.get('standard_cat4')
        self.platform_id = data_json.get('platform_id')
        return self

    def get_spu_code(self, sku_code):
        if sku_code.find('CD202011090091SH') != -1:
            return sku_code.split('-')[0]

        option_count = len(self.options)
        spu_code = "-".join(sku_code.split('-')[0:-1 * (option_count)])
        return spu_code


    def fromSpiderData(self, product):
        self.number = product.get_product_id()
        self.shop_id = 1
        self.vendor = ""
        self.handle = product.handle
        self.tags = product.get_tags()
        self.chineseTitle = product.get_title()
        self.englishTitle = product.get_title()
        self.defaultImageUrl = product.get_default_image_url()
        self.html = product.get_body_html()
        self.imageIndex = 0
        self.source_type = 1
        self.totalInventoryQuantity = 10000 
        self.options = product.options
        self.description = DescSPU(self).fromSpiderData(product.description, self.options) 
        for variant in product.variants:
            prod_sku = ProdSKU(self).fromSpiderData(variant, self.description)
            spu_code = self.get_spu_code(prod_sku.sku) 
            self.skuList.append(prod_sku)
        self.spu = spu_code
        self.createdByName = "tangyipeng"
        self.updatedByName = "tangyipeng"
        self.createdAt = int(time.time() * 1000)
        self.updatedAt = int(time.time() * 1000)
        self.createdBy = 0
        self.updatedBy = 0
        self.productType = product.get_product_type()
        self.images = []
        self.labels = self.get_labels(product.labels)
        for image in product.images:
            image_spu = ImageSPU(self).fromSpiderData(image) 
            self.images.append(image_spu)

        return self

    def get_labels(self, labels):
        if "THE CIDER BASICS" in labels:
            new_labels = labels.copy()
            new_labels.add("THE BASICS")
            return new_labels
        return labels

    def is_valid(self):
        if self.handle in self.infringe_black_list_handle:
            return False
        return True

    def compareUpdateDiff(self, spu):
        # image priority
        if len(self.images) != len(spu.images):
            return False, 'image count change, old_count:%s, new_count:%s' % (len(spu.images), len(self.images)), 'image_count'
        for image in self.images:
            for other_image in spu.images:
                if image.image_id == other_image.image_id:
                    status, reason, reason_type = image.compareUpdateDiff(other_image)
                    if status == False:
                        return False, reason, reason_type

        compare_field_list = ['handle', 'chineseTitle', 'englishTitle' ]
        for field in compare_field_list:
            status = utils.DataCompare(getattr(self, field), getattr(spu, field))
            if status == False:
                print("field:%s, title: new_value:%s, old_value:%s, self.chineseTitle:%s, diff" % (field, str(getattr(self, field)).encode(encoding='utf-8'), str(getattr(spu, field)).encode(encoding='utf-8'), self.chineseTitle.encode(encoding='utf-8')))
                return False, 'base info change, field:%s, new_value:%s, old_value:%s' % (field, getattr(self, field), getattr(spu, field) ), "base_info_%s" % (field)

        # new_default_image_url = self.defaultImageUrl if self.defaultImageUrl is None else self.defaultImageUrl.split('?')[0]
        # print(new_default_image_url)
        # old_default_image_url = spu.defaultImageUrl if spu.defaultImageUrl is None else spu.defaultImageUrl.split('?')[0]
        # print(old_default_image_url)
        # status = utils.DataCompare(new_default_image_url, old_default_image_url)
        # if status == False:
        #     return False


        if len(self.skuList) != len(spu.skuList):
            return False, 'sku count change, old_count:%s, new_count:%s' % (len(spu.skuList), len(self.skuList)), "sku_count"
        for variant in self.skuList:
            for other_variant in spu.skuList:
                if variant.sku == other_variant.sku:
                    status, reason, reason_type = variant.compareUpdateDiff(other_variant)
                    if status == False:
                        return False, reason, reason_type
        status, reason, reason_type = self.description.compareUpdateDiff(spu.description)
        if status == False:
            return False, reason, reason_type

        return True, '', '' 

    def compareReviewDiff(self, spu):
        #if len(self.images) != len(spu.images):
        #    return False
        #for image in self.images:
        #    for other_image in spu.images:
        #        if image.image_id == other_image.image_id:
        #            status = image.compareReviewDiff(other_image)
        #            if status == False:
        #                return False

        if len(self.skuList) != len(spu.skuList):
            return False
        for variant in self.skuList:
            for other_variant in spu.skuList:
                if variant.sku == other_variant.sku:
                    status = variant.compareReviewDiff(other_variant)
                    if status == False:
                        return False

        status = self.description.compareReviewDiff(spu.description)
        if status == False:
            return False

        return True

    def toESJSON(self):
        platform_id = self.platform_id
        region_id = 1
        md5 = hashlib.md5()
        md5.update(("%s_%s_%s" % (platform_id, region_id, self.number)).encode('utf-8'))
        es_doc_id = md5.hexdigest()

        prod_json = {}
        prod_json['platform_id'] = platform_id
        prod_json['spu'] = self.spu
        prod_json['img_src'] = self.defaultImageUrl
        prod_json['detail_url'] = self.detail_url
        prod_json['title'] = self.englishTitle
        prod_json['body_html'] = self.html
        prod_json['vendor'] = self.vendor
        prod_json['handle'] = self.handle
        prod_json['price'] = self.skuList[0].get_price() 
        prod_json['product_type'] = self.productType
        prod_json['created_at'] = int(self.createdAt)
        prod_json['shipping_and_returns'] = self.description.shipping_and_returns
        prod_json['pre_sale'] = 'In Stock'
        prod_json['options'] = self.options
        # prod_json['size'] = {"cm": self.desc.cm_size, "inch": self.desc.inch_size}
        prod_json['size'] = {"cm": self.description.cm_size, "inch": self.description.inch_size}
        prod_json['images'] = [image.toESJSON() for image in self.images]
        prod_json['variants'] = [variant.toESJSON() for variant in self.skuList]
        prod_json['bs_html_body'] = self.description.toESJSON()
        print(prod_json['created_at'])
        prod_json['launch_time'] = utils.DatetimeFormat(prod_json['created_at'] / 1000)
        prod_json['sku_count'] = len(self.skuList)
        prod_json['category_1_id'] = self.standard_cat1_id
        prod_json['category_2_id'] = self.standard_cat2_id
        prod_json['category_3_id'] = self.standard_cat3_id
        prod_json['category_4_id'] = self.standard_cat4_id
        color_count = 0
        size_count = 0
        for option in self.options:
            if option.get('name').lower() == 'color':
                color_count = len(option.get('values'))
            if option.get('name').lower() == 'size':
                size_count = len(option.get('values'))
        prod_json['size_count'] = size_count
        prod_json['color_count'] = color_count

        output_tags = self.get_output_tags()
        prod_json['tags_list'] = [{"tag_id": "", "tag": tag_name} for tag_name in output_tags] 
        return es_doc_id, prod_json

class ProdSKU(object):
    def __init__(self, prod_spu):
        self.sku_id = 0
        self.sku = ""
        self.status = 1
        self.barCode = ""
        self.defaultImageUrl = ""
        self.imageIndex = ""
        self.inventoryPolicy = "continue"
        self.inventoryManagement = "shopify"
        self.name = ""
        self.sellPrice = 0
        self.msrp = 0
        self.purchasePrice = 0
        self.inventoryQuantity = 0
        self.html = ""
        self.productSkuPropertyList = []
        self.requiresShipping = 'true'
        self.provider_platform = ""
        self.provider_name = ""
        self.weight_unit = ''
        self.src = ""
        self.prod_spu = prod_spu

    def get_price(self):
        return self.sellPrice

    def get_src_test (self):
        images = self.prod_spu.images
        src = ""
        for image in images:
            variantIds = image.variantIds
            for variant_id in variantIds:
                if variant_id == self.sku_id:
                    src = image.get_src()
        return src
    
    def get_src(self):
        return self.src


    def toESJSON(self):
        prod_json = {}
        prod_json['id'] = self.sku_id
        prod_json['requires_shipping'] = self.requiresShipping
        prod_json['title'] = self.name
        prod_json['sku'] = self.sku
        prod_json['option1'] = self.option1
        prod_json['option2'] = self.option2
        prod_json['option3'] = self.option3
        prod_json['price'] = self.sellPrice
        prod_json['purchase_price'] = self.purchasePrice
        # prod_json['source_type'] = self.source_type
        prod_json['source_type'] = ""
        # prod_json['purchase_type'] = self.purchase_type
        prod_json['purchase_type'] = ""
        prod_json['provider_platform'] = self.provider_platform
        prod_json['provider_name'] = self.provider_name
        prod_json['compare_at_price'] = self.msrp
        prod_json['barcode'] = self.barCode
        prod_json['grams'] = self.grams
        prod_json['image_index'] = self.imageIndex
        prod_json['src'] = self.get_src()
        prod_json['inventory_quantity'] = self.inventoryQuantity
        prod_json['inventory_management'] = self.inventoryManagement
        prod_json['inventory_policy'] = self.inventoryPolicy
        prod_json['weight'] = self.weight
        prod_json['weight_unit'] = self.weight_unit

        return prod_json

    def set_property(self, name, value):
        for property in self.productSkuPropertyList:
            if property["name"] == name:
                property["value"] = value
                return
        property = {}
        property["id"] = 0
        property["name"] = name
        property["value"] = value
        self.productSkuPropertyList.append(property)

    def toJson(self):
        sku_json = {}
        # sku_json['id'] = random.randint(0,10000)
        sku_json["sku"] = self.sku
        sku_json['variantId'] = self.sku_id
        sku_json["status"] = self.status
        sku_json["statusStr"] = self.status_str
        sku_json['requiresShipping'] = self.requiresShipping
        sku_json["barCode"] = self.barCode
        sku_json["defaultImageUrl"] = self.defaultImageUrl
        sku_json["imageIndex"] = self.imageIndex
        sku_json["inventoryPolicy"] = self.inventoryPolicy
        sku_json["inventoryManagement"] = self.inventoryManagement
        sku_json["name"] = self.name
        sku_json["sellPrice"] = self.sellPrice
        sku_json["msrp"] = self.msrp
        sku_json["purchasePrice"] = self.purchasePrice
        sku_json["inventoryQuantity"] = self.inventoryQuantity
        sku_json["html"] = ""
        #sku_json["productSkuPropertyList"] = self.productSkuPropertyList
        sku_json['weightUnit'] = 'g'
        sku_json['weight'] = 10
        sku_json['grams'] = self.grams
        sku_json['option1'] = self.option1
        sku_json['option2'] = self.option2
        sku_json['option3'] = self.option3
        return sku_json

    def fromSpiderData(self, variant, desc):
        self.sku = variant.get_sku()
        self.sku_id = str(variant.id)
        self.barcode = ""
        self.defaultImageUrl = variant.get_default_image_url()
        self.imageIndex = variant.get_default_image_index()
        self.name = variant.get_title()
        self.sellPrice = float(variant.get_price())
        self.msrp = float(variant.get_sale_price())
        self.purchasePrice = 0
        self.inventoryQuantity = 1000
        self.html = ""
        self.option1 = variant.option1
        self.option2 = variant.option2
        self.option3 = variant.option3
        self.grams = variant.grams
        status_info = desc.get_stock_info(self.option1, self.option2)
        self.status = status_info.get('stock_status')
        self.status_str = status_info.get('stock_status_str')
        #print(variant.get_option_count())
        #for index in range(variant.get_option_count()):
        #    name, value = variant.get_option_info(index + 1)
        #    self.set_property(name, value)
        return self

    def fromJSON(self, data_json):
        self.sku = data_json.get('sku')
        self.sku_id = data_json.get('variantId')
        self.status = data_json.get('status')
        self.status_str = data_json.get('statusStr')
        self.requiresShipping = data_json.get('requiresShipping')
        self.barCode = data_json.get('barCode')
        self.defaultImageUrl = data_json.get('defaultImageUrl')
        self.imageIndex = data_json.get('imageIndex')
        self.inventoryPolicy = data_json.get('inventoryPolicy')
        self.inventoryManagement = data_json.get('inventoryManagement')
        self.name = data_json.get('name')
        self.sellPrice = data_json.get('sellPrice')
        self.msrp = data_json.get('msrp')
        self.created_at = data_json.get('createdAt')
        print("create_at:%s" % self.created_at)
        self.purchasePrice = data_json.get('purchasePrice')
        self.inventoryQuantity = data_json.get('inventoryQuantity')
        self.html = data_json.get('html')
        self.weightUnit = data_json.get('weightUnit')
        self.weight = data_json.get('weight')
        self.grams = data_json.get('grams')
        self.option1 = data_json.get('option1')
        self.option2 = data_json.get('option2')
        self.option3 = data_json.get('option3')
        self.src = data_json.get('src')
        print("sku src:%s" % data_json.get('src'))
        return self

    def compareUpdateDiff(self, sku):
        compare_field_list = ['status', 'sellPrice', 'msrp' ]
        for field in compare_field_list:
            status = utils.DataCompare(getattr(self, field), getattr(sku, field))
            if status == False:
                return False, "sku info diff, field:%s, new_value:%s, old_value:%s" % (field, getattr(self, field), getattr(sku, field)), 'sku_%s' % (field)

        return True, '', ''
           
    def compareReviewDiff(self, sku):
        compare_field_list = ['sellPrice', 'msrp', 'status', 'status_str']
        for field in compare_field_list:
            status = utils.DataCompare(getattr(self, field), getattr(sku, field))
            if status == False:
                return False

class ImageSPU(object):
    def __init__(self, SPU):
        self.SPU = SPU
        pass

    def fromSpiderData(self, image):
        self.image_id = image.image_id
        self.code = image.position
        self.src = image.src
        self.variantIds = image.variants_ids
        self.width = image.width
        self.height = image.height
        self.position = image.position
        return self

    def get_src(self):
        return self.src

    def toESJSON(self):
        prod_json = {}
        prod_json['platform_image_id'] = self.image_id
        prod_json['code'] = self.code
        prod_json['src'] = self.src
        prod_json['variantIds'] = self.variantIds
        prod_json['width'] = self.width
        prod_json['height'] = self.height
        prod_json['position'] = self.position
        return prod_json

    def toJSON(self):
        image_json = {}
        image_json['code'] = self.code
        image_json['src'] = self.src
        image_json['variantIds'] = self.variantIds
        image_json['width'] = self.width
        image_json['height'] = self.height
        image_json['position'] = self.position
        return image_json

    def fromJSON(self, data_json):
        self.code = data_json.get('code')
        self.image_id = data_json.get('image_id')
        self.src = data_json.get('src')
        self.variantIds = data_json.get('variantIds')
        self.position = data_json.get('position', 0)
        self.width = data_json.get('width')
        self.height = data_json.get('height')
        return self

    def compareUpdateDiff(self, image):
        compare_field_list = ['src']
        for field in compare_field_list:
            new_value = getattr(self, field)
            old_value = getattr(image, field)
            if field == 'src':
                new_value = new_value if new_value is None else new_value.split("?")[0]
                old_value = old_value if old_value is None else old_value.split("?")[0]
            status = utils.DataCompare(new_value, old_value)
            if status == False:
                print("image diff, field:%s, new_value:%s, old_value:%s, title:%s" % (field, getattr(self, field), getattr(image, field), self.SPU.chineseTitle))
                return False, "image info diff, field:%s, new_value:%s, old_value:%s" % (field, getattr(self, field), getattr(image, field)), 'image_%s' % (field)

        return True, '', ''

    def compareReviewDiff(self, image):
        compare_field_list = ['src', 'width', 'height']
        for field in compare_field_list:
            new_value = getattr(self, field)
            old_value = getattr(image, field)
            if field == 'src':
                new_value = new_value if new_value is None else new_value.split("?")[0]
                old_value = old_value if old_value is None else old_value.split("?")[0]
            status = utils.DataCompare(new_value, old_value)
            if status == False:
                print("desc diff, field:%s, new_value:%s, old_value:%s, title:%s" % (field, new_value, old_value, self.SPU.chineseTitle))
                return False

        return True

class DescSPU(object):
    def __init__(self, SPU):
        self.SPU = SPU
        pass

    def fromSpiderData(self, desc, sku_prop_list):
        self.cm_size = desc.cm_size
        self.inch_size = desc.inch_size
        self.shipping_and_returns = desc.shipping_and_returns
        self.description = desc.description
        self.detail = self.detail_clean(desc.detail)
        self.preSale = desc.preSale
        self.init_stock_info(sku_prop_list)
        return self

    def fromJSON(self, data_json):
        self.cm_size = data_json.get('cm_size')
        self.inch_size = data_json.get('inch_size')
        self.shipping_and_returns = data_json.get('shippingAndReturns')
        self.description = data_json.get('description')
        self.detail = data_json.get('detail')
        self.preSale = data_json.get('preSale')
        return self

    def detail_clean(self, detail):
        if detail is None or (isinstance(detail, dict) and len(detail.keys()) == 0):
            return detail

        detail_field = ['category', 'sleeve_length', 'sleeve_type', 'fit', 'neckline', 'waist', 'pattern', 'composition', 'type']
        new_detail = {}
        detail = detail[0]
        for field in detail_field:
            field_value = detail.get(field)
            if field_value != '' and field_value is not None:
                new_detail[field] = field_value
        return new_detail

    def init_stock_info(self, sku_prop_list):
        self.stock_info = {}
        if self.preSale is None:
            return 
        if self.preSale == '' or self.preSale is None:
            return 
        estimateDeliveryTime = self.preSale.get('estimateDeliveryTime')
        if estimateDeliveryTime is None or len(estimateDeliveryTime) == 0:
            return 

        for estimate_delivery_unit in estimateDeliveryTime:
            for key, value in estimate_delivery_unit.items():
                if key == 'color':
                    color = value
                elif key == 'size':
                    for size_unit in value:
                        stock_status_str = size_unit['time']
                        size = size_unit['size']

                        key = "%s_%s" % (color, size)
                        if stock_status_str == 'Out of stock':
                            stock_status = 2
                        elif stock_status_str == 'In stock':
                            stock_status = 1
                        elif stock_status_str.find('Pre-order') != -1:
                            stock_status = 3
                        else:
                            stock_status = -1

                        if stock_status == 2 or stock_status == 3:
                            stock_status = 1
                            stock_status_str = 'In stock'
                        self.stock_info[key] = {"stock_status_str":stock_status_str, "stock_status": stock_status}

    def toJSON(self):
        desc_json = {}
        if len(self.cm_size) == 0 or len(self.inch_size) == 0:
            desc_json['size'] = None
        else:
            desc_json['size'] = {"cm": self.cm_size, "inch": self.inch_size}
        desc_json['shippingAndReturns'] = self.shipping_and_returns
        desc_json['description'] = self.description
        desc_json['detail'] = self.detail
        desc_json['preSale'] = self.preSale
        return desc_json
    
    def toESJSON(self):
        desc_json = {}
        if len(self.cm_size) == 0 or len(self.inch_size) == 0:
            desc_json['size'] = None
        else:
            desc_json['size'] = {"cm": self.cm_size, "inch": self.inch_size}
        desc_json['shipping_and_returns'] = self.shipping_and_returns
        desc_json['description'] = self.description
        desc_json['detail'] = self.detail
        return desc_json

    def compareUpdateDiff(self, desc):
        compare_field_list = ['cm_size', 'inch_size', 'shipping_and_returns', 'description', 'detail']
        for field in compare_field_list:
            status = utils.DataCompare(getattr(self, field), getattr(desc, field))
            if status == False:
                print("desc diff, field:%s, new_value:%s, old_value:%s, title:%s" % (field, getattr(self, field), getattr(desc, field), self.SPU.chineseTitle))
                return False, "description info change, field:%s, new_value:%s, old_value:%s" % (field, getattr(self, field), getattr(desc, field)), "desc_%s" % (field)

        return True, '', ''

    def compareReviewDiff(self, desc):
        compare_field_list = ['shipping_and_returns', 'description', 'detail']
        for field in compare_field_list:
            status = utils.DataCompare(getattr(self, field), getattr(desc, field))
            if status == False:
                print("desc diff, field:%s, new_value:%s, old_value:%s, title:%s" % (field, getattr(self, field), getattr(desc, field), self.SPU.chineseTitle))
                return False

        return True

    def get_stock_info(self, color, size):
        key = "%s_%s" % (color, size)
        if key in self.stock_info:
            return self.stock_info.get(key)
        else:
            return {"stock_status_str": "In stock", "stock_status": 1}
        


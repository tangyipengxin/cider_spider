# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
from common import utils
from lxml import etree
import json
import time
import random

from common import emmiol_common as ec



class EmmiolProdEntity(object):
    def __init__(self):
        self.spu_id = 0
        self.spu_name = ""
        self.spu_code = ""
        self.cat1 = ""
        self.cat2 = ""
        self.cat3 = ""
        self.cat4 = ""
        self.standard_cat1_id = 0
        self.standard_cat2_id = 0
        self.standard_cat3_id = 0
        self.standard_cat4_id = 0
        self.standard_cat1 = ""
        self.standard_cat2 = ""
        self.standard_cat3 = ""
        self.standard_cat4 = ""
        self.original_cat_id = 0
        self.sku_list = []
        self.images = []
        self.description = None
        self.tags = []
        self.collection_labels = []
        self.init_detail_url = ""
        self.pre_sale = ""

    def init_standard_cat_info(self):
        dest_cat_info = ec.category_mapping(int(self.original_cat_id))
        self.standard_cat1_id = dest_cat_info.get('cat1_id')
        self.standard_cat2_id = dest_cat_info.get('cat2_id')
        self.standard_cat3_id = dest_cat_info.get('cat3_id')
        self.standard_cat4_id = dest_cat_info.get('cat4_id')
        self.standard_cat1 = dest_cat_info.get('cat1_name')
        self.standard_cat2 = dest_cat_info.get('cat2_name')
        self.standard_cat3 = dest_cat_info.get('cat3_name')
        self.standard_cat4 = dest_cat_info.get('cat4_name')


    def toDumpJSON(self):
        prod_json = {}
        prod_json['spu_id'] = self.spu_id   # 待解决
        prod_json['spu_name'] = self.spu_name 
        prod_json['spu_code'] = self.spu_code
        prod_json['cat1'] = self.cat1
        prod_json['cat2'] = self.cat2
        prod_json['cat3'] = self.cat3
        prod_json['cat4'] = self.cat4
        prod_json['standard_cat1_id'] = self.standard_cat1_id
        prod_json['standard_cat2_id'] = self.standard_cat2_id
        prod_json['standard_cat3_id'] = self.standard_cat3_id
        prod_json['standard_cat4_id'] = self.standard_cat4_id
        prod_json['standard_cat1'] = self.standard_cat1
        prod_json['standard_cat2'] = self.standard_cat2
        prod_json['standard_cat3'] = self.standard_cat3
        prod_json['standard_cat4'] = self.standard_cat4
        prod_json['original_cat_id'] = self.original_cat_id
        prod_json['sku_list'] = [unit.toDumpJSON() for unit in self.sku_list] 
        prod_json['image_list'] = [unit.toDumpJSON() for unit in self.images] 
        prod_json['tags'] = self.tags
        prod_json['collection_labels'] = self.collection_labels
        prod_json['desc'] = self.description.toDumpJSON()
        prod_json['default_image_url'] = self.default_image.toDumpJSON()
        prod_json['pre_sale'] = self.pre_sale
        return prod_json

    def fromJSON(self, data_json):
        self.spu_id = data_json.get('spu_id')
        self.spu_name = data_json.get('spu_name')
        self.spu_code = data_json.get('spu_code')
        self.cat1 = data_json.get('cat1')
        self.cat2 = data_json.get('cat2')
        self.cat3 = data_json.get('cat3')
        self.cat4 = data_json.get('cat4')
        self.standard_cat1 = data_json.get('standard_cat1')
        self.standard_cat2 = data_json.get('standard_cat2')
        self.standard_cat3 = data_json.get('standard_cat3')
        self.standard_cat4 = data_json.get('standard_cat4')
        self.standard_cat1_id = data_json.get('standard_cat1_id')
        self.standard_cat2_id = data_json.get('standard_cat2_id')
        self.standard_cat3_id = data_json.get('standard_cat3_id')
        self.standard_cat4_id = data_json.get('standard_cat4_id')
        self.original_cat_id = data_json.get('original_cat_id')
        self.tags = data_json.get('tags')
        self.collection_labels = data_json.get('collection_labels')
        self.description = EmmiolDesc().fromJSON(data_json.get('desc'))
        images = data_json.get('image_list')
        index = 1
        for image in images:
            image['position'] = index
            index = index + 1
        self.images = [EmmiolImage(self).fromJSON(unit) for unit in images]
        self.sku_list = [EmmiolSKU(self).fromJSON(unit) for unit in data_json.get('sku_list')]
        if "default_image_url" in data_json and type(data_json.get('default_image_url')) != dict:
            default_image_url = data_json.get('default_image_url')
            self.default_image = EmmiolImage(self)
            self.default_image.set_image_id(default_image_url)
            self.default_image.set_src(default_image_url)

        else:
            default_image_json = data_json.get('default_image_url')
            self.default_image = EmmiolImage(self).fromJSON(default_image_json)
        # self.init_detail_url = data_json.get('init_detail_url')
        self.pre_sale = data_json.get('pre_sale')
        return self

    def get_image(self, image_id):
        for image in self.images:
            if image.image_id == image_id:
                return image
        print("warning: image_id:%s not exist" % (image_id))
        return None

    def get_image_id(self, url):
        return url.split("-")[-1].split(".")[0]

    def set_color_images(self, color, images):
        for sku in self.sku_list:
            if sku.color == color:
                sku.images = images

    def add_image(self, image):
        self.images.append(image)

    def set_desc(self, description):
        self.description = description

    def get_price(self):
        return self.price

    def add_sku(self, sku):
        for cur_sku in self.sku_list:
            if cur_sku.color == sku.color and cur_sku.size == sku.size:
                return 
        self.sku_list.append(sku)

    def set_color_image_list(self, color, images):
        for sku in self.sku_list:
            if sku.get_color() == color:
                sku.set_images(images)

    def extract_spu_code(self, item_id):
        if item_id.find('-') == -1:
            spu_code = item_id[0:9]
        else:
            spu_code = item_id.split('-')
        return spu_code

    def init_spu_id(self, spu_code):
        self.spu_id = utils.gene_id_fromstring(spu_code)

    def from_json(self, data_json):
        cat_list = data_json.get('item_list_name').split('>')
        self.cat1 = cat_list[0].strip() if len(cat_list) >= 1 else ''
        self.cat2 = cat_list[1].strip() if len(cat_list) >= 2 else ''
        self.cat3 = cat_list[2].strip() if len(cat_list) >= 3 else ''
        self.cat4 = cat_list[3].strip() if len(cat_list) >= 4 else ''
        self.original_cat_id = data_json.get('item_list_id')
        self.spu_name = data_json.get('item_name')

        size = data_json.get('item_variant').split('/')[1].strip()
        self.spu_code = ec.extract_spu_code(data_json.get('item_id'))
        self.init_spu_id(self.spu_code)
        sku = EmmiolSKU(self)
        sku.sku_name = "%s %s" % (data_json.get("item_name"), data_json.get('item_variant'))
        sku.sku_code = data_json.get('item_id')
        sku.set_detail_url(data_json.get('detail_url'))
        sku.price = data_json.get('price')
        sku.set_color(data_json.get('item_variant').split('/')[0].strip())
        sku.set_size(data_json.get('item_variant').split('/')[1].strip())
        self.init_detail_url = data_json.get('detail_url')
        self.init_sku_id = ec.extract_sku_id(self.init_detail_url)
        self.price = data_json.get('price')
        self.default_image = EmmiolImage(self)
        self.default_image.set_image_id(data_json['first_image'])
        self.default_image.set_src(data_json['first_image'])
        return self

    def add_collection_label(self, label):
        self.collection_labels.append(label)

    def set_pre_sale(self, pre_sale):
        self.pre_sale = pre_sale

    def init_sku_code(self):
        used_sku_codes = []
        for sku in self.sku_list:
            if sku.sku_code != '':
                used_sku_code = sku.sku_code.replace(sku.size, "")
                used_sku_codes.append(used_sku_code)
        sku_count = len(self.sku_list)
        available_sku_code = []
        for index in range(sku_count):
            sku_index = index + 1
            new_sku_code = "%s%s" % (self.spu_code, "0%d" % sku_index if sku_index > 10 else "00%s" % (sku_index))
            if new_sku_code not in used_sku_codes:
                available_sku_code.append(new_sku_code)

        index = 0
        for sku in self.sku_list:
            if sku.sku_code == "":
                sku.sku_code = "%s%s" % (available_sku_code[index], sku.size)
                index = index + 1

    def get_handle(self):
        if self.spu_name is None:
            return None

        return "-".join(self.spu_name.split(" "))

    def get_options(self):
        color_list = []
        size_list = []
        for sku in self.sku_list:
            color = sku.color
            size = sku.size
            if color not in color_list:
                color_list.append(color)
            if size not in size_list:
                size_list.append(size)
        options = []
        options.append({"name": "Color", "position": 1, "values": color_list})
        options.append({"name": "Size", "position": 2, "values": size_list})
        return options

    def get_product_type(self):
        if self.standard_cat4 != "" and self.standard_cat4 is not None:
            return self.standard_cat4

        if self.standard_cat3 != "" and self.standard_cat3 is not None:
            return self.standard_cat3

        if self.standard_cat2 != "" and self.standard_cat2 is not None:
            return self.standard_cat2

        if self.standard_cat1 != "" and self.standard_cat1 is not None:
            return self.standard_cat1

        return ""

    def get_labels(self):
        return self.collection_labels if self.collection_labels is not None else []

    def get_tags(self):
        if self.cat1 is not None and self.cat1 != '':
            self.tags.append(self.cat1)
        if self.cat2 is not None and self.cat2 != '':
            self.tags.append(self.cat2)
        if self.cat3 is not None and self.cat3 != '':
            self.tags.append(self.cat3)
        if self.cat4 is not None and self.cat4 != '':
            self.tags.append(self.cat4)


        return list(set(self.tags))

    def get_spu_id(self):
        return self.spu_id

    def get_spu_name(self):
        return self.spu_name

    def get_spu_code(self):
        return self.spu_code

    def get_default_image_url(self):
        url = self.default_image.get_src()
        return url


    def toShelfProdJson(self):
        prod_json = {}
        prod_json['platform_id'] = 1
        prod_json["number"] = self.get_spu_id()
        prod_json["shop_id"] = 1
        prod_json["vendor"] = ""
        prod_json["handle"] = self.get_handle()
        prod_json["tags"] = self.get_tags()
        prod_json["chineseTitle"] = self.get_spu_name()
        prod_json["englishTitle"] = self.get_spu_name()
        prod_json["defaultImageUrl"] = self.get_default_image_url()
        prod_json["html"] = ''
        prod_json["imageIndex"] = ""
        prod_json["source_type"] = ''
        prod_json["totalInventoryQuantity"] = ''
        prod_json["options"] = self.get_options()
        prod_json["description"] = self.description.toShelfProdJson()
        prod_json["variants"] = [unit.toShelfProdJson() for unit in self.sku_list]
        prod_json["spu"] = self.get_spu_code()
        prod_json["createdByName"] = "tangyipeng"
        prod_json["updatedByName"] = "tangyipeng"
        prod_json["createdAt"] =int(time.time() * 1000)
        prod_json["updatedAt"] = int(time.time() * 1000) 
        prod_json["createdBy"] = 0
        prod_json["updatedBy"] = 0
        prod_json["productType"] = self.get_product_type()
        prod_json["images"] = [unit.toShelfProdJson() for unit in self.images]
        prod_json["labels"] = self.get_labels() 
        prod_json['detail_url'] = self.get_detail_url()
        prod_json['standard_cat1'] = self.standard_cat1
        prod_json['standard_cat2'] = self.standard_cat2
        prod_json['standard_cat3'] = self.standard_cat3
        prod_json['standard_cat4'] = self.standard_cat4
        prod_json['standard_cat1_id'] = self.standard_cat1_id
        prod_json['standard_cat2_id'] = self.standard_cat2_id
        prod_json['standard_cat3_id'] = self.standard_cat3_id
        prod_json['standard_cat4_id'] = self.standard_cat4_id
        prod_json['cat1'] = self.cat1
        prod_json['cat2'] = self.cat2
        prod_json['cat3'] = self.cat3
        prod_json['cat4'] = self.cat4


        return prod_json

    def get_image_variantids(self, image_id):
        sku_id_list = []
        for sku in self.sku_list:
            if len(sku.images) == 0:
                continue
            print("image_id:%s, image_id:%s" % (sku.images[0].image_id, image_id))
            if sku.images[0].image_id == image_id:
                sku_id_list.append(sku.sku_id)
        return sku_id_list
    
    def get_detail_url(self):
        for sku in self.sku_list:
            detail_url = sku.detail_url
            if detail_url != '':
                return detail_url
        return ''


        


class EmmiolSKU(object):
    def __init__(self, spu):
        self.spu = spu
        self.sku_name = ""
        self.sku_code = ""
        self.sku_id = 0
        self.price = 0 
        self.first_image = ""
        self.detail_url = ""
        self.color = ""
        self.size = ""
        self.is_spider = False 
        self.images = []

    def get_detail_url(self):
        return self.detail_url

    def add_image(self, image):
        self.images.append(image)

    def add_images(self, images):
        self.images.extend(images)

    def set_images(self, images):
        self.images = images

    def set_detail_url(self, detail_url):
        self.detail_url = detail_url
        # self.sku_id = detail_url.split('product')[1].replace('.html', '') 

    def get_sku_id(self):
        return self.sku_id

    def get_sku_code(self):
        return self.sku_code

    def get_color(self):
        return self.color

    def get_sku_name(self):
        return self.sku_name

    def init_sku_name(self):
        if self.spu is not None and self.color != "" and self.size != '':
            self.sku_name = "%s %s / %s" % (self.spu.get_spu_name(), self.color, self.size)

    def set_color(self, color):
        self.color = color
        self.init_sku_name()

    def set_size(self, size):
        self.size = size
        self.init_sku_name()

    def set_sku_name(self, sku_name):
        self.sku_name = sku_name

    def toDumpJSON(self):
        dump_json = {}
        dump_json['sku_name'] = self.sku_name
        dump_json['price'] = self.price
        dump_json['sku_code'] = self.sku_code
        dump_json['sku_id'] = self.sku_id
        dump_json['detail_url'] = self.detail_url
        dump_json['color'] = self.color
        dump_json['size'] = self.size
        dump_json['images'] = [image.toDumpJSON() for image in self.images]
        return dump_json

    def fromJSON(self, data_json):
        self.item_name = data_json.get('sku_name')
        self.price = data_json.get('price')
        self.sku_code = data_json.get('sku_code')
        random.seed(self.sku_code)
        self.sku_id = round(random.random() * 100000)
        self.detail_url = data_json.get('detail_url')
        self.color = data_json.get('color')
        self.size = data_json.get('size')
        images = data_json.get('images')
        image_obj_list = []
        if images is not None:
            for image in images:
                image_id = image.get('image_id')
                image = self.spu.get_image(image_id)
                image_obj_list.append(image)

        self.images = image_obj_list
        return self

    def get_size(self):
        return self.size

    def get_price(self):
        if self.price == '' or self.price == 0:
            for sku in self.spu.sku_list:
                if sku.price != 0:
                    return sku.price
        return self.price

    def toShelfProdJson(self):
        prod_json = {}
        prod_json['sku'] = self.get_sku_code() 
        prod_json['variantId'] = self.get_sku_id()
        prod_json['status'] = 1
        prod_json['statusStr'] = "In Stock" 
        prod_json['requiresShipping'] = "true"
        prod_json['barCode'] = ""
        prod_json['defaultImageUrl'] = ""
        prod_json['imageIndex'] = 0 
        prod_json['inventoryPolicy'] = "continue"
        prod_json['inventoryManagement'] = "shopify"
        prod_json['name'] = self.get_sku_name()
        prod_json['sellPrice'] = self.get_price()
        prod_json['msrp'] = self.get_price()
        prod_json['purchasePrice'] = 0
        prod_json['inventoryQuantity'] = 1000
        prod_json['html'] = ''
        prod_json['weightUnit'] = "g"
        prod_json['weight'] = "10" 
        prod_json['grams'] = ""
        prod_json['option1'] = self.get_color()
        prod_json['option2'] = self.get_size()
        prod_json['option3'] = ""
        prod_json['src'] = '' if len(self.images) == 0 else self.images[0].get_src()
        print("dddddddddddsku image len:%s" % (len(self.images)))
        return prod_json
        

class EmmiolImage(object):
    def __init__(self, emmiol_prod):
        self.src = ""
        self.image_id = ""
        self.variantIds = []
        self.position = 0
        self.width = 800
        self.height = 800
        self.emmiol_prod = emmiol_prod
        self.aws_url = ""

    def init_aws_url(self, S3):
        if self.aws_url == "" or self.aws_url is None:
            image_name = utils.image_download(self.src)
            utils.async_image_cut(image_name)
            status, image_url = utils.upload_aws(S3, image_name)
            if status == True:
                self.aws_url = image_url

    def set_image_id(self, src):
        self.image_id = src.split("-")[-1].split(".")[0]

    def set_src(self, src):
        self.src = src
    
    def get_src(self):
        if self.aws_url != '' and self.aws_url is not None:
            return self.aws_url
        return self.src

    def get_image_id(self):
        return self.image_id

    def get_position(self):
        return self.position

    def toShelfProdJson(self):
        prod_json = {}
        prod_json['code'] = ""
        prod_json['image_id'] = self.get_image_id()
        prod_json['src'] = self.get_src()
        prod_json['variantIds'] = self.emmiol_prod.get_image_variantids(self.image_id)
        prod_json['width'] = self.width
        prod_json['height'] = self.height
        prod_json['position'] = self.position
        return prod_json

    def toDumpJSON(self):
        prod_json = {}
        prod_json['image_id'] = self.get_image_id()
        prod_json['src'] = self.get_src()
        prod_json['position'] = self.get_position()
        return prod_json

    def fromJSON(self, data_json):
        self.image_id = data_json.get('image_id')
        self.src = data_json.get('src')
        self.aws_url = data_json.get('aws_url')
        self.position = data_json.get('position')
        return self


class EmmiolDesc(object):
    def __init__(self):
        self.detail = ""
        self.return_and_shipping = ""
        self.size = []

    def get_size(self):
        return self.size

    def get_return_and_shipping(self):
        return self.return_and_shipping

    def get_detail(self):
        return self.detail

    def get_preSale(self):
        return ''

    def get_description(self):
        return ''

    def toShelfProdJson(self):
        prod_json = {}
        cm_size, inch_size = self.get_shopify_size()
        prod_json['cm_size'] = cm_size 
        prod_json['inch_size'] = inch_size
        prod_json['shippingAndReturns'] = self.get_return_and_shipping()
        prod_json['description'] = self.get_description()
        cur_detail = {} 
        for unit in self.detail:
            cur_detail.update(unit)
        prod_json['detail'] = cur_detail
        prod_json['preSale'] = self.get_preSale()
        return prod_json

    def get_shopify_size(self):
        menu = self.size.get('menu')
        print(self.size)
        if len(self.size.get("size_list")) == 0:
            return [], [] 
        size_list = self.size.get("size_list")[0]
        result = {}
        cm_size = [] 
        inch_size = []
        for key, value in size_list.items():
            total_records = []
            if type(value) == dict:
                value = value.values()
            for unit in value:
                index = 0
                record = {}
                if len(menu) != len(unit):
                    return [], []
                for size_value in unit:
                    head_key = menu[index]
                    has_char = False
                    for char_value in size_value:
                        if char_value.isdigit() == False  and char_value != '.' and char_value != '-':
                            has_char = True

                    if key.lower() == 'cm':
                        if head_key.lower() != 'size' and has_char == False:
                            if size_value.find('-') != -1:
                                front_value = size_value.split('-')[0]
                                end_value = size_value.split('-')[1]
                                front_value = utils.cm_float_format(front_value)
                                end_value = utils.cm_float_format(end_value)
                                size_value = "%s-%s" % (str(front_value), str(end_value))
                            else:
                                size_value = utils.cm_float_format(size_value)
                    else:
                        if head_key.lower() != 'size' and has_char == False:
                            if size_value.find('-') != -1:
                                front_value = size_value.split('-')[0]
                                end_value = size_value.split('-')[1]
                                front_value = utils.inch_float_int_format(front_value)
                                end_value = utils.inch_float_int_format(end_value)
                                size_value = "%s-%s" % (str(front_value), str(end_value))
                            else:
                                size_value = utils.inch_float_int_format(size_value)
                    record[head_key] = size_value
                    index = index + 1
                total_records.append(record)
            if key.lower() == 'cm':
                cm_size = total_records
            else:
                inch_size = total_records
        return cm_size, inch_size

    def toDumpJSON(self):
        dump_json = {}
        dump_json['detail'] = self.get_detail()
        dump_json['return_and_shipping'] = self.get_return_and_shipping()
        dump_json['size'] = self.get_size()
        return dump_json

    def fromJSON(self, data_json):
        self.detail = data_json.get('detail')
        self.return_and_shipping = [data_json.get('return_and_shipping'), 'Free returns for the U.S only']
        self.size = data_json.get('size')
        return self


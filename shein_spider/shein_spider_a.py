# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
import json
import re

import time
from common import myRequest
from lxml import etree
import datetime
from sqlalchemy import create_engine
import requests
import traceback

class SheinSpider(object):
    def __init__(self, url_type):
        self.url_type = url_type
        self.date = datetime.date.today().strftime("%Y%m%d")
        self.req = myRequest.MyRequest()

    def init_conf(self):
        self.base_url = "https://us.shein.com/"
        self.url_info_conf = {
                "new": {"url_part": '/daily-new.html', "tag":""},
                "dress": {"url_part": "/women-dresses-c-1727.html", "tag": 'dress', "start_page": 1},
                "maxi_dress": {"url_part": "/style/Maxi-Dresses-sc-00100346.html", "tag": "maxi_dress", "start_page": 1},
                "midi_dress": {"url_part": "/style/Midi-Dresses-sc-00100629.html", "tag": "midi_dress", "start_page": 1},
                "short_dress": {"url_part": "/style/Short-Dresses-sc-00100608.html", "tag": "short_dress", "start_page": 1},
                "tees" : {"url_part": "/T-Shirts-c-1738.html", "tag": "tees"},
                "blouses" : {"url_part": "/Blouses-c-1733.html", "tag": "blouses"},
                "tank_top_camis" : {"url_part": "/Tank-Tops-Camis-c-1779.html", "tag": "tank_top_camis"},
                "sweatshirts" : {"url_part": "/Sweatshirts-c-1773.html", "tag": "sweatshirts"},
                "sweaters_cardigans" : {"url_part": "/Sweaters%20&%20Cardigans-c-2216.html", "tag": "sweaters_cardigans"},
                "coats_jackets" : {"url_part": "/Coats-Jackets-c-2037.html", "tag": "coats_jackets"},
                "blazers" : {"url_part": "/Suits-c-1739.html", "tag": "blazers"},
                "denim_jackets_coat" : {"url_part": "/Denim-Jackets-Coats-c-1933.html", "tag": "denim_jackets_coat"},
                "short_dress" : {"url_part": "/style/Short-Dresses-sc-00100608.html", "tag": "short_dress"},
                "jumpsuits_romper" : {"url_part": "/Jumpsuits-c-1860.html", "tag": "jumpsuits_romper"},
                "two_piece" : {"url_part": "/Two-piece-Outfits-c-1780.html", "tag": "two_piece"},
                "pants" : {"url_part": "/Pants-c-1740.html", "tag": "pants"},
                "leggings" : {"url_part": "/Leggings-c-1871.html", "tag": "leggings"},
                "skirts" : {"url_part": "/Skirts-c-1732.html", "tag": "skirts"},
                "biker_shorts" : {"url_part": "/category/Biker-Shorts-sc-00824061.html", "tag": "biker_shorts"},
                "shorts" : {"url_part": "/Shorts-c-1912.html", "tag": "shorts"},
                "jeans" : {"url_part": "/Jeans-c-1934.html", "tag": "jeans"},
                "denim_shorts" : {"url_part": "/Denim-Shorts-c-1935.html", "tag": "denim_shorts"},
                "sweatpants" : {"url_part": "/Sweatpants-c-2990.html", "tag": "sweatpants"},
                "swimwear" : {"url_part": "/Beachwear-c-2039.html", "tag": "swimwear"},
                "tops": {"url_part": "/Tops-c-1766.html", "tag": "tops"},
                "bottoms": {"url_part": "/Bottoms-c-1767.html", "tag": "bottoms"},
                "knittops": {"url_part": "/Knit-Tops-c-2217.html", "tag": "knittops"},
                "acc": {"url_part": "/Accessories-c-1765.html", "tag": "acc", "page_count": 10, "start_page": 1}
                }

    def init_db(self):
        self.engine = create_engine("mysql+pymysql://admin:JYFscvVevsANJYmrz9Ud@infwaves.c0rck9lj4qyp.us-east-2.rds.amazonaws.com:3306/erp_product_analysis", max_overflow=5)
        self.db_conn = self.engine.connect()

    def extract_item_list(self, url_info, page, sort_type):
        url_part = url_info.get('url_part')
        tag = url_info.get("tag")
        if sort_type == 0:
            url = "%s%s?page=%s" % (self.base_url, url_part, page)
        else:
            url = "%s%s?page=%s&sort=%s" % (self.base_url, url_part, page, str(sort_type))
        print("start spider:%s ...." % url)
        while True:
            try:
                resp = self.req.sync_get(url)
                # resp = self.req.sync_get(url)
                if resp is None:
                    continue
                else:
                    break
            except Exception as e:
                print(e)
                continue
        if resp is None:
            return 

        print("spider finish %s, resp:%s" % (url, resp.text))
        text_split = re.split("<script>|</script>", resp.text)
        data_json_str = "{}"
        for unit in text_split:
            if unit.find('gbProductListSsrData') != -1:
                data_json_str = "=".join(unit.split('gbProductListSsrData')[-1].split("=")[1:])
        data_json = json.loads(data_json_str)
        if "results" not in data_json or "goods" not in data_json.get('results'):
            return []

        resultData = data_json.get('results')
        goodsData = resultData.get('goods')
        print("goods_data:%s" % goodsData)
        if len(goodsData) == 0:
            return 

        good_id_list = []
        for good_unit in goodsData:
            good_id = good_unit.get('goods_id')
            good_id_list.append(good_id)
        price_url = "%s/%s" % (self.base_url, "/product/getPricesByIds")
        data = {"goodsIds": good_id_list}
        print(data)
        headers = {"Content-Type": "application/json", "Content-Length": str(len(str(data))), "Host": "us.shein.com"}
        print("spider start %s" % price_url)
        resp = self.req.sync_post(price_url, headers=headers, json=data)
        print("spider finish %s" % price_url)
        text_json = json.loads(resp.text)
        print(text_json)

        price_info = text_json.get('info').get('prices')
        print(price_info)
        value_str_list = []
        tag_value_str_list = []
        for good_unit in goodsData:
            good_id = good_unit.get('goods_id')
            price = price_info.get(str(good_id))
            good_unit['price'] = price
            good_sn = good_unit.get('goods_sn')
            pretreatInfo = good_unit.get('pretreatInfo')
            if pretreatInfo is not None:
                detail_url = pretreatInfo.get('goodsDetailUrl')
            else:
                detail_url = ''
            good_unit_str = json.dumps(good_unit).replace("'", "").replace('%', '%%')
            value_str = "('%s', %s, '%s', '%s', '%s', 0)" % ('shein', self.date, good_sn, detail_url, good_unit_str)
            tag_value_str = "('%s', '%s')" % (good_sn, tag)
            tag_value_str_list.append(tag_value_str)
            value_str_list.append(value_str)


        try:
            insert_sql = "replace into erp_product_analysis.spider_prod_shein (source_type, date, spu_code, detail_url, detail_info, prod_create_date) values %s" % (",".join(value_str_list))
            print(insert_sql)
            self.db_conn.execute(insert_sql)

            insert_tag_sql = "replace into erp_product_analysis.spider_prod_tags_shein (spu_code, tag_name) values %s" % (",".join(tag_value_str_list))
            print(insert_tag_sql)
            self.db_conn.execute(insert_tag_sql)
        except Exception as e:
            traceback.print_exc()


    def run(self):
        self.init_conf()
        self.init_db()
        url_info = self.url_info_conf.get(self.url_type)
        print(url_info)
        if url_info is not None:
            page_count = 50 if url_info.get('page_count') is None else url_info.get("page_count")
            start_page = 1 if url_info.get("start_page") is None else url_info.get("start_page")
            print(page_count)
            print(start_page)
            for index in range(start_page, page_count):
                sort_type_list = [9]
                for sort_type in sort_type_list:
                    self.extract_item_list(url_info, index, sort_type)
                    time.sleep(2)


if __name__ == '__main__':
    url_type = sys.argv[1]
    SheinSpider(url_type).run()








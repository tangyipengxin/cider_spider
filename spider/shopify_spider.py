# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
from entity.ProductEntity import Image, Product, Variant
from common import utils
from common import myRequest
import codecs
import json
import time
import datetime
import requests


class ShopifySpider(object):
    def __init__(self, shopify_site, debug):
        self.shopify_site = shopify_site
        self.req = myRequest.MyRequest()
        self.product_list = {}
        self.debug = debug
        self.date = datetime.date.today().strftime("%Y%m%d")

    def init_conf(self):
        with open("../conf/%s.json" % (self.shopify_site), "r") as conf_file:
            data = conf_file.read()
        conf_json = json.loads(data)

        self.url = conf_json.get('url')
        self.label_info = conf_json.get('label_info')
        self.best_seller_conf = conf_json.get('best_seller')
        self.exclude_label_list = [] if conf_json.get("exclude_label") is None else conf_json.get("exclude_label")

    def spider_prod_list(self):
        index = 1 
        while True:
            url = "%s/products.json?page=%s" % (self.url, index)
            print("spider start...url:%s" % url)
            resp = self.req.sync_get(url)
            print("spider end...url:%s" % url)
            if resp is None or resp.status_code != 200:
                return False, {}

            json_data = resp.json()
            products = json_data.get('products')
            if not products or len(products) == 0:
                break

            if self.debug == True and index >= 2:
                break

            for unit in products:
                product_unit = Product()
                product_unit.site = self.shopify_site
                product_unit = product_unit.fromSpiderJSON(unit)
                self.product_list[product_unit.product_id] = product_unit
            index = index + 1
        return True, self.product_list

    def spider_lable_info(self):
        debug_index = 1
        for label_unit in self.label_info:
            url_part = label_unit.get('url_part')
            label = label_unit.get('label')
            cat_info = label_unit.get('cat_info')
            cat1 = cat_info.get('cat1')
            cat2 = cat_info.get('cat2')
            cat3 = cat_info.get('cat3')
            cat4 = cat_info.get('cat4')
            standard_cat1 = cat_info.get('standard_cat1')
            standard_cat2 = cat_info.get('standard_cat2')
            standard_cat3 = cat_info.get('standard_cat3')
            standard_cat4 = cat_info.get('standard_cat4')
            standard_cat1_id = cat_info.get('standard_cat1_id')
            standard_cat2_id = cat_info.get('standard_cat2_id')
            standard_cat3_id = cat_info.get('standard_cat3_id')
            standard_cat4_id = cat_info.get('standard_cat4_id')
            if debug_index == 2 and self.debug == True:
                return 
            debug_index = debug_index + 1

            index = 1
            while True:
                ab_url = "%scollections/%s/products.json?page=%s" % (self.url, url_part, index)
                print("spider start...url:%s" % (ab_url))
                start = time.time()
                resp = self.req.sync_get(ab_url)
                end = time.time()
                print("spider end...url:%s, cost_time:%s" % (ab_url, end - start))
                if resp is None:
                    print("resp None page:%s get fail" % (index))
                    continue
                status_code = resp.status_code
                if status_code != 200:
                    print("page:%s get fail resp:%s, %s" % (index, resp.status_code, resp.text))
                    continue

                json_data = resp.json()
                products = json_data.get('products')
                if not products or len(products) == 0:
                    break

                if self.debug == True and index >= 2:
                    break

                for unit in products:
                    product_id = unit.get('id')
                    title = unit.get('title')
                    if product_id not in self.product_list:
                        print(title)
                        print("warning: product_id %s, title:%s, not in all prod list" % (product_id, title))
                        continue

                    product_info = self.product_list.get(product_id)
                    product_info.add_label(label)
                    product_info.update_cat_label(cat1, cat2, cat3, cat4)
                    print("standard_cat:%s, %s, %s, %s, %s, %s, %s, %s" % (standard_cat1, standard_cat2, standard_cat3, standard_cat4, standard_cat1_id, standard_cat2_id, standard_cat3_id, standard_cat3_id))
                    product_info.update_standard_cat_label(standard_cat1, standard_cat2, standard_cat3, standard_cat4, standard_cat1_id, standard_cat2_id, standard_cat3_id, standard_cat4_id)
                index = index + 1
        for prod_unit in self.product_list.values():
            prod_unit.init_default_cat_info()
        return True

    def prodJsonDump(self, json_file_path):
        prods_json = []
        for product in self.product_list.values():
            prods_json.append(product.toDumpJSON())
        #print(prods_json)
        prods_json_str = json.dumps(prods_json)
        json_file = open(json_file_path, "w")
        json_file.write(prods_json_str)
        json_file.close()

    def spider_best_seller(self):
        url_part = self.best_seller_conf.get('url_part')
        index = 1 
        unit_index = 0
        while True:
            url = "%scollections/%s/products.json?page=%s&sort_by=best-selling" % (self.url, url_part, index)
            print("spider start...url:%s" % (url))
            start = time.time()
            resp = self.req.sync_get(url)
            end = time.time()
            print("spider end...url:%s, cost_time:%s" % (url, end - start))
            status_code = resp.status_code
            if status_code != 200:
                print("url:%s, index:%s, get fail, retry again" % (url, index))
                continue

            json_data = resp.json()
            products = json_data.get('products')
            if not products or len(products) == 0 or (self.debug == True and index >= 1):
                break
            for unit in products:
                product_id = unit.get('id')
                title = unit.get('title')
                if product_id not in self.product_list:
                    print("warning: product_id %s, title:%s, not in all prod list" % (product_id, title))
                    continue

                product_info = self.product_list.get(product_id)
                product_info.best_seller_index = unit_index
                unit_index = unit_index + 1
            index = index + 1

    def exclude_prod(self):
        new_product_list = {}
        if len(self.exclude_label_list) == 0:
            return self.product_list
        for product_id, prod_unit in self.product_list.items():
            for label_unit in self.exclude_label_list:
                print("product_id:%s" % (product_id))
                if prod_unit.contain_label(label_unit) == True:
                    continue
                else:
                    new_product_list[product_id] = prod_unit
        self.product_list = new_product_list

    def run(self):
        self.init_conf()
        self.spider_prod_list()
        self.spider_lable_info()
        self.spider_best_seller()
        #self.exclude_prod()
        self.prodJsonDump("../data/%s_%s.json" % (self.shopify_site, self.date))

if __name__ == '__main__':
    site = sys.argv[1]
    if len(sys.argv) == 3:
        debug = True if sys.argv[2] == 'true' else False
    else:
        debug = False
    try:
        ShopifySpider(site, debug).run()
    except Exception as e:
        print(e)
        utils.send_alarm("site:%s, date:%s, error:%s" % (site, datetime.date.today().strftime("%Y%m%d"), e) )

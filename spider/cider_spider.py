# encoding=utf-8

headers = {
    "authority": None,
    "method": "GET",
    "path": None,
    "scheme": "https",
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "accept-encoding": "gzip, deflate, br",
    "accept-language": "zh-CN,zh;q=0.9,en;q=0.8",
    "cache-control": "no-cache",
    "cookie": "secure_customer_sig=; _shopify_country=China; cart_currency=USD; _y=93cad61b-2772-47b6-81f0-6e116995cd33; _s=77e5a78e-a390-4e57-b58d-07d7556a4646; _shopify_y=93cad61b-2772-47b6-81f0-6e116995cd33; _shopify_s=77e5a78e-a390-4e57-b58d-07d7556a4646; _shopify_fs=2021-03-29T05%3A43%3A05Z; _tracking_consent=%7B%22v%22%3A%222.0%22%2C%22lim%22%3A%5B%22CCPA_BLOCK_ALL%22%2C%22GDPR%22%5D%2C%22reg%22%3A%22%22%2C%22con%22%3A%7B%22GDPR%22%3A%22%22%7D%7D; _shopify_tm=; _shopify_tw=; _shopify_m=persistent; cart_sig=304498b87fce1cdaa06dbfa95dc4232f",
    "pragma": "no-cache",
    "sec-fetch-dest": "document",
    "sec-fetch-mode": "navigate",
    "sec-fetch-site": "none",
    "sec-fetch-user": "?1",
    "upgrade-insecure-requests": "1",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36"
}

index_url = 'https://shopcider.com'

# index_url = 'https://thehalara.com'

json_url = '/products.json'
goods_url = '/products/'

headers['authority'] = index_url.split('//')[1]

page_str = '?page={page}'

from myRequest import MyRequest
import json
# from log.log import logger

# json.loads() # str to dict
# json.dumps()  # dict to str



fields = ['sku', '商品名', '商品类目', '折扣价', '原价', '尺寸', '详情页地址','spu号','标签']
"""
sku: sku
商品名: goods_name
价格: price
尺寸: size
详情页地址: goods_detail_url
"""


records = {}
start_page = 1
page = start_page
while True:
    req_url = index_url + json_url + page_str.format(page=str(page))
    headers['path'] = req_url
    resp = MyRequest.sync_get(req_url)
    try:
        json_data = resp.json()
    except BaseException as e:
        print('出错网站{}, 出错原因{}'.format(req_url, e))
        page += 1
        continue
    page += 1

    products = json_data['products']
    if not products:
        break

    for p in products:

        if p['body_html']:
            try:
                body_html = p['body_html'].split('id="description_data">')[1]
                body_html = body_html.split('</div>')[0]
            except IndexError as e:
                #logger.debug(p)
                continue

            body_html_dict = json.loads(body_html)
        
        if p.get('variants'):
            sku = p['id']
            spu_code = ''
            price = p['variants'][0]['price']
            off_price = p['variants'][0]['compare_at_price']
            if not off_price:
                off_price = price
            for sku_cd in p['variants']:
                spu_temp = sku_cd['sku'].split('-')[0]
                spu_code = spu_temp
                # spu_code += ','

        if p.get('tags'):
            tags = ''
            for p2 in p['tags']:
                tags += p2
                tags += ','
        # if body_html_dict:

        goods_name = p['title'].replace(',', '-')

        for opt in p['options']:
            if opt['name'] in 'Size':
                size = opt['values']
                size = ';'.join(size)

        goods_detail_url = index_url + goods_url + p['handle']
        created_at = p['created_at']
        created_date = created_at[0:10]
        record = {'sku': sku, "goods_name": goods_name, "price": price, "off_price": off_price, "size": size, "goods_detail_url": goods_detail_url, "spu_code": spu_code, "tags": tags, 'created_at':created_at, 'created_date': created_date} 
        records[spu_code] = record


    print('[{}]'.format(req_url))




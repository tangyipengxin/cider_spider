# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
import json
from entity.ProductEntity import Product



if __name__ == '__main__':
    finer_path = '../data/getfiner_20210422.json'
    cider_path = '../data/cider_20210421.json'
    infringe_path = '../conf/cider_infringe_blacklist.conf'
    
    finer_file = open(finer_path, 'r')
    finer_data = finer_file.read()
    finer_file.close()

    cider_file = open(cider_path, 'r')
    cider_data = cider_file.read()
    cider_file.close()

    infringe_file = open(infringe_path, 'r')
    infringe_data = infringe_file.readlines()
    infringe_file.close()
    infringe_list = [unit.strip() for unit in infringe_data]

    finer_json = json.loads(finer_data)
    finer_prods = {}
    for finer_unit in finer_json:
        finer_prod = Product().fromLoadJSON(finer_unit)
        if finer_prod.handle.split('-')[-1].isdigit() == True:
            finer_prods["-".join(finer_prod.handle.split('-')[0:-1])] = finer_prod
        else:
            finer_prods[finer_prod.handle] = finer_prod

        

    cider_json = json.loads(cider_data)
    cider_prods = {}
    for cider_unit in cider_json:
        cider_prod = Product().fromLoadJSON(cider_unit)
        if cider_prod.product_type.lower() != 'accessories' and cider_prod.isValid() == True and cider_prod.handle not in infringe_list:
            if cider_prod.handle.split('-')[-1].isdigit() == True:
                cider_prods["-".join(cider_prod.handle.split('-')[0:-1])] = cider_prod
            else:
                cider_prods[cider_prod.handle] = cider_prod

    print(len(finer_prods.keys()))
    print(len(cider_prods.keys()))
    for cider_handle, cider_prod in cider_prods.items():
        if cider_handle not in finer_prods:
            print(cider_handle)
            #title = new_cider_prod.title
            #handle = new_cider_prod.handle
            #product_id = new_cider_prod.product_id
            #srcs = []
            #images = new_cider_prod.images
            #for image in images:
            #    srcs.append(image.src)
            #    out_str = "%s\t%s\t%s\t%s" % (product_id, title, handle, image.src)
            #    outs_str.append(out_str)

    #print("\r\n".join(outs_str).encode(encoding='utf-8'))



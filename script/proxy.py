# encoding=utf-8
import time
import random

import requests
# from log import logger


class LocalProxy:
    local_proxies = {
        'http': '127.0.0.1:1080',
        'https': '127.0.0.1:1080'
    }


class QingTingProxy(LocalProxy):
    # 代理服务器
    proxyHost = "dyn.horocn.com"
    proxyPort = "50000"

    # 代理隧道验证信息
    proxyUser = "XFH01667821190036729"
    proxyPass = "tzrxEUv760qf"

    proxyMeta = "http://%(user)s:%(pass)s@%(host)s:%(port)s" % {
        "host": proxyHost,
        "port": proxyPort,
        "user": proxyUser,
        "pass": proxyPass,
    }

    proxies = {
        "http": proxyMeta,
        "https": proxyMeta
    }

    def get_proxy(self):
        return self.proxies




class XiaoXiangProxy(LocalProxy):
    IP_url = 'https://api.xiaoxiangdaili.com/ip/get'
    release_url = 'https://api.xiaoxiangdaili.com/ip/release'

    appKey = '691488115193696256'
    appSecret = 'li1PVqt2'
    PROXY_LIFE = 60


    def __init__(self):
        self.start_time = time.time()
        # self.proxies = self._get_proxy()
        self.proxies = self.local_proxies


    def get_proxy(self):
        """
        检测ip是否使用超时
        :return:
        """
        return self.proxies
        # now_time = time.time()
        # if now_time - self.start_time > self.PROXY_LIFE:
        #     # self.release_proxy(self.proxies['https'])
        #     self.start_time = now_time
        #     self.proxies = self._get_proxy()
        # return self.proxies
    #
    # def release_proxy(self, proxy: str):
    #     data = {
    #         'appKey': self.appKey,
    #         'appSecret': self.appSecret,
    #         'proxy': proxy.split('//')[1].split(':')[0]
    #     }
    #     resp = requests.post(self.release_url, params=data)
    #     print(resp.text)

    def _get_proxy(self, cnt=1, wt='json', method='https', releaseAuto=False):
        """
        获取代理ip
        :param cnt:提取数量，默认值为应用的最大可提取数
        :param wt:响应体格式，"json"或者"text"，默认值"json"
        :param method:代理方式，"http" 或 "s5"（s5 即 socks5）,默认值"http"
        :param releaseAuto:(可选，仅静态IP应用下有效)：是否自动释放旧的IP
        :return: proxies
        """
        while True:
            resp = requests.get(self.IP_url, params={'appKey': self.appKey,
                                                     'appSecret': self.appSecret,
                                                     'cnt': cnt,
                                                     'wt': wt,
                                                     'method': method,
                                                     'releaseAuto': releaseAuto
                                                     }, timeout=3)
            data = resp.json()
            hosts = data['data'][0]['ip']
            ports = data['data'][0]['port']
            # hosts = random.choice(['114.104.139.59'])
            # ports = random.choice(['5021'])

            httpMeta = "http://%(host)s:%(port)s" % {
                "host": hosts,
                "port": ports,
            }

            httpsMeta = "http://%(host)s:%(port)s" % {
                "host": hosts,
                "port": ports,
            }

            self.proxies = {
                "http": httpMeta,
                "https": httpsMeta
            }
            if self.test_proxy(self.proxies):
                break
            time.sleep(10)

        #logger.info('重新获取代理ip[{}]'.format(self.proxies['https']))
        return self.proxies


    def test_proxy(self, proxy):
        test_url = "http://icanhazip.com/"
        try:
            res = requests.get(url=test_url, timeout=5, proxies=proxy)
            proxyIP = res.text
            if proxyIP.replace('\n', '') in proxy['http']:
                return True
            else:
                return False
        except BaseException as e:
            return False


proxy = QingTingProxy()
proxies = proxy.get_proxy

# 测试ip地址
# test_ip_url = 'http://pv.sohu.com/cityjson'
# resp = requests.get(test_ip_url, proxies=proxies(), timeout=3)
# print(resp.text)

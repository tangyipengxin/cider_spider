# encoding=utf-8
import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
from entity import ProductEntity as PE
from entity import ShelfProdEntity as SP
import json

class ProdExport(object):
    def __init__(self, title):
        self.title = title

    def run(self):
        path = '../data/getfiner_20210519.json'
        data_file = open(path, 'r')
        json_data = data_file.read()
        data_file.close()

        json_data = json.loads(json_data)
        print(len(json_data))
        export_str_list = []
        for product_json in json_data:
            product = PE.Product().fromLoadJSON(product_json)
            prodSPU = SP.ProdSPU().fromSpiderData(product)


            export_str_list.append(json.dumps({"id": product.product_id, "shopId": 1, "htmlBody": prodSPU.description.toJSON()}))
            
        print("\r\n".join(export_str_list).encode(encoding='utf-8'))
            #if product.title.find(self.title) != -1:
            #    prodSPU = SP.ProdSPU().fromSpiderData(product)
            #    print(prodSPU.toJson())
                
                

        


if __name__ == '__main__':
    title = sys.argv[1]
    ProdExport(title).run()

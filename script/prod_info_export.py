# encoding=utf-8
import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
from entity import ProductEntity as PE
from entity import ShelfProdEntity as SP
import json
from common import utils

class ProdExport(object):
    def __init__(self, title):
        self.title = title

    def run(self):
        date_index = 20210413
        prod_dict = {}
        while date_index <= 20210512:
            path = '../data/cider_%d.json' % (date_index)
            data_file = open(path, 'r')
            json_data = data_file.read()
            data_file.close()

            json_data = json.loads(json_data)
            export_str_list = []
            for product_json in json_data:
                product = PE.Product().fromLoadJSON(product_json)
                if product.product_type.lower() != 'accessories' and product.isValid() == True:
                    product_id = product.product_id
                    prod_dict[product_id] = {"prod": product, "date": date_index}


            #if product_json.get('title') == 'Secret Graphic Mini Skirt':
            #    export_str_list.append(json.dumps(product_json))
            #    export_str_list.append(json.dumps(product.toDumpJSON()))
            #    export_str_list.append("image len:%s" % (len(product.images)))
                #if "THE CIDER BASICS" in product.labels:
            #    desc = product.description
            #    es_time = json.dumps(desc.preSale.get('estimateDeliveryTime'))
            if date_index == 20210430:
                date_index = 20210501
            else:
                date_index = date_index + 1
        for key, value in prod_dict.items():
            product = value.get('prod')
            date = value.get('date')
            export_str_list.append("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % (product.product_id, product.title, utils.DateFormat(product.created_at), product.cat1, product.cat2, product.variants[0].price, "https://shopcider.com/products/%s" % (product.handle), product.images[0].src, date if date != 20210512 else '在架'))
        #print(export_str_list) 
        #print("\r\n".join(export_str_list).encode(encoding='utf-8'))
        print("\r\n".join(export_str_list))
            #if product.title.find(self.title) != -1:
            #    prodSPU = SP.ProdSPU().fromSpiderData(product)
            #    print(prodSPU.toJson())
                
                

        


if __name__ == '__main__':
    title = sys.argv[1]
    ProdExport(title).run()

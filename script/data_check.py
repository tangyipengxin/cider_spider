import json



def check_size_empty(size):
    cm = size.get('cm')
    inch = size.get('inch')
    if len(cm) == 0 or len(cm[0]) == 0:
        return True

    return False


if __name__ == '__main__':
    with open("../data/unicornbae_20210605.json", "r") as f:
       data = f.read()
       data_json = json.loads(data)

    empty_count = 0
    empty_handle_list = []
    total_count = 0
    size_error_count = 0
    size_error_handl_list = []
    max_create_time = 0
    for unit in data_json:
        handle = unit.get('handle')
        description = unit.get('description')
        create_at = unit.get('created_at')
        if create_at > max_create_time:
            max_create_time = create_at
        size = description.get('size')
        if check_size_empty(size) == True:
            empty_count = empty_count + 1
            empty_handle_list.append(handle)
        else:
            cm_size = size.get('cm')
        
            contain_size = False
            for cm_unit in cm_size:
                keys = cm_unit.keys()
                for key in keys:
                    if key.lower().find('size') != 0:
                        contain_size = True

            if contain_size == False:
                size_error_count = size_error_count + 1
                size_error_handl_list.append(handle)

            
        total_count = total_count + 1


    print("empty_count:%s" % empty_count)
    print("total_count:%s" % total_count)
    print("max_create_at:%s" % max_create_time)
    print("empty_handle:%s" % (empty_handle_list))
    print("size_error_count:%s" % (size_error_count))
    print("size_error_handl_list:%s" % (size_error_handl_list))
        
        # value = "%s\t%s" % (handle, size)
        # print(value)



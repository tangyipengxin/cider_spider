# encoding=utf-8
import json
import pandas as pd


def get_price_level(price, interval = 10):
    
    if price == -1:
        return -1
    price = round(float(price), 0)
    return "%s~%s" % (interval * (int(price) // interval), interval * ((int(price) // interval) + 1))



data_file = open('data/emmiol_spider_20210517_t.json', "r")
lines = data_file.readlines()
data_file.close()

prod_list = []
for line in lines:
    unit = json.loads(line)
    prod_list.append(unit)
str_list = []
item_id_set = set()
new_prod_list = []
cat_set = set()
for unit in prod_list:
    item_id = unit.get('item_id')
    if item_id not in item_id_set:
        item_id_set.add(item_id)
    else:
        print("item_id:%s, dumplicate" % (item_id))
        continue
    new_prod_list.append(unit)
    cat_list = unit.get('item_list_name').split('>')
    cat1 = cat_list[0].strip() if len(cat_list) >= 1 else ''
    cat2 = cat_list[1].strip() if len(cat_list) >= 2 else ''
    cat3 = cat_list[2].strip() if len(cat_list) >= 3 else ''
    cat4 = cat_list[3].strip() if len(cat_list) >= 4 else ''
    unit['cat1'] = cat1
    unit['cat2'] = cat2
    unit['cat3'] = cat3
    unit['cat4'] = cat4
    cat_set.add("%s\t%s\t%s\t%s" % (cat1, cat2, cat3, cat4))
    unit['item_id_int'] = unit.get('detail_url').split('product')[1].replace('.html', '')
    
    export_str = "%s,%s,%s,%s,%s,%s,%s,%s,%s" % (unit.get('item_name'), unit.get('item_id'), unit.get('price'), unit.get('detail_url'), unit.get('first_image'), cat1, cat2, cat3, cat4)
    str_list.append(export_str)
export_file = open('emmiol_export.csv', 'w')
export_file.write("\r\n".join(str_list))
export_file.close()
print("\r\n".join(cat_set))

prod_list = new_prod_list


rows = []
for unit in prod_list:
    row = []
    row.append(unit.get('item_id'))
    row.append(unit.get('cat2'))
    row.append(unit.get('price'))
    row.append(get_price_level(unit.get('price'), 10))
    rows.append(row)
header = ['product_id', 'cat1', 'price', 'price_level']
df = pd.DataFrame(rows, columns=header)
cat_price_grouped = pd.DataFrame(df.groupby(['cat1', 'price_level']).agg({'product_id':'count'}))
cat_group = pd.DataFrame(df.groupby(['cat1']).agg({'product_id':'count'}))
price_group = pd.DataFrame(df.groupby(['price_level']).agg({'product_id':'count'}))
all_group = pd.DataFrame(df.agg({'product_id': 'count'}))
total_mechant = all_group.at['product_id', 0]
cat_percent_group = pd.DataFrame(cat_price_grouped.div(cat_group, level='cat1'))
all_percent_group = pd.DataFrame(price_group.div(total_mechant))
price_group.columns = ['商品数']

all_result_group = all_percent_group.join(price_group, how='outer', on=['price_level'])
all_result_group['商品总数'] = total_mechant
all_result_group['cat1'] = '总计'
all_result_group = all_result_group.join(all_result_group.index.to_frame(), how='outer', on=['price_level'])
all_result_group = all_result_group.reset_index(drop=True)


merge_1 = pd.DataFrame(pd.merge(cat_percent_group, cat_price_grouped, how='outer', on=['cat1', 'price_level']))
merge_2 = merge_1.join(cat_group, how='outer', on=['cat1'])
merge_index = merge_2.index.to_frame()
merge_3 = merge_2.join(merge_index, how='outer', on=['cat1', 'price_level'])
merge_3 = merge_3.reset_index(drop=True)
all_result_group.columns = ['占比', '商品数', '分类商品总数量', '分类名', '价格区间']
merge_3.columns = ['占比', '商品数', '分类商品总数量', '分类名', '价格区间']
all_result_group = all_result_group.append(merge_3)
all_result_group['占比'] = all_result_group['占比'].apply(lambda x: format(x, '.2%'))
all_result_group = all_result_group[['分类名', '价格区间', '占比', '商品数', '分类商品总数量']]
#print(all_result_group.to_csv(sep='\t', index=None))




good_field_list = ['item_id_int', 'item_id', 'item_name', 'item_name', 'item_id', 'detail_url', 'price', 'first_image', 'cat1', 'cat2', 'cat3', 'cat4']
new_in_field_list = ['item_id_int', '2021-05-11', '2021-05-11 00:00:00', 'emmiol']
image_field_list = ['item_id_int', 'item_id', 'first_image']
good_value_list = []
new_in_value_list = []
image_value_list = []
print(len(prod_list))
for unit in prod_list:
    good_field_value = []
    new_in_field_value = []
    image_field_value = []
    for field in good_field_list:
        if field == 'item_id_int':
            good_field_value.append(unit.get(field))
        else:
            good_field_value.append('"%s"' % unit.get(field))
    for field in new_in_field_list:
        if field == 'item_id_int':
            new_in_field_value.append(unit.get(field))
        else:
            new_in_field_value.append('"%s"' % (field if unit.get(field) is None else unit.get(field)))
    for field in image_field_list:
        if field == 'item_id_int':
            image_field_value.append(unit.get(field))
        else:
            image_field_value.append('"%s"' % unit.get(field))

    good_field_value_str = "(%s)" % (",".join(good_field_value))
    good_value_list.append(good_field_value_str)
    new_in_field_value_str = "(%s)" % (",".join(new_in_field_value))
    new_in_value_list.append(new_in_field_value_str)
    image_field_value_str = '(%s)' % (','.join(image_field_value))
    image_value_list.append(image_field_value_str)

good_header = "INSERT INTO `intelforce-dev`.shein_goods_bak (goods_id,goods_sn,goods_name,goods_url_name,productRelationID,goodsDetailUrl,price,goods_image,level1_cat_name,level2_cat_name,level3_cat_name,level4_cat_name) values"
new_in_header = "insert into `intelforce-dev`.shein_goods_new_in_time (goods_id,new_in_time,add_time,platform) VALUES"
image_header = "INSERT INTO `intelforce-dev`.shein_image (goods_id,productRelationID,image_url) VALUES"

index = 0
good_sql = ""
new_in_sql = ""
image_sql = ""
print(len(good_field_value))
while index * 100 < len(good_value_list):
    good_sql = "%s\r\n%s %s;" % (good_sql, good_header, ",\r\n".join(good_value_list[index * 100: (index + 1) * 100]))
    new_in_sql = "%s\r\n%s %s;" % (new_in_sql, new_in_header, ",\r\n".join(new_in_value_list[index * 100: (index + 1) * 100]))
    image_sql = "%s\r\n%s %s;" % (image_sql, image_header, ",\r\n".join(image_value_list[index * 100: (index + 1) * 100]))
    index = index + 1

good_sql_file = open('good_sql', 'w')
good_sql_file.write(good_sql)
good_sql_file.close()
new_in_sql_file = open('new_in_sql', 'w')
new_in_sql_file.write(new_in_sql)
new_in_sql_file.close()
image_sql_file = open('image_sql', 'w')
image_sql_file.write(image_sql)
image_sql_file.close()





import json
import requests

headers = {
    "Content-Type": "application/json"
}
url = 'https://open.feishu.cn/open-apis/bot/v2/hook/0a638cee-06cb-4a50-938e-7f64fbe99e30'
spu_info_str = ""
for index in range(220):
    str_test = "stripes-ribbed-long-sleeves https://cdn.shopifycdn.net/s/files/1/0471/8277/4427/products/839920fae0cba74d88ecfb386de1a3a5.jpg?v=1618453193 %s" % (index)
    spu_info_str = "%s\r\n%s" % (spu_info_str, str_test)

data = json.dumps({"msg_type": "text", "content": {"text": spu_info_str}})     
resp = requests.post(url, headers=headers, data=data) 
print("status:%s, text:%s" % (resp.status_code, resp.text))

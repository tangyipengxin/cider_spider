# encoding=utf-8
import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')

import json
from sqlalchemy import create_engine
from common import emmiol_common as ec



class EmmiolMerge(object):
    def __init__(self):
        self.date = 20210519
        pass

    def init_db(self):
        self.engine = create_engine("mysql+pymysql://admin:JYFscvVevsANJYmrz9Ud@infwaves.c0rck9lj4qyp.us-east-2.rds.amazonaws.com:3306/erp_product_analysis", max_overflow=5)


    def run(self):
        self.init_db()
        list_file_list = ['./data_dump_1.json', '/home/ec2-user/project/compete_spider/data/emmiol_spider_20210517_t.json']
        detail_file_list = ['../data/emmiol_20210519_new.json', '../data/emmiol_20210522_newtmp.json', '../data/emmiol_20210519_part2.json', '../data/emmiol_20210519_part3.json', '../data/emmiol_20210520_part1.json', '../data/emmiol_20210520_part1_part2.json', '../data/emmiol_20210520_part1_part3.json']
        list_item_json = {}
        for list_file in list_file_list:
            with open(list_file, "r") as f:
                lines = f.readlines()
                for line in lines:
                    new_unit = {}
                    item_unit = json.loads(line.strip())
                    new_unit['item_name'] = item_unit.get('item_name')
                    item_id = item_unit.get('item_id')
                    if item_id.find('-') != -1:
                        spu_code = item_id.split('-')[0]
                    else:
                        spu_code = item_unit.get('item_id')[0:9]
                    #print("%s\t%s\t%s" % (spu_code, item_id, item_unit.get('item_name')))
                    new_unit['spu_code'] = spu_code
                    new_unit['price'] = item_unit.get('price')
                    new_unit['cat1'] = item_unit.get('item_list_name').split('>')[0] if len(item_unit.get('item_list_name').split('>')) >= 1 else ''
                    new_unit['cat2'] = item_unit.get('item_list_name').split('>')[1] if len(item_unit.get('item_list_name').split('>')) >= 2 else ''
                    new_unit['cat3'] = item_unit.get('item_list_name').split('>')[2] if len(item_unit.get('item_list_name').split('>')) >= 3 else ''
                    new_unit['cat4'] = item_unit.get('item_list_name').split('>')[3] if len(item_unit.get('item_list_name').split('>')) >= 4 else ''
                    new_unit['original_cat_id'] = item_unit.get('item_list_id')
                    new_unit['detail_url'] = item_unit.get('detail_url')
                    new_unit['color'] = item_unit.get('item_variant').split('/')[0]
                    new_unit['size'] = item_unit.get('item_variant').split('/')[1]
                    new_unit['first_image'] = item_unit.get('first_image')
                    new_unit['status'] = 0
                    if new_unit['cat1'].strip().lower() != 'women':
                        continue
                    else:
                        list_item_json[new_unit.get('spu_code')] = new_unit
                    
                    
        
        list_item_list = {}
        for detail_file in detail_file_list:
            with open(detail_file, "r") as f:
                json_data_str = f.read()
                json_data = json.loads(json_data_str)
                for unit in json_data:
                    list_unit = {}
                    list_unit['item_name'] = unit.get('spu_name')
                    list_unit['spu_code'] = ec.extract_spu_code(unit.get('spu_code'))
                    list_unit['price'] = unit.get('price')
                    list_unit['cat1'] = unit.get('cat1')
                    list_unit['cat2'] = unit.get('cat2')
                    list_unit['cat3'] = unit.get('cat3')
                    list_unit['cat4'] = unit.get('cat4')
                    original_cat_id = unit.get('original_cat_id')

                    sku = unit.get('sku_list')[0]
                    list_unit['detail_url'] = sku.get('detail_url')
                    list_unit['color'] = sku.get('color')
                    list_unit['size'] = sku.get('size')

                    if len(unit.get('image_list')) != 0:
                        image = unit.get('image_list')[0]
                        list_unit['first_image'] = image.get('src')
                    else:
                        list_unit['first_image'] = ''
                    if list_unit['cat1'].strip().lower() != 'women':
                        continue
                    else:
                        list_item_list[list_unit.get('spu_code')] = list_unit

        for spu_code, unit in list_item_list.items():
            if spu_code in list_item_json:
                new_unit = list_item_json.get(spu_code)
                new_unit['status'] = 1
            else:
                list_item_json[spu_code] = unit
                unit['status'] = 0

        #with open("../data/emmiol_all_prod_%s.json" % (self.date), "w") as f:
        #    f.write(json.dumps(list(list_item_json.values())))


        index = 0
        item_list = list(list_item_json.values())
        while index * 100 <= len(item_list):
            self.update_data(item_list[index*100: index *100 + 100])
            index = index + 1
            
                    
    def update_data(self, data_json):
        insert_sql = "replace into erp_product_analysis.spider_prod (source_type, date, spu_code, detail_url, detail_info, status)values"
        value_str_list = []
        for unit in data_json:
            source_type = 'emmiol'
            date = self.date
            spu_code = unit.get('spu_code')
            detail_url = unit.get('detail_url')
            detail_info = json.dumps(unit).replace("'", "\\'")
            status = unit.get('status')
            value_str = "('%s',%s,'%s', '%s', '%s', %s)" % (source_type, date, spu_code, detail_url, detail_info, status)
            value_str_list.append(value_str)
        value = ",".join(value_str_list)
        insert_sql = "%s %s;" % (insert_sql, value)
        print(insert_sql)
        cur = self.engine.execute(insert_sql)


if __name__ == '__main__':
    EmmiolMerge().run()


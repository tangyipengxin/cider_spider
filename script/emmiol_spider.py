# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
import json

import myRequest
from lxml import etree

req = myRequest.MyRequest()

url_list = [
        'bottoms-c-36',
        'tops-c-24',
        'swimwear-c-44',
        'dresses-c-6',
        'two-piece-outfits-c-43',
        'jumpsuits-rompers-b-97',
        'outerwear-c-31',
        'loungewear-c-448',
        'activewear-c-454',
        'trends-c-606',
        'sale-c-501'
        ]

data_file = open('./data/emmiol_spider_20210517.json', 'w')

field_list = ['item_name', 'item_id', 'price', 'item_brand', 'item_category', 'item_list_name', 'item_list_id', 'index', 'quantity', 'item_category2', 'item_category3', 'item_variant']

item_list = {}

for url in url_list:
    index = 1
    cur_url_item_list = {}
    first_item_id = None
    while True:
        if index != 1:
            req_url = "https://www.emmiol.com/%s-page-%s.html" % (url, index)
        else:
            req_url = "https://www.emmiol.com/%s.html" % (url)
        print(req_url)
        resp = req.sync_get(req_url)
        print(resp.status_code)
        desc_html = etree.HTML(resp.text)
        text_split = resp.text.split(";")
        data_json_str = ""
        for unit in text_split:
            if unit.find('googleGoodsItem') != -1:
                data_json_str = unit.split('=')[1]
        data_json = json.loads(data_json_str)
        quit_status = False
        for unit in data_json:
            item_id = unit.get('item_id')
            div_data = desc_html.xpath('//div[@data-goods-id="%s"]' % item_id)
            div_unit = div_data[0]
            detail_url = div_unit.get('data-href')

            img_unit = div_unit.xpath('//img[@data-sku="%s"]'%(item_id))
            first_image = img_unit[0].get('data-original')
            if first_item_id is None:
                first_item_id = item_id
            else:
                if first_item_id == item_id:
                    quit_status = True
                    break
            unit["detail_url"] = detail_url
            unit["first_image"] = first_image
            cur_url_item_list[item_id] = unit
            item_list[item_id] = unit
            data_file.write("\r\n%s" % json.dumps(unit))
        index = index + 1
        if quit_status == True:
            break




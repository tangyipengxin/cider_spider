# encoding=utf-8

import requests
from proxy import proxies
from asyncio import get_event_loop
from time import sleep


class MyRequest:
    loop = get_event_loop()
    # Timeout = httpx.Timeout(timeout=5.0, connect=2, read=2, write=2, pool=2)
    MAX_RETRY = 10

    @staticmethod
    def sync_get(url, headers=None, **kwargs):
        error = None
        for i in range(1, MyRequest.MAX_RETRY):
            try:
                resp = requests.get(url=url, headers=headers, timeout=(5, 7), proxies=proxies(), **kwargs)
                if resp:
                    return resp
                else:
                    continue
            except BaseException as e:
                error = e
                continue
        print('链接[{}]访问失败，共{}次重试,错误原因[{}]'.format(url, str(MyRequest.MAX_RETRY), error))
        return None

    @staticmethod
    def sync_post(url, headers=None, **kwargs):
        error = None
        for i in range(1, MyRequest.MAX_RETRY):
            try:
                resp = requests.post(url=url, headers=headers, timeout=(5, 7), proxies=proxies(), **kwargs)
                if resp:
                    return resp
                else:
                    continue
            except BaseException as e:
                error = e
                continue
        # logger.warning('链接[{}]访问失败，共{}次重试,错误原因[{}]'.format(url, str(MyRequest.MAX_RETRY), error))
        return None

    @staticmethod
    def async_get(url, headers=None, **kwargs):
        for i in range(1, MyRequest.MAX_RETRY):
            try:
                with httpx.AsyncClient(proxies=proxies(), timeout=MyRequest.Timeout) as session:
                    return session.get(url=url, headers=headers, **kwargs)
            except BaseException as e:
                logger.warning('链接[{}]访问失败，第{}次重试,错误原因[{}]'.format(url, str(i), e))
        return None

    @staticmethod
    def async_post(url, headers=None, **kwargs):
        for i in range(1, MyRequest.MAX_RETRY):
            try:
                with httpx.AsyncClient(proxies=proxies(), timeout=MyRequest.Timeout) as session:
                    return session.post(url=url, headers=headers, **kwargs)
            except BaseException as e:
                logger.warning('链接[{}]访问失败，第{}次重试,错误原因[{}]'.format(url, str(i), e))
        return {}

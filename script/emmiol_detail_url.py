# encoding=utf-8

import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
import json
from lxml import etree

import myRequest

req = myRequest.MyRequest()


url_list = ['https://www.emmiol.com/high-waisted-flare-leg-pants-product154748.html']

item_id = 'EBD0234BO002M'

for url in url_list:
    resp = req.sync_get(url)
    print(resp.status_code)
    text = resp.text
    desc_html = etree.HTML(text)
    div_data = desc_html.xpath('//div[@data-goods-id="%s"]' % item_id)
    div_unit = div_data[0]
    print(div_unit.get('data-href'))
    img_unit = div_unit.xpath('//img[@data-sku="%s"]'%(item_id))
    print(img_unit[0].get('data-original'))
    cat_pic_p = div_unit.getchildren()[0]
    print(cat_pic_p.getchildren()[0])
    print(cat_pic_p.getchildren()[0].getchildren()[0].get('data-original'))

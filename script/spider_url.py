import sys
sys.path.insert(0, '.')
sys.path.insert(0, '..')
sys.path.insert(0, '../spider/')
from entity.ProductEntity import Image, Product, Variant
from common import utils
from spider import myRequest
import codecs
import json
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())
import time
import datetime


if __name__ == '__main__':
    url = "https://shopcider.com/products.json?page=1"
    print("spider start...url:%s" % url)
    req = myRequest.MyRequest()
    resp = req.sync_get(url)
    print("spider end...url:%s" % url)
    status_code = resp.status_code
    json_data = resp.json()
    products = json_data.get('products')
    for unit in products:
        product_unit = Product().fromSpiderJSON(unit)
        if product_unit.title == 'Vintage Dreams Cardigan':
            print(product_unit.toDumpJSON())

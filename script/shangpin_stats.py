
import json



if __name__ == "__main__":
    with open("../data/getfiner_20210603.json", "r") as f:
        data_json_str = f.read()
        data_json = json.loads(data_json_str)

    print(len(data_json))
    output = []
    for unit in data_json:
        first_variant = unit.get('variants')[0]
        value_str = "%s\t%s\t%s\t%s\t%s\t%s" % (unit.get('title'), unit.get('handle'), unit.get('product_type'), unit.get('created_at'), first_variant.get('price'), first_variant.get('compare_at_price'))
        output.append(value_str)
    with open('finer_stats.out', 'w') as f:
        f.write("\r\n".join(output))
        

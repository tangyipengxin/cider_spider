def GetStableHashCode(text):
    hash = 23;
    for char in text:
        hash = hash * 31 + ord(char)
    return hash;


import sys
text = sys.argv[1]
print(GetStableHashCode(text))
